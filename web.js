var express = require('express');
var Paraglide = require('./lib/paraglide/paraglide');
var app = express(express.logger());
var port = process.env.PORT || 80;

app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.session({secret: 'he1pcJ8YmcEV'}));
app.use(express.static(__dirname + '/public'));

app.get('*', function(request, response) {
	request.port = port;
	var paraglide = new Paraglide(request, response);
	paraglide.load(request.originalUrl);
});
app.post('*', function(request, response) {
	request.port = port;
	var paraglide = new Paraglide(request, response);
	paraglide.isPost = true;
	paraglide.load(request.originalUrl);
});

app.listen(port, function() {
	console.log('Listening on ' + port);
});