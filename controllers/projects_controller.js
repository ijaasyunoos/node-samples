var _ = require('underscore');
var async = require('async');
var Controller = require('../lib/paraglide/controller');

var Project = require('../models/project');
var ProjectLevel = require('../models/project_level');
var ProjectUpdate = require('../models/project_update');
var StripeUser = require('../models/stripe_user');
var University = require('../models/university');
var User = require('../models/user');

var ProjectsController = function() {
	Controller.apply(this);
};

ProjectsController.routes = {
	':id': {
		action: 'view',
		params: [':id']
	},
	':id/edit': {
		action: 'edit',
		params: [':id']
	}
};

ProjectsController.prototype = {
	_noEquityAccess: false,
	_user: null,
	
	_getProjectUpdateOrRedirect: function(project, id, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (!id) {
					callback(true);
					return;
				}
				
				project.updates(id, function(update) {
					callback(!update, update);
				});
			},
			function(update, callback) {
				project.canView(self._user, function(canView) {
					callback(!canView, update);
				});
			}
		], function(error, update) {
			if (error) {
				self.redirect();
				return;
			}
			
			callback(update);
		});
	},
	
	_getProjectOrRedirect: function(id, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (!id) {
					callback(true);
					return;
				}
				
				Project.find(id, function(project) {
					if (project && self.paraglide.data.isEquity != project.isEquity) {
						project = null;
					}
					
					callback(!project, project);
				});
			},
			function(project, callback) {
				project.canView(self._user, function(canView) {
					callback(!canView, project);
				});
			}
		], function(error, project) {
			if (error) {
				self.redirect();
				return;
			}
			
			callback(project);
		});
	},
	
	_preprocess: function(callback) {
		this._user = this.paraglide.data.user;
		
		if (
			this.paraglide.data.isEquity
			&& (
				!this._user
				|| !this._user.isAccreditedInvestor()
			)
			&& this.paraglide.action != 'require-accredited-investor'
			&& this.paraglide.action != 'entrepreneurs'
		) {
			this._noEquityAccess = true;
		}

		callback();
	},
	
	index: function(id, universitySlug, action, actionId) {
		if (id && Number(id) == id) {
			this.paraglide.action = 'view';
			return this.view(id);
		}
		
		var self = this;
		
		// get whitelabel university URLs to work for URLs like /projects/:project_slug
		if (self.paraglide.data.whitelabelUniversity && id != 'unapproved') {
			if (id == 'universities') {
				self.redirect();
				return;
			}
		
			// shift args to the left
			actionId = action;
			action = universitySlug;
			universitySlug = id;
			id = self.paraglide.data.whitelabelUniversity.slug;
		}
		
		// get alternate URLs to work for URLs like /projects/:university_slug/:project_slug
		if (id && universitySlug && id != 'unapproved' && id != 'universities') {
			var projectSlug = universitySlug;
			universitySlug = id;
			
			async.parallel({
				project: function(callback) {
					Project.findBySlug(projectSlug, function(project) {
						if (project && self.paraglide.data.isEquity != project.isEquity) {
							project = null;
						}
						
						callback(null, project);
					});
				},
				university: function(callback) {
					University.findOne({
						conditions: {
							slug: universitySlug
						}
					}, function(university) {
						callback(null, university);
					});
				}
			}, function(error, results) {
				if (error || !results.university) {
					self.redirect();
					return;
				}
				
				if (!results.project || results.project.universityId != results.university.id) {
					self.redirect(null, ['universities', results.university.slug]);
					return;
				}
				
				if (action == 'fund') {
					self.paraglide.action = 'fund';
					self.fund(results.project.id);
					return;
				}

				if (action == 'updates') {
					self.paraglide.action = 'updates';
					self.updates(results.project.id, actionId);
					return;
				}
				
				self.paraglide.action = 'view';
				self.view(results.project.id, actionId);
			});
			return;
		}
		
		if (this._noEquityAccess) {
			this.redirect('require-accredited-investor');
			return;
		}
		
		async.waterfall([
			function(callback) {
				if (!self._user) {
					callback(null, false);
					return;
				}
				
				callback(null, self._user.isAdmin);
			},
			function(isAdmin, callback) {
				var conditions = {};
				
				if (!isAdmin) {
					conditions.isActive = true;
					conditions.dateActive = Project.condition('lt', (new Date()));
				}
				
				University.find({
					conditions: conditions,
					order: 'name'
				}, function(universities) {
					async.map(universities, function(university, callback) {
						callback(null, {
							isSelected: false,
							university: university
						});
					}, function(error, universityItems) {
						callback(null, isAdmin, universityItems);
					});
				});
			},
			function(isAdmin, universityItems, callback) {
				if (self.paraglide.data.whitelabelUniversity) {
					callback(null, isAdmin, universityItems, self.paraglide.data.whitelabelUniversity);
					return;
				}
				
				if (id != 'universities') {
					callback(null, isAdmin, universityItems, null);
					return;
				}
				
				var selectedUniversity = null;
				universityItems.forEach(function(universityItem, i) {
					if (universityItem.university.slug == universitySlug || universityItem.university.id == universitySlug) {
						selectedUniversity = universityItem.university;
					}
				});
				callback(null, isAdmin, universityItems, selectedUniversity);
			},
			function(isAdmin, universityItems, university, callback) {
				var conditions = {
					dateFundingStarts: Project.condition('lte', new Date()),
					dateFundingEnds: Project.condition('gt', new Date()),
					isApproved: true,
					isEquity: !!self.paraglide.data.isEquity,
					isPrivate: false
				};
				
				if (isAdmin && id == 'unapproved') {
					delete conditions.dateFundingStarts;
					delete conditions.dateFundingEnds;
					conditions.isApproved = false;
					conditions.isSubmitted = true;
				}

				if (university) conditions.universityId = university.id;
				Project.find({
					conditions: conditions,
					order: 'dateFundingEnds'
				}, function(projects) {
					async.map(projects, function(project, callback) {
						project.calculateFields(['owner', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
							results.owner.url(self.paraglide, function(ownerUrl) {
								callback(null, {
									owner: results.owner,
									ownerUrl: ownerUrl,
									project: project,
									projectUrl: results.url,
									thumbnail: results.thumbnail
								});
							});
						});
					}, function(error, projectItems) {
						callback(null, universityItems, university, projectItems);
					});
				});
			}
		], function(error, universityItems, university, projectItems) {
			self.render('index', {
				hasOneProject: (projectItems.length == 1),
				projects: projectItems,
				universities: universityItems,
				university: university
			});
		});
	},
	
	create: function() {
		var project = new Project();
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					project.userId = self._user.id;
					callback(null);
				});
			},
			function(callback) {
				if (!self.paraglide.isPost) {
					callback(null);
					return;
				}
				
				project.setValues(self.request.body, null, function() {
					project.duration = self.request.body.duration;
					project.isEquity = self.paraglide.data.isEquity;
					project.universityId = self._user.primaryUniversityId;
					project.save(function(saved) {
						if (!saved) {
							callback(null);
							return;
						}
						
						project.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			}
		], function(error) {
			self.render('edit', {
				isDurationEditable: true,
				project: project
			});
		});
	},
	
	createUpdate: function(id) {
		var update = new ProjectUpdate();
		var self = this;
		async.waterfall([
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				update.projectId = project.id;
				project.canManage(self._user, function(canManage) {
					callback(!canManage, project);
				});
			},
			function(project, callback) {
				if (!self.paraglide.isPost) {
					callback(null, project);
					return;
				}
				
				update.setValues(self.request.body, {booleans: ['hasMailed', 'isPublic']}, function() {
					update.save(function(saved) {
						if (!saved) {
							callback(null, project, update);
							return;
						}
						
						update.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			},
			function(project, callback) {
				project.calculateFields(['levels', 'owner', 'thumbnail'], {paraglide: self.paraglide}, function(error, results) {
					callback(error, project, results.owner, results.thumbnail, results.levels);
				});
			}
		], function(error, project, owner, thumbnail, levels) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('edit-update', {
				levels: levels,
				owner: owner,
				project: project,
				thumbnail: thumbnail,
				update: update
			});
		});
	},
	
	delete: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
					return;
				});
			},
			function(project, callback) {
				project.canEdit(self._user, function(canEdit) {
					callback(!canEdit, project);
				});
			},
			function(project, callback) {
				if (!self.paraglide.isPost || !self.paraglide.request.body.delete) {
					callback(null, project);
					return;
				}
				
				project.delete(function(deleted) {
					if (!deleted) {
						callback(null, project);
						return;
					}
					
					self.redirect();
				});
			},
			function(project, callback) {
				project.url(self.paraglide, function(url) {
					callback(null, project, url);
				});
			}
		], function(error, project, url) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('delete', {
				project: project,
				projectUrl: url
			});
		});
	},
	
	deleteUpdate: function(id, updateId) {
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
					return;
				});
			},
			function(project, callback) {
				project.canManage(self._user, function(canEdit) {
					callback(!canEdit, project);
				});
			},
			function(project, callback) {
				self._getProjectUpdateOrRedirect(project, updateId, function(update) {
					callback(null, project, update);
				});
			},
			function(project, update, callback) {
				if (!self.paraglide.isPost || !self.paraglide.request.body.delete) {
					callback(null, project, update);
					return;
				}
				
				update.delete(function(deleted) {
					if (!deleted) {
						callback(null, project, update);
						return;
					}
					
					project.url(self.paraglide, 'updates', function(url) {
						self.paraglide.redirectTo(url);
					});
				});
			},
			function(project, update, callback) {
				async.parallel({
					updateUrl: function(callback) {
						update.url(self.paraglide, function(url) {
							callback(null, url);
						});
					},
					url: function(callback) {
						project.url(self.paraglide, function(url) {
							callback(null, url);
						});
					}
				}, function(error, results) {
					callback(null, project, update, results.url, results.updateUrl);
				});
			}
		], function(error, project, update, url, updateUrl) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('delete-update', {
				project: project,
				projectUrl: url,
				update: update,
				updateUrl: updateUrl
			});
		});
	},
	
	edit: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
					return;
				});
			},
			function(project, callback) {
				project.canEdit(self._user, function(canEdit) {
					callback(!canEdit, project);
				});
			},
			function(project, callback) {
				project.url(self.paraglide, function(url) {
					callback(null, project, url);
				});
			},
			function(project, url, callback) {
				if (!self.paraglide.isPost) {
					callback(null, project, url);
					return;
				}
				
				project.setValues(self.request.body, {
					booleans: ['isPrivate']
				}, function() {
					if (self.request.body.duration) {
						project.duration = self.request.body.duration;
					}
					
					project.save(function(saved) {
						if (!saved) {
							callback(null, project, url);
							return;
						}
						
						if (self.request.files && self.request.files.image) {
							project.uploadImage(self.request.files.image, function(image) {
								project.url(self.paraglide, function(url) {
									self.paraglide.redirectTo(url);
								});
							});
							return;
						}
						
						project.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			},
			
		], function(error, project, url) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('edit', {
				isDurationEditable: (project.isDurationEditable() || self._user.isAdmin),
				project: project,
				projectUrl: url
			});
		});
	},
	
	editLevels: function(id) {
		var self = this;
		var newLevel = new ProjectLevel();
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
					return;
				});
			},
			function(project, callback) {
				project.canManage(self._user, function(canManage) {
					callback(!canManage, project);
				});
			},
			function(project, callback) {
				project.calculateFields(['levels', 'url'], {paraglide: self.paraglide}, function(error, results) {
					callback(null, project, results.url, results.levels);
				});
			},
			function(project, url, levels, callback) {
				if (!self.paraglide.isPost || !self.request.body.level) {
					callback(null, project, url, levels, null);
					return;
				}
				
				if (self.request.body.level == 'new') {
					callback(null, project, url, levels, newLevel);
					return;
				}
				
				selectedLevel = null;
				levels.forEach(function(level) {
					if (level.id == self.request.body.level) {
						selectedLevel = level;
					}
				});
				callback(null, project, url, levels, selectedLevel);
			},
			function(project, url, levels, level, callback) {
				if (!level) {
					callback(null, project, url, levels);
					return;
				}
				
				if (self.request.body.delete) {
					level.delete(function(deleted) {
						self.redirect('edit-levels', project.id);
					});
					return;
				}
				
				level.setValues(self.request.body, {
					booleans: ['canSellUnlimited']
				}, function() {
					level.projectId = project.id;
					level.save(function(saved) {
						if (!saved) {
							callback(null, project, url, levels);
							return;
						}
						
						self.redirect('edit-levels', project.id);
					});
				});
			},
		], function(error, project, url, levels) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('edit-levels', {
				canManage: true,
				levels: levels,
				newLevel: newLevel,
				project: project,
				projectUrl: url
			});
		});
	},
	
	editUpdate: function(id, updateId) {
		var self = this;
		async.waterfall([
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				project.canManage(self._user, function(canManage) {
					callback(!canManage, project);
				});
			},
			function(project, callback) {
				self._getProjectUpdateOrRedirect(project, updateId, function(update) {
					callback(null, project, update);
				});
			},
			function(project, update, callback) {
				if (!self.paraglide.isPost) {
					callback(null, project, update);
					return;
				}
				
				if (self.request.body.delete) {
					update.delete(function(deleted) {
						project.url(self.paraglide, 'updates', function(url) {
							self.paraglide.redirectTo(url);
						});
					});
					return;
				}
				
				update.setValues(self.request.body, {booleans: ['hasMailed', 'isPublic']}, function() {
					update.save(function(saved) {
						if (!saved) {
							callback(null, project, update);
							return;
						}
						
						project.url(self.paraglide, 'updates', update.id, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			},
			function(project, update, callback) {
				project.calculateFields(['levels', 'owner', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
					callback(error, project, update, results.nextUpdate, results.previousUpdate, results.owner, results.thumbnail, results.url, results.levels);
				});
			}
		], function(error, project, update, nextUpdate, previousUpdate, owner, thumbnail, url, levels) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('edit-update', {
				levels: levels,
				owner: owner,
				nextUpdate: nextUpdate,
				previousUpdate: previousUpdate,
				project: project,
				projectUrl: url,
				thumbnail: thumbnail,
				update: update
			});
		});
	},
	
	entrepreneurs: function() {
		this.render('entrepreneurs');
	},
	
	fund: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				project.calculateFields(['levels', 'owner', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
					if (results.levels.length == 0) {
						self.paraglide.redirectTo(results.url);
						return;
					}
			
					results.levels = results.levels.map(function(level) {
						return {
							isSelected: false,
							level: level
						};
					});
					callback(error, project, results.owner, results.thumbnail, results.url, results.levels);
				});
			},
			function(project, owner, thumbnail, url, levels, callback) {
				if (!levels) {
					project.url(self.paraglide, 'fund', function(url) {
						self.paraglide.redirectTo(url);
					});
					return;
				}
			
				selectedLevel = null;
				levels.forEach(function(level, i) {
					if (level.level.id != self.request.body.level) {
						return;
					}
					
					selectedLevel = level.level;
					level.isSelected = true;
					var levelOffset = 1;
					
					while (self.request.body.amount < selectedLevel.amount) {
						if (i - levelOffset < 0) {
							selectedLevel.errors.amount = 'The minimum funding amount must be greater than or equal to the lowest funding level';
							return;
						}
						
						levels[i - levelOffset + 1].isSelected = false;
						selectedLevel = levels[i - levelOffset].level;
						levels[i - levelOffset].isSelected = true;
						levelOffset++;
					}
					
					// if the level has changed, set an error message
					if (levelOffset > 1) {
						selectedLevel.errors.amount = 'Your funding level has been decreased because your funding amount was lower than the minimum amount accepted by the funding level you selected';
					}
				});
				
				if (selectedLevel && selectedLevel.isSoldOut()) {
					selectedLevel.errors.numAllowed = 'The funding level you selected is sold out';
				}
				
				if (!selectedLevel || (_.size(selectedLevel.errors) > 0) || !self.request.body.amount) {
					self.render('fund-select-level', {
						amount: self.request.body.amount,
						levels: levels,
						project: project,
						projectUrl: url,
						owner: owner,
						selectedLevel: selectedLevel,
						thumbnail: thumbnail
					});
					return;
				}
				
				if (self.request.body.pay) {
					self._user.setPaymentType(self.request.body, function(success) {
						callback(null, project, owner, thumbnail, url, selectedLevel, self.request.body.amount);
					});
					return;
				}
				
				callback(null, project, owner, thumbnail, url, selectedLevel, self.request.body.amount);
			},
			function(project, owner, thumbnail, url, level, amount, callback) {
				if (!self._user.paymentType) {
					callback(null, project, owner, thumbnail, url, level, amount, false);
					return;
				}
				
				self._user.validatePaymentType(function(validated) {
					if (!validated) {
						callback(null, project, owner, thumbnail, url, level, amount, false);
						return;
					}
					
					project.fund(self._user, level, self.request.body.amount, function(funded) {
						callback(null, project, owner, thumbnail, url, level, amount, funded);
					});
				});
			},
			function(project, owner, thumbnail, url, level, amount, funded, callback) {
				if (funded) {
					callback(null, project, owner, thumbnail, url, level, amount);
					return;
				}
				
				self.render('fund-enter-payment', {
					amount: self.request.body.amount,
					level: level,
					project: project,
					projectUrl: url,
					owner: owner,
					thumbnail: thumbnail
				});
			}
		], function(error, project, owner, thumbnail, url, level, amount) {
			self.render('fund-success', {
				amount: amount,
				level: level,
				project: project,
				projectUrl: url,
				owner: owner,
				thumbnail: thumbnail
			});
		});
	},
	
	requireAccreditedInvestor: function() {
		if (this._user && this._user.isAccreditedInvestor()) {
			this.redirect();
			return;
		}
		
		this.render('require-accredited-investor');
	},
	
	updates: function(id, updateId) {
		if (updateId) {
			this.action = 'view-update';
			return this.viewUpdate(id, updateId);
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				project.canManage(self._user, function(canManage) {
					callback(null, project, canManage);
				});
			},
			function(project, canManage, callback) {
				project.calculateFields(['fundUrl', 'levels', 'owner', 'thumbnail', 'updates', 'updatesUrl', 'url'], {
					canManage: canManage,
					paraglide: self.paraglide
				}, function(error, results) {
					callback(error, project, canManage, results.owner, results.thumbnail, results.url, results.fundUrl, results.updatesUrl, results.levels, results.updates);
				});
			}
		], function(error, project, canManage, owner, thumbnail, url, fundUrl, updatesUrl, levels, updates) {
			self.render('updates', {
				canManage: canManage,
				fundUrl: fundUrl,
				levels: levels,
				project: project,
				projectUrl: url,
				updates: updates,
				updatesUrl: updatesUrl,
				owner: owner,
				thumbnail: thumbnail
			});
		});
	},

	view: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				if (self._user && self._user.isAdmin && self.paraglide.isPost) {
					if (self.request.body.approve) {
						project.isApproved = true;
					} else if (self.request.body.convertToEquity) {
						project.isEquity = true;
					} else if (self.request.body.convertToNonEquity) {
						project.isEquity = false;
					}
					
					project.save(function(saved) {
						project.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
					return;
				}
				
				if (
					self._user && self._user.isAdmin
					&& self.paraglide.isPost && self.request.body.reject
				) {
					project.isSubmitted = false;
					project.save(function(saved) {
						project.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
					return;
				}
				
				callback(null, project);
			},
			function(project, callback) {
				project.calculateFields(['canEdit', 'canManage', 'fundUrl', 'levels', 'numUpdates', 'owner', 'thumbnail', 'university', 'updatesUrl', 'url'], {
					paraglide: self.paraglide,
					user: self._user
				}, function(error, results) {
					callback(error, project, results.canEdit, results.canManage, results.levels, results.numUpdates, results.owner, results.thumbnail, results.university, results.url, results.fundUrl, results.updatesUrl);
				});
			},
			function(project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, callback) {
				if (!canManage && self._noEquityAccess) {
					self.redirect('require-accredited-investor');
					return;
				}
				
				owner.url(self.paraglide, function(ownerUrl) {
					callback(null, project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl);
				});
			},
			function(project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl, callback) {
				owner.stripeUser(function(stripeUser) {
					if (
						self._user && self._user.id == owner.id
						&& (self.request.body.connect == 'stripe' || self.request.query.code)
						&& !stripeUser
					) {
						var params = {
							redirectUrl: self.request.originalUrl
						};
						StripeUser.connect(self.paraglide, params, self._user, function(error, stripeUser) {
							if (error) {
								callback(error);
								return;
							}
							
							self.paraglide.redirectTo(url);
						});
						return;
					}

					var needsPaymentAccount = (!stripeUser && self._user && self._user.id == owner.id)
					callback(null, project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl, needsPaymentAccount);
				});
			},
			function(project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl, needsPaymentAccount, callback) {
				if (
					self._user && canManage && !needsPaymentAccount
					&& self.paraglide.isPost && self.request.body.submit
					&& levels.length > 0 // a project must have at least one level
				) {
					project.isSubmitted = true;
					project.save(function(saved) {
						self.paraglide.redirectTo(url);
					});
					return;
				}
				
				callback(null, project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl, needsPaymentAccount);
			}
		], function(error, project, canEdit, canManage, levels, numUpdates, owner, thumbnail, university, url, fundUrl, updatesUrl, ownerUrl, needsPaymentAccount) {
			self.render('view', {
				canEdit: canEdit,
				canManage: canManage,
				fundUrl: fundUrl,
				levels: levels,
				needsPaymentAccount: needsPaymentAccount,
				numUpdates: numUpdates,
				owner: owner,
				ownerUrl: ownerUrl,
				project: project,
				projectUrl: url,
				thumbnail: thumbnail,
				university: university,
				updatesUrl: updatesUrl
			});
		});
	},
	
	viewUpdate: function(id, updateId) {
		var self = this;
		async.waterfall([
			function(callback) {
				self._getProjectOrRedirect(id, function(project) {
					callback(null, project);
				});
			},
			function(project, callback) {
				self._getProjectUpdateOrRedirect(project, updateId, function(update) {
					callback(null, project, update);
				});
			},
			function(project, update, callback) {
				project.canManage(self._user, function(canManage) {
					callback(null, project, update, canManage);
				});
			},
			function(project, update, canManage, callback) {
				if (!canManage || !self.paraglide.isPost || !self.paraglide.request.body.publish) {
					callback(null, project, update, canManage);
					return;
				}
				
				update.publish({paraglide: self.paraglide}, function(published) {
					if (!published) {
						callback(null, project);
						return;
					}
					
					project.url(self.paraglide, 'updates', update.id, function(url) {
						self.paraglide.redirectTo(url);
					});
				});
			},
			function(project, update, canManage, callback) {
				async.parallel({
					nextUpdate: function(callback) {
						update.next(function(nextUpdate) {
							callback(null, nextUpdate);
						});
					},
					previousUpdate: function(callback) {
						update.previous(function(previousUpdate) {
							callback(null, previousUpdate);
						});
					},
					updateUrl: function(callback) {
						update.url(self.paraglide, function(url) {
							callback(null, url);
						});
					},
					projectFields: function(callback) {
						project.calculateFields(['fundUrl', 'levels', 'owner', 'thumbnail', 'url'], {
							paraglide: self.paraglide
						}, function(error, results) {
							callback(null, results);
						});
					}
				}, function(error, results) {
					callback(
						error,
						project,
						update,
						canManage,
						results.nextUpdate,
						results.previousUpdate,
						results.projectFields.owner,
						results.projectFields.thumbnail,
						results.projectFields.url,
						results.projectFields.fundUrl,
						results.updateUrl,
						results.projectFields.levels
					);
				});
			}
		], function(error, project, update, canManage, nextUpdate, previousUpdate, owner, thumbnail, url, fundUrl, updateUrl, levels) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('view-update', {
				canManage: canManage,
				fundUrl: fundUrl,
				levels: levels,
				owner: owner,
				nextUpdate: nextUpdate,
				previousUpdate: previousUpdate,
				project: project,
				projectUrl: url,
				thumbnail: thumbnail,
				update: update,
				updateUrl: updateUrl
			});
		});
	}
};

module.exports = ProjectsController;
