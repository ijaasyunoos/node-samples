var _ = require('underscore');
var async = require('async');
var Controller = require('../lib/paraglide/controller');
var User = require('../models/user');

var UsersController = function() {
	Controller.apply(this);
};

UsersController.prototype = {
	_user: null,
	
	_getUserOrRedirect: function(id, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (!id) {
					callback(true);
					return;
				}

				if (Number(id) == id) {
					User.find(id, function(user) {
						callback(!user, user);
					});
					return;
				}

				User.findBySlug(id, function(user) {
					callback(!user, user);
				});
			},
			function(user, callback) {
				user.canView(self._user, function(canView) {
					callback(!canView, user);
				});
			}
		], function(error, user) {
			if (error) {
				self.redirect();
				return;
			}

			callback(user);
		});
	},
	
	_preprocess: function(callback) {
		this._user = this.paraglide.data.user;
		callback();
	},
	
	index: function(id) {
		if (id) {
			return this.view(id);
		}
		
		this.paraglide.redirect();
	},

	view: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				self._getUserOrRedirect(id, function(user) {
					callback(null, user);
				});
			},
			function(user, callback) {
				if (!self._user) {
					callback(null, user);
					return;
				}
				
				if (self._user.isAdmin && self.request.body.login) {
					user.login(self.paraglide);
					return;
				}
				
				callback(null, user);
			},
			function(user, callback) {
				user.url(self.paraglide, function(url) {
					callback(null, user, url);
				});
			},
			function(user, url, callback) {
				async.parallel({
					backed: function(callback) {
						user.projectBackers(function(projectBackers) {
							async.map(projectBackers, function(projectBacker, callback) {
								projectBacker.project(function(project) {
									if (!project || project.isPrivate) {
										callback(null, null);
										return;
									}
									
									project.calculateFields(['isOtherUniversity', 'owner', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
										results.owner.url(self.paraglide, function(url) {
											callback(null, {
												isOtherUniversity: results.isOtherUniversity,
												owner: results.owner,
												project: project,
												projectUrl: results.url,
												thumbnail: results.thumbnail
											});
										});
									});
								});
							}, function(error, projects) {
								callback(null, _.compact(projects));
							});
						});
					},
					started: function(callback) {
						user.projects({
							conditions: {
								isPrivate: false
							}
						}, function(projects) {
							async.map(projects, function(project, callback) {
								project.calculateFields(['isOtherUniversity', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
									callback(null, {
										isOtherUniversity: results.isOtherUniversity,
										owner: self._user,
										ownerUrl: url,
										project: project,
										projectUrl: results.url,
										thumbnail: results.thumbnail
									});
								});
							}, function(error, projectItems) {
								callback(null, projectItems);
							});
						});
					},
					thumbnail: function(callback) {
						user.thumbnail(function(thumbnail) {
							callback(null, thumbnail);
						});
					}
				}, function(error, results) {
					callback(error, user, results.backed, results.started, results.thumbnail);
				});
			}
		], function(error, user, backed, started, thumbnail) {
			self.render('view', {
				hasOneProject: (started.length == 1),
				hasOneProjectBacked: (backed.length == 1),
				projects: started,
				projectsBacked: backed,
				isAdmin: self._user && self._user.isAdmin,
				profileUser: user,
				thumbnail: thumbnail
			});
		});
	}
};

module.exports = UsersController;
