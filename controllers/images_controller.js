var async = require('async');
var Controller = require('../lib/paraglide/controller');
var ImageManipulator = require('../lib/helpers/image_manipulator');
var Paraglide = require('../lib/paraglide/paraglide');
var StoredImage = require('../models/stored_image');

var AWS = require('aws-sdk');
var s3;

Paraglide.onInitComplete(function() {
	AWS.config.update({
		accessKeyId: Paraglide.config.amazon.aws.key,
		secretAccessKey: Paraglide.config.amazon.aws.secret
	});
	s3 = new AWS.S3();
});

var ImagesController = function() {
	Controller.apply(this);
};

ImagesController.prototype = {
	index: function() {
		var path = this.paraglide.request.url.substr(Paraglide.siteRoot.length);
		var cache = StoredImage.getCache();
		var cacheKey = 'amazon-s3/' + path
		
		var args = arguments;
		var self = this;
		async.waterfall([
			function(callback) {
				if (!cache) {
					callback(null);
					return;
				}
				
				cache.get(cacheKey, function(cachedUrl) {
					if (!cachedUrl) {
						callback(null);
						return;
					}
					
					self.paraglide.redirectTo(cachedUrl);
				});
			},
			function(callback) {
				s3.client.headObject({
					Bucket: StoredImage.s3Bucket,
					Key: path
				}, function(error, data) {
					if (error) {
						callback(null);
						return;
					}
					
					var url = (self.paraglide.request.protocol == 'https') ? 'https://' : 'http://';
					url += StoredImage.s3Bucket + '.s3.amazonaws.com/';
					url += path;
					
					async.waterfall([
						function(callback) {
							if (!cache) {
								callback(null);
								return;
							}
							
							cache.set(cacheKey, url, 60 * 60 * 24, function() {
								callback(null);
							});
						}
					], function(error) {
						self.paraglide.redirectTo(url);
					});
				});
			},
			function(callback) {
				var imageUrl = args[args.length - 1];
				var extPos = imageUrl.lastIndexOf('.');
				var extension = (extPos !== false) ? imageUrl.substr(extPos + 1) : '';
				var parts = imageUrl.split('-');
				
				if (parts.length == 1) {
					callback(true);
					return;
				}
				
				var imageId = parts[0];
				var hash = null;
				var dimensions = parts[1];
				
				if (parts.length == 3) {
					hash = parts[1];
					dimensions = parts[2];
				}
				
				if (extension) {
					dimensions = dimensions.substr(0, dimensions.length - 1 - extension.length);
				}
		
				var dimensionParts = dimensions.split('x');
				
				if (dimensionParts.length < 2) {
					callback(true);
					return;
				}
		
				var width = dimensionParts[0];
				var height = dimensionParts[1];
				callback(null, imageId, extension, width, height, hash);
			},
			function(imageId, extension, width, height, hash, callback) {
				if (isNaN(parseFloat(imageId))) {
					callback(null, imageId, extension, width, height, hash, null);
					return;
				}
				
				StoredImage.find(imageId, function(storedImage) {
					callback(null, imageId, extension, width, height, hash, storedImage);
				});
			},
			function(imageId, extension, width, height, hash, image, callback) {
				if (image) {
					callback(null, imageId, extension, width, height, hash, image);
					return;
				}
				
				var originalImage = 'images/';
				
				for (var i = 0; i < args.length - 1; i++) {
					originalImage += args[i] + '/';
				}
				
				originalImage += imageId + '.' + extension;
				image = new ImageManipulator(originalImage, function(error) {
					if (error) {
						callback(true);
						return;
					}
					
					callback(null, imageId, extension, width, height, hash, image);
				});
			},
			function(imageId, extension, width, height, hash, image, callback) {
				if (hash != image.sizeHash(width, height)) {
					return;
				}
				
				image.resize(width, height, function() {
					callback(null);
				});
			}
		], function(error) {
			if (error) {
				return;
			}
			
			self.paraglide.redirectTo(self.paraglide.request.url);
		});
	}
};

module.exports = ImagesController;
