var Controller = require('../lib/paraglide/controller');

var ContactController = function() {
	Controller.apply(this);
};

ContactController.prototype = {
	index: function() {
		this.render('contact');
	}
};

module.exports = ContactController;
