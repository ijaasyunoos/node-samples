var async = require('async');
var Controller = require('../lib/paraglide/controller');
var User = require('../models/user');
var UserEmail = require('../models/user_email');

var MainController = function() {
	Controller.apply(this);
};

MainController.prototype = {
	index: function() {
		this.paraglide.layout = 'main/layout.index-equity';
		this.render('index-equity');
	},
	
	beta: function() {
		this.render('index');
	},
	
	crowdfunders: function() {
		if (this.paraglide.data.isEquity) {
			this.redirect();
			return;
		}

		if (this.paraglide.data.whitelabelUniversity) {
			this.render('index-whitelabel');
			return;
		}

		this.render('index');
	},
	
	launch: function() {
		if (this._user) {
			this.redirect();
			return;
		}

		var user = new User();
		var userEmail = new UserEmail();
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self.paraglide.isPost) {
					callback(true);
					return;
				}
				
				user.name = self.request.body.name;
				user.password = 1;
				user.validate(function(validated) {
					if (!userEmail.validateEmails({
						email: self.request.body.email,
						emailVerify: self.request.body.email
					})) {
						if (userEmail.errors.email) {
							user.errors.email = userEmail.errors.email;
						}
						
						callback(true);
						return;
					}
	
					userEmail.email = self.request.body.email;
					userEmail.validate(function(validated) {
						if (validated) {
							callback(null);
							return;
						}
						
						if (userEmail.errors.email) {
							user.errors.email = userEmail.errors.email;
						}
						
						callback(true);
					});
				});
			},
			function(callback) {
				user.save(function(saved) {
					if (!saved) {
						callback(null);
						return;
					}
					
					userEmail.save(function(saved) {
						user.primaryEmailId = userEmail.id;
						user.save(function(saved) {
							self.render('launch-submitted', {
								hideNav: true
							});
						});
					});
				});
			},
		], function(error) {
			self.render('launch', {
				hideNav: true,
				newUser: user
			});
		});
	}
};

module.exports = MainController;
