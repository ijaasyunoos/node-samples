var _ = require('underscore');
var async = require('async');
var Controller = require('../lib/paraglide/controller');
var User = require('../models/user');

var ContestsController = function() {
	Controller.apply(this);
};

ContestsController.prototype = {
	_user: null,
	
	_preprocess: function(callback) {
		this._user = this.paraglide.data.user;
		callback();
	},
	
	index: function() {
		this.paraglide.redirect();
	},
	
	georgetown: function() {
		this.render('georgetown');
	},
	
	princeton: function() {
		this.render('princeton');
	},
	
	berkeley: function() {
		this.render('berkeley');
	},
	
	claremont: function() {
		this.render('claremont');
	}
};

module.exports = ContestsController;
