var _ = require('underscore');
var async = require('async');
var Controller = require('../lib/paraglide/controller');
var FacebookUser = require('../models/facebook_user');
var LinkedInUser = require('../models/linkedin_user');
var StripeUser = require('../models/stripe_user');
var University = require('../models/university');
var User = require('../models/user');

var AccountController = function() {
	Controller.apply(this);
};

AccountController.prototype = {
	_user: null,
	
	_preprocess: function(callback) {
		this._user = this.paraglide.data.user;
		callback();
	},
	
	index: function() {
		this.paraglide.redirect();
	},
	
	viewPaymentConfirmationEmail: function() {
		var self = this;
		var data = {
			user: {name: "Drew"},
			project: {title: "Brass Roots"},
			projectUrl: "http://www.alumnifunder.com/", 
			company: "New Orleans Productions",
			level: {amount: 50.20, title: "platinum"},
			payment: {dateCreated: "2012-10-09 15:51:26"}
		};
		self.render('email.payment-confirmation', data, function(html) {
			self.paraglide.response.send(html);
		});
	},
	viewPledgeConfirmationEmail: function() {
		var self = this;
		var data = {
			user: {name: "Drew"},
			project: {title: "Brass Roots"},
			projectUrl: "http://www.alumnifunder.com/", 
			company: "New Orleans Productions",
			level: {amount: 50.20, title: "platinum"},
			payment: {dateCreated: "2012-10-09 15:51:26"}
		};
		self.render('email.pledge-confirmation', data, function(html) {
			self.paraglide.response.send(html);
		});
	},
	
	changePassword: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				if (!self.paraglide.isPost || !self._user.validatePasswords(self.request.body)) {
					callback(null);
					return;
				}
				
				self._user.password = User.hashPassword(self.request.body.password);
				self._user.save(function(saved) {
					if (!saved) {
						callback(null);
						return;
					}
					
					self._user.login(self.paraglide, function() {
						self.render('change-password-success');
					});
				});
			}
		], function(error) {
			self.render('change-password');
		});
	},

	edit: function() {
		var self = this;

		async.waterfall([
			function(callback) {
				User.requireLogin(self.paraglide, function() {
					callback(null);
				});
			},
			function(callback) {
				if (!self.paraglide.isPost || self.request.body.connect || self.request.body.disconnect) {
					callback(null);
					return;
				}

				if (self.request.body.password || self.request.body.passwordVerify) {
					if (!self._user.validatePasswords(self.request.body)) {
						callback(null);
						return;
					}
					self._user.password = User.hashPassword(self.request.body.password);
					self._user.save(function(saved) {
						if (!saved) {
							callback(null);
							return;
						}

						self._user.login(self.paraglide, function() {
							self.redirect('edit');
						});
					});
					return;
				}

				self._user.setValues(self.request.body, null, function() {
					self._user.save({paraglide: self.paraglide}, function(saved) {
						if (!saved) {
							callback(null);
							return;
						}

						if (self.request.files && self.request.files.image) {
							self._user.uploadImage(self.request.files.image, function(image) {
								self.redirect('edit');
							});
							return;
						}

						self.redirect('edit');
					});
				});
			},
			function(callback) {
				async.parallel({
					facebookUser: function(callback) {
						self._user.facebookUser(function(facebookUser) {
							if (self.request.query.connect == 'facebook' && !facebookUser) {
								FacebookUser.connect(self.paraglide, null, self._user, function() {
									self.redirect('edit');
									return;
								});
								return;
							}

							if (self.request.body.disconnect == 'facebook' && facebookUser) {
								facebookUser.delete(function(deleted) {
									self.redirect('edit');
								});
								return;
							}

							callback(null, facebookUser);
						});
					},
					linkedInUser: function(callback) {
						self._user.linkedInUser(function(linkedInUser) {
							if (self.request.query.connect == 'linkedin' && !linkedInUser) {
								LinkedInUser.connect(self.paraglide, null, self._user, function() {
									self.redirect('edit');
									return;
								});
								return;
							}

							if (self.request.body.disconnect == 'linkedin' && linkedInUser) {
								linkedInUser.delete(function(deleted) {
									self.redirect('edit');
								});
								return;
							}

							callback(null, linkedInUser);
						});
					},
					primaryEmail: function(callback) {
						self._user.primaryEmail(function(primaryEmail) {
							callback(null, primaryEmail);
						});
					},
					stripeUser: function(callback) {
						self._user.stripeUser(function(stripeUser) {
							if ((self.request.body.connect == 'stripe' || self.request.query.code) && !stripeUser) {
								StripeUser.connect(self.paraglide, null, self._user, function(error, stripeUser) {
									if (error) {
										callback(error);
										return;
									}
									
									self.redirect('edit');
								});
								return;
							}
							
							if (self.request.body.disconnect == 'stripe' && stripeUser) {
								stripeUser.delete(function(deleted) {
									self.redirect('edit');
								});
								return;
							}

							callback(null, stripeUser);
						});
					},
					thumbnail: function(callback) {
						self._user.thumbnail(function(thumbnail) {
							callback(null, thumbnail);
						});
					},
					universityItems: function(callback) {
						University.find({
							order: 'name'
						}, function(universities) {
							async.map(universities, function(university, callback) {
								var isSelected = (self._user.primaryUniversityId == university.id);
								
								if (self.paraglide.isPost) {
									isSelected = (self.request.body.primaryUniversityId == university.id);
								}
								
								callback(null, {
									isSelected: isSelected,
									university: university
								});
							}, function(error, universityItems) {
								callback(null, universityItems);
							});
						});
					}
				}, function(error, results) {
					callback(error, results.facebookUser, results.linkedInUser, results.primaryEmail, results.stripeUser, results.thumbnail, results.universityItems);
				});
			}
		], function(error, facebookUser, linkedInUser, primaryEmail, stripeUser, thumbnail, universityItems) {
			self.render('edit', {
				error: error && error.message,
				facebookUser: facebookUser,
				linkedInUser: linkedInUser,
				primaryEmail: primaryEmail,
				stripeUser: stripeUser,
				thumbnail: thumbnail,
				universities: universityItems
			});
		});
	},
	
	forgotPassword: function() {
		if (this._user) {
			this.redirect();
			return;
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self.paraglide.isPost|| !self.request.body.email) {
					callback(null);
					return;
				}
				
				User.forgotPassword(self.paraglide, self.request.body.email, function(result) {
					if (result.result) {
						self.render('forgot-password-success', {
							email: result.email
						});
						return;
					}
					
					callback(null, result);
				});
			}
		], function(error, result) {
			self.render('forgot-password', {
				email: self.request.body && self.request.body.email || '',
				errors: result && result.errors || null
			});
		});
	},

	login: function() {
		var redirect = null;
		
		if (this.request.body.redirect) {
			redirect = this.request.body.redirect;
		} else if (this.request.query.redirect) {
			redirect = this.request.query.redirect;
		} else if (this.request.get('referrer')) {
			redirect = this.request.get('referrer');
		}

		if (this._user) {
			if (!redirect || redirect.indexOf('login') > -1) {
				redirect = this.url();
			}
			
			this.paraglide.redirectTo(redirect);
			return;
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self.request.query.connect) {
					callback(null, false);
					return;
				}

				if (self.request.query.connect == 'facebook') {
					FacebookUser.login(self.paraglide, redirect, null, function(error) {
						callback(null, true);
					});
					return;
				}
				
				if (self.request.query.connect == 'linkedin') {
					LinkedInUser.login(self.paraglide, redirect, null, function(error) {
						callback(null, true);
					});
					return;
				}
				
				callback(null, false);
			},
			function(loggedIn, callback) {
				if (loggedIn) {
					callback(null);
					return;
				}
				
				if (!self.paraglide.isPost) {
					callback(null);
					return;
				}
				
				var email = (self.request.body.email && self.request.body.email.length > 0) ? self.request.body.email : '';
				var password = (self.request.body.password && self.request.body.password.length > 0) ? self.request.body.password : '';
				User.tryLogin(self.paraglide, email, password, redirect, function(error) {
					callback(error);
				});
			}
		], function(error) {
			self.render('login', {
				errors: error ? [error] : null,
				isLoginProcess: true,
				redirect: redirect
			});
		});
	},
	
	logout: function() {
		if (!this._user) {
			this.redirect();
			return;
		}
		
		this._user.logout(this.paraglide);
	},
	
	profile: function() {
		if (!this._user) {
			this.redirect();
			return;
		}

		this.paraglide.redirect('users', null, this._user.id);
	},

	projects: function() {
		if (!this._user) {
			this.redirect();
			return;
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				self._user.url(self.paraglide, function(url) {
					callback(null, url);
				});
			},
			function(url, callback) {
				async.parallel({
					backed: function(callback) {
						self._user.projectBackers(function(projectBackers) {
							async.map(projectBackers, function(projectBacker, callback) {
								projectBacker.project(function(project) {
									if (!project) {
										callback(null, null);
										return;
									}
							
									project.calculateFields(['isOtherUniversity', 'owner', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
										results.owner.url(self.paraglide, function(url) {
											callback(null, {
												isOtherUniversity: results.isOtherUniversity,
												owner: results.owner,
												ownerUrl: url,
												project: project,
												projectUrl: results.url,
												thumbnail: results.thumbnail
											});
										});
									});
								});
							}, function(error, projects) {
								callback(null, _.compact(projects));
							});
						});
					},
					started: function(callback) {
						self._user.projects(function(projects) {
							async.map(projects, function(project, callback) {
								project.calculateFields(['isOtherUniversity', 'thumbnail', 'url'], {paraglide: self.paraglide}, function(error, results) {
									callback(null, {
										isOtherUniversity: results.isOtherUniversity,
										owner: self._user,
										ownerUrl: url,
										project: project,
										projectUrl: results.url,
										thumbnail: results.thumbnail
									});
								});
							}, function(error, projectItems) {
								callback(null, projectItems);
							});
						});
					}
				}, function(error, results) {
					callback(null, results.backed, results.started);
				});
			}
		], function(error, backed, started) {
			self.render('projects', {
				hasOneProject: (started.length == 1),
				hasOneProjectBacked: (backed.length == 1),
				projects: started,
				projectsBacked: backed
			});
		});
	},
	
	register: function() {
		var redirect = null;
		
		if (this.request.body.redirect) {
			redirect = this.request.body.redirect;
		} else if (this.request.query.redirect) {
			redirect = this.request.query.redirect;
		} else if (this.request.get('referrer')) {
			redirect = this.request.get('referrer');
		}

		if (this._user) {
			if (!redirect || redirect.indexOf('register') > -1) {
				redirect = this.url();
			}
			
			this.paraglide.redirectTo(redirect);
			return;
		}

		var user = new User();
		var self = this;
		async.waterfall([
			function(callback) {
				University.find({
					conditions: {isActive: true},
					order: 'name'
				}, function(universities) {
					async.map(universities, function(university, callback) {
						var isSelected = (self.paraglide.isPost && self.request.body.primaryUniversityId == university.id);
						callback(null, {
							isSelected: isSelected,
							university: university
						});
					}, function(error, universityItems) {
						callback(null, universityItems);
					});
				});
			},
			function(universityItems, callback) {
				if (!self.paraglide.isPost) {
					callback(null, universityItems);
					return;
				}
				
				user.register(self.paraglide, self.request.body, function(success) {
					if (success) {
						user.login(self.paraglide, redirect);
						return;
					}
					
					callback(null, universityItems);
				});
			},
		], function(error, universityItems) {
			self.render('register', {
				isLoginProcess: true,
				newUser: user,
				redirect: redirect,
				universities: universityItems
			});
		});
	},
	
	registerEquityInvestor: function() {
		var redirect = null;
		
		if (this.request.body.redirect) {
			redirect = this.request.body.redirect;
		} else if (this.request.query.redirect) {
			redirect = this.request.query.redirect;
		} else if (this.request.get('referrer')) {
			redirect = this.request.get('referrer').substr(this.paraglide.longUrl().length);
		}

		if (this._user) {
			if (!redirect || redirect.indexOf('register') > -1 || redirect == '/') {
				redirect = this.paraglide.url('projects');
			}
			
			this.paraglide.redirectTo(redirect);
			return;
		}

		var user = new User();
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self.paraglide.isPost) {
					callback(null);
					return;
				}
				
				user.registerEquityInvestor(self.paraglide, self.request.body, function(success) {
					if (success) {
						user.login(self.paraglide, redirect);
						return;
					}
					
					callback(null);
				});
			},
		], function(error) {
			self.render('register-equity-investor', {
				isLoginProcess: true,
				newUser: user,
				redirect: redirect
			});
		});
	},
	
	resetPassword: function(verificationCode) {
		if (this._user) {
			this.paraglide.redirect();
			return;
		}
		
		if (!verificationCode) {
			this.redirect();
			return;
		}

		var self = this;
		async.waterfall([
			function(callback) {
				User.findByVerificationCode(verificationCode, function(user) {
					if (!user) {
						self.redirect();
						return;
					}
					
					callback(null, user);
				});
			},
			function(user, callback) {
				if (!self.paraglide.isPost) {
					callback(null, user);
					return;
				}
				
				user.resetPassword(self.request.body, function(saved) {
					if (!saved) {
						callback(null, user);
						return;
					}
					
					self.render('reset-password-success');
				});
			}
		], function(error, user) {
			self.render('reset-password', {
				forgottenUser: user
			});
		});
	},
	
	social: function() {
		var self = this;
		async.waterfall([
			function(callback) {
				self._user.facebookUser(function(facebookUser) {
					callback(null, facebookUser);
				});
			},
			function(facebookUser, callback) {
				if (self.request.body.disconnect == 'facebook' && facebookUser) {
					facebookUser.delete(function(deleted) {
//						self.redirect('social');
					});
					return;
				}
				
				callback(null, facebookUser);
			},
			function(facebookUser, callback) {
				if (self.request.query.connect == 'facebook' && !facebookUser) {
					FacebookUser.connect(self.paraglide, null, self._user, function() {
//						self.redirect('social');
//						return;
					});
					return;
				}
				
				callback(null, facebookUser);
			}
		], function(error, facebookUser) {
			self.render('social', {
				facebookUser: facebookUser
			});
		});
	},
	
	stripe: function() {
		var self = this;
		async.waterfall([
			function(callback) {
				self._user.stripeUser(function(stripeUser) {
					callback(null, stripeUser);
				});
			},
			function(stripeUser, callback) {
				if (self.request.body.disconnect && stripeUser) {
					stripeUser.delete(function(deleted) {
						self.redirect('stripe');
					});
					return;
				}
				
				callback(null, stripeUser);
			},
			function(stripeUser, callback) {
				if ((self.request.body.connect || self.request.query.code) && !stripeUser) {
					StripeUser.connect(self.paraglide, null, self._user, function(error, stripeUser) {
						if (error) {
							callback(error);
							return;
						}
						
						self.redirect('stripe');
					});
					return;
				}
				
				callback(null, stripeUser);
			}
		], function(error, stripeUser) {
			if (error) {
				self._user.errors['stripe'] = error.message;
			}
			
			self.render('stripe', {
				stripeUser: stripeUser
			});
		});
	},
	
	verify: function(verificationCode) {
		var self = this;
		
		if (this._user) {
			if (this.request.body && this.request.body.resend) {
				return this._user.sendEmailVerification(this.paraglide, function() {
					self.render('verification-sent');
				});
			}
		
			if (!verificationCode) {
				return this.render('verify');
			}
		}
		
		if (!verificationCode) {
			this.redirect('login');
			return;
		}
		
		User.findByVerificationCode(verificationCode, function(user) {
			if (!user) {
				self.redirect();
				return;
			}
			
			user.isVerified = true;
			user.save(function() {
				self.render('verification-success');
			});
		});
	}
};

module.exports = AccountController;
