var Controller = require('../lib/paraglide/controller');
var User = require('../models/user');

var PagesController = function() {
	Controller.apply(this);
};

PagesController.prototype = {
	index: function() {
		this.paraglide.redirect();
	},
	
	about: function() {
		this.render('about');
	},
	
	howItWorks: function() {
		this.render('how-it-works');
	},
	
	investorTerms: function() {
		this.render('terms-investor');
	},
	
	privacy: function() {
		this.render('privacy');
	},
	
	terms: function() {
		this.render('terms');
	}
};

module.exports = PagesController;
