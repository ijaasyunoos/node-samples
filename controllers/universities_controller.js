var async = require('async');
var Controller = require('../lib/paraglide/controller');

var University = require('../models/university');
var User = require('../models/user');

var UniversitiesController = function() {
	Controller.apply(this);
};

UniversitiesController.routes = {
	':id': {
		action: 'view',
		params: [':id']
	},
	':id/edit': {
		action: 'edit',
		params: [':id']
	}
};

UniversitiesController.prototype = {
	_user: null,

	_getUniversityOrRedirect: function(id, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (!id) {
					callback(true);
					return;
				}
				
				University.find(id, function(university) {
					callback(!university, university);
				});
			},
			function(university, callback) {
				university.canView(self._user, function(canView) {
					callback(!canView, university);
				});
			}
		], function(error, university) {
			if (error) {
				self.redirect();
				return;
			}
			
			callback(university);
		});
	},
	
	_preprocess: function(callback) {
		this._user = this.paraglide.data.user;
		
		if (!this._user || !this._user.isAdmin) {
			this.paraglide.redirect();
			return;
		}
		
		callback();
	},
	
	index: function(id) {
		if (id) {
			this.paraglide.action = 'view';
			return this.view(id);
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self._user) {
					callback(null, false);
					return;
				}
				
				callback(null, self._user.isAdmin);
			},
			function(isAdmin, callback) {
				var conditions = {};
				if (!isAdmin) conditions.isActive = true;
				University.find({
					conditions: conditions,
					order: 'name'
				}, function(universities) {
					async.map(universities, function(university, callback) {
						university.calculateFields(['url'], {paraglide: self.paraglide}, function(error, results) {
							callback(null, {
								university: university,
								universityUrl: results.url,
							});
						});
					}, function(error, universityItems) {
						callback(null, universityItems);
					});
				});
			},
		], function(error, universities) {
			self.render('index', {
				universities: universities
			});
		});
	},
	
	create: function() {
		if (!this._user || !this._user.isAdmin) {
			self.redirect();
			return;
		}
	
		var university = new University();
		var self = this;
		async.waterfall([
			function(callback) {
				if (!self.paraglide.isPost) {
					callback(null);
					return;
				}
				
				university.setValues(self.request.body, {
					booleans: ['isActive']
				}, function() {
					university.save(function(saved) {
						if (!saved) {
							callback(null);
							return;
						}
						
						university.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			}
		], function(error) {
			self.render('edit', {
				university: university
			});
		});
	},

	delete: function(id) {
		if (!this._user || !this._user.isAdmin) {
			self.redirect();
			return;
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				self._getUniversityOrRedirect(id, function(university) {
					callback(null, university);
					return;
				});
			},
			function(university, callback) {
				university.canEdit(self._user, function(canEdit) {
					callback(!canEdit, university);
				});
			},
			function(university, callback) {
				if (!self.paraglide.isPost || !self.paraglide.request.body.delete) {
					callback(null, university);
					return;
				}
				
				university.delete(function(deleted) {
					if (!deleted) {
						callback(null, university);
						return;
					}
					
					self.redirect();
				});
			},
			function(university, callback) {
				university.url(self.paraglide, function(url) {
					callback(null, university, url);
				});
			}
		], function(error, university, url) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('delete', {
				university: university,
				universityUrl: url
			});
		});
	},
	
	edit: function(id) {
		if (!this._user || !this._user.isAdmin) {
			self.redirect();
			return;
		}
		
		var self = this;
		async.waterfall([
			function(callback) {
				self._getUniversityOrRedirect(id, function(university) {
					callback(null, university);
					return;
				});
			},
			function(university, callback) {
				university.canEdit(self._user, function(canEdit) {
					callback(!canEdit, university);
				});
			},
			function(university, callback) {
				university.url(self.paraglide, function(url) {
					callback(null, university, url);
				});
			},
			function(university, url, callback) {
				if (!self.paraglide.isPost) {
					callback(null, university, url);
					return;
				}
				
				university.setValues(self.request.body, {
					booleans: ['isActive']
				}, function() {
					university.save(function(saved) {
						if (!saved) {
							callback(null, university, url);
							return;
						}

						university.url(self.paraglide, function(url) {
							self.paraglide.redirectTo(url);
						});
					});
				});
			},
			
		], function(error, university, url) {
			if (error) {
				self.redirect();
				return;
			}
			
			self.render('edit', {
				university: university,
				universityUrl: url
			});
		});
	},

	view: function(id) {
		var self = this;
		async.waterfall([
			function(callback) {
				University.findBySlug(id, function(university) {
					callback(null, university);
				});
			},
			function(university, callback) {
				if (university) {
					callback(null, university);
					return;
				}
				
				self._getUniversityOrRedirect(id, function(university) {
					callback(null, university);
				});
			},
			function(university, callback) {
				university.calculateFields(['canEdit', 'url'], {
					paraglide: self.paraglide,
					user: self._user
				}, function(error, results) {
					callback(error, university, results.canEdit, results.url);
				});
			}
		], function(error, university, canEdit, url) {
			self.render('view', {
				canEdit: canEdit,
				university: university,
				universityUrl: url
			});
		});
	}
};

module.exports = UniversitiesController;
