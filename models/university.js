var _ = require('underscore');
var async = require('async');
var Paragon = require('../lib/classes/paragon/paragon');
var Project = require('./project');
var ProjectBacker = require('./project_backer');
var User = require('./user');

var University = function() {
	Paragon.apply(this);
};

University._table = 'universities';

University.validations = {
	dateActive: {},
	dateCreated: {},
	dateUpdated: {},
	
	isActive: {boolean: true},
	
	numProjects: {integer: true, min: 0},
	numPendingProjects: {integer: true, min: 0},
	numCompletedProjects: {integer: true, min: 0},
	numSuccessfulProjects: {integer: true, min: 0},
	numSuccessfulProjectsPercentage: {integer: true, min: 0},
	numUnsuccessfulProjects: {integer: true, min: 0},
	
	amountGoal: {integer: true, min: 0},
	amountRaised: {integer: true, min: 0},
	amountGoalCompleted: {integer: true, min: 0},
	amountRaisedCompleted: {integer: true, min: 0},
	amountRaisedCompletedPercentage: {integer: true, min: 0},

	slug: {required: true},
	name: {required: true},
	description: {required: true}
};

University.findBySlug = function(slug, callback) {
	University.findOne({
		conditions: {slug: slug}
	}, function(university) {
		callback(university);
	});
};


University.prototype = {
	id: null,
	
	dateActive: new Paragon.DatetimeField(),
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	
	isActive: false,
	
	numProjects: 0,
	numPendingProjects: 0,
	numCompletedProjects: 0,
	numSuccessfulProjects: 0,
	numSuccessfulProjectsPercentage: 0,
	numUnsuccessfulProjects: 0,
	
	amountGoal: 0,
	amountRaised: 0,
	amountGoalCompleted: 0,
	amountRaisedCompleted: 0,
	amountRaisedCompletedPercentage: 0,
	
	slug: null,
	name: null,
	description: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	user: function(callback) {
		if (this._relationshipInstances.user) {
			callback(this._relationshipInstances.user);
			return;
		}
		
		var self = this;
		User.findOne({
			conditions: {primaryUniversityId: this.id}
		}, function(user) {
			self._relationshipInstances.user = user;
			callback(user);
		});
	},

/*
	users: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.users) {
			callback(this._relationshipInstances.users);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(Project._hasMany.users.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.projectId = this.id;
		if (!params.order && Project._hasMany.users.order) params.order = Project._hasMany.users.order;
		
		var self = this;
		ProjectUser.find({
			conditions: params.conditions,
			order: params.order
		}, function(users) {
			if (_.size(params) == 0) {
				self._relationshipInstances.users = users;
			}
			
			if (single) {
				callback(users[0] || null);
				return;
			}
			
			callback(users);
		});
	},
*/
	
	/* END magic methods */

/*
	numPendingProjects: {integer: true, min: 0},
	numCompletedProjects: {integer: true, min: 0},
	numSuccessfulProjects: {integer: true, min: 0},
	numSuccessfulProjectsPercentage: {integer: true, min: 0},
	numUnsuccessfulProjects: {integer: true, min: 0},
	
	amountGoal: {integer: true, min: 0},
	amountRaised: {integer: true, min: 0},
	amountGoalCompleted: {integer: true, min: 0},
	amountRaisedCompleted: {integer: true, min: 0},
	amountRaisedCompletedPercentage: {integer: true, min: 0},
*/

	calculateFields: function(fields, params, callback) {
		var self = this;
		var funcs = {};
		
		if (_.indexOf(fields, 'canEdit') > -1) {
			funcs.canEdit = function(callback) {
				self.canEdit(params.user, function(canEdit) {
					callback(null, canEdit);
				});
			};
		}

		if (_.indexOf(fields, 'url') > -1) {
			funcs.url = function(callback) {
				self.url(params.paraglide, function(url) {
					callback(null, url);
				});
			}
		}
		
		async.parallel(funcs, function(error, results) {
			callback(error, results);
		});
	},

	canEdit: function(user, callback) {
		if (!user) {
			callback(false);
			return;
		}
		
		callback(user.isAdmin);
	},
	
	canView: function(user, callback) {
		var now = (new Date()).getTime();
		
		if (this.isActive || (this.dateActive && this.dateActive.getTime() <= now)) {
			callback(true);
			return;
		}
		
		this.canEdit(user, callback);
	},
	
	isActiveNow: function() {
		return (
			this.isActive
			&& this.dateActive
			&& this.dateActive.getTime() <= (new Date()).getTime()
		);
	},
	
	recalculate: function(callback) {
		var self = this;
		async.parallel({
			numProjects: function(callback) {
				self.total('projects', function(total) {
					callback(total);
				});
			},
			numPendingProjects: function(callback) {
				self.total('projects', {
					isCompleted: false
				}, function(total) {
					callback(total);
				});
			},
			numCompletedProjects: function(callback) {
				self.total('projects', {
					isCompleted: true
				}, function(total) {
					callback(total);
				});
			},
			numSuccessfulProjects: function(callback) {
				self.total('projects', {
					isSuccessful: true
				}, function(total) {
					callback(total);
				});
			},
		}, function(error, results) {
			self.numProjects = results.numProjects;
			self.numPendingProjects = results.numPendingProjects;
			callback();
		});
	},
	
	// action and actionId are optional
	url: function(paraglide, action, actionId, callback) {
		if (typeof actionId == 'function') {
			callback = actionId;
			actionId = null;
		}
		
		if (typeof action == 'function') {
			callback = action;
			action = null;
		}
		
		var url = paraglide.url('universities', null, [this.slug, action, actionId]);
		callback(url);
	}
};

Paragon.registerModel(University);
module.exports = University;
