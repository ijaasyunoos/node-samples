var async = require('async');
var FacebookClient = require('facebook-client').FacebookClient;
var Paragon = require('../lib/classes/paragon/paragon');

var facebookConfig = {};

var Facebook = function(config) {
	this.facebook = new FacebookClient(config.appId, config.secret);
	this.request = config.request;
};

Facebook.prototype = {
	facebook: null,
	request: null,
	
	api: function(path, params, method, callback) {
		var sessionCallback = this.facebook.getSessionByRequestHeaders(this.request.headers);
		
		if (!sessionCallback) {
			callback(true);
			return;
		}
		
		sessionCallback(function(session) {
			if (!session) {
				callback(true);
				return;
			}
			
			var graphCallback = session.graphCall(path, params, method);
			
			if (!graphCallback) {
				callback(true);
				return;
			}
			
			graphCallback(function(result) {
				callback(null, result);
			});
		});
	}
};

var FacebookUser = function() {
	Paragon.apply(this);
};

FacebookUser._table = 'facebook_users';

FacebookUser._belongsTo = {
	user: 'User'
};

FacebookUser._getApi = function(paraglide) {
	var config = {};
	config.appId = facebookConfig.appId;
	config.secret = facebookConfig.secret;
	config.request = paraglide.request;
	return new Facebook(config);
};

FacebookUser.auth = function(paraglide, params, callback) {
	var facebook = FacebookUser._getApi(paraglide);
	facebook.api('/me', params, null, function(error, me) {
		if (error || !me) {
			callback(error || !me || !me.id);
			return;
		}
		
		callback(me);
	});
};

FacebookUser.connect = function(paraglide, params, user, callback) {
	var facebook = FacebookUser._getApi(paraglide);
	if (!params) params = {};
	async.waterfall([
		function(callback) {
			facebook.api('/me', params, null, function(error, me) {
				if (error || !me) {
					callback(error || !me || !me.id);
					return;
				}
				
				callback(null, me);
			});
		},
		function(me, callback) {
			FacebookUser.findByUid(me.id, function(facebookUser) {
				callback(null, me, facebookUser);
			});
		},
		function(me, facebookUser, callback) {
			if (!facebookUser || !facebookUser.userId) {
				callback(null, me, facebookUser)
				return;
			}

			facebookUser.user(function(existingUser) {
				if (existingUser && user && existingUser.id == user.id) {
					callback(true);
					return;
				}
				
				if (!user) {
					user = existingUser;
				}
				
				callback(null, me, facebookUser);
			});
		},
		function(me, facebookUser, callback) {
			if (!user && !me.email) {
				callback(true);
				return;
			}
			
			if (user) {
				callback(null, me, facebookUser);
				return;
			}
			
			User.findByEmail(me.email, function(existingUser) {
				user = existingUser;
				callback(null, me, facebookUser);
			});
		},
		function(me, facebookUser, callback) {
			if (user) {
				user.dateLastAuth = new Date();
				user.name = me.name;
				user.email = me.email;
				user.isVerified = true;
				user.save(function(saved) {
					callback(null, me, facebookUser);
				});
				return;
			}
			
			params.name = me.name;
			params.email = me.email;
			params.emailVerify = me.email;
			params.password = '1';
			params.passwordVerify = '1';
			user = new User();
			user.register(paraglide, params, function(registered) {
				if (!registered) {
					callback(true);
					return;
				}
				
				user.isVerified = true;
				user.save(function(saved) {
					callback(null, me, facebookUser);
				});
			});
		},
		function(me, facebookUser, callback) {
			if (!facebookUser) {
				facebookUser = new FacebookUser();
				facebookUser.userId = user.id;
				facebookUser.uid = me.id;
				facebookUser.save(function(saved) {
					callback(null);
				});
				return;
			}
			
			if (facebookUser.userId == 0) {
				facebookUser.userId = user.id;
				facebookUser.save(function(saved) {
					callback(null);
				});
				return;
			}
			
			callback(null, user);
		}
	], function(error) {
		if (error) {
			callback(false);
			return;
		}
		
		callback(user);
	});
};

FacebookUser.findByUid = function(uid, callback) {
	FacebookUser.findOne({
		conditions: {uid: uid}
	}, function(facebookUser) {
		callback(facebookUser);
	});
};

FacebookUser.login = function(paraglide, redirect, params, callback) {
	if (typeof redirect == 'undefined') redirect = true;
	if (!params) params = {};
	FacebookUser.connect(paraglide, params, null, function(user) {
		if (!user) {
			callback(false);
			return;
		}
		
		user.login(paraglide, redirect);
	});
};

FacebookUser.setApiConfig = function(appId, secret) {
	facebookConfig = {
		appId: appId,
		secret: secret
	};
};

FacebookUser.prototype = {
	id: null,
	userId: null,
	
	dateCreated: null,
	
	uid: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	user: function(callback) {
		User.find(this.userId, function(user) {
			callback(user);
		});
	},
	
	/* END magic methods */
	
	friends: function(paraglide, params, callback) {
		var facebook = FacebookUser._getApi(paraglide);
		facebook.api('/me/friends', params, null, function(error, friends) {
			callback(friends.data);
		});
	},
	
	isFriendsWith: function(facebookUser, callback) {
		callback(false);
	},
	
	isLoggedIn: function(paraglide, callback) {
		var facebook = FacebookUser._getApi(paraglide);
		facebook.api('/me', null, null, function(error, me) {
			callback(!!me);
		});
	},
	
	photoUrl: function(type) {
		var url = 'http://graph.facebook.com';
		url += '/' + this.uid + '/picture';
		url += '?type=' + (type || 'normal');
		return url;
	},
	
	publish: function(paraglide, message, params, callback) {
		var facebook = FacebookUser._getApi(paraglide);
		facebook.api('/' + this.uid + '/feed', params, 'POST', function(error, result) {
			callback(!error);
		});
	}
};

Paragon.registerModel(FacebookUser);
module.exports = FacebookUser;
var User = require('./user');