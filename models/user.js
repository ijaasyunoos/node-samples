var _ = require('underscore');
var async = require('async');
var md5 = require('MD5');

var ImageManipulator = require('../lib/helpers/image_manipulator');
var Mailer = require('../lib/helpers/mailer');
var Paragon = require('../lib/classes/paragon/paragon');
var Pay = require('../lib/classes/pay/pay');
var Slug = require('../lib/helpers/slug');
var Validator = require('../lib/classes/paragon/validator');

var User = module.exports = function() {
	Paragon.apply(this);
};

var FacebookUser = require('./facebook_user');
var LinkedInUser = require('./linkedin_user');
var Project = require('./project');
var ProjectBacker = require('./project_backer');
var StoredImage = require('./stored_image');
var StripeUser = require('./stripe_user');
var UserEmail = require('./user_email');

User._table = 'users';

User._belongsTo = {
	primaryEmail: 'UserEmail'
};

User._hasAndBelongsToMany = {
	roles: {
		class: 'UserRole',
		table: 'user_role_assignments',
		primaryKey: 'userId',
		foreignKey: 'userRoleId',
		order: 'name'
	}
};

User._hasMany = {
	emails: 'UserEmail',
	projects: 'Project',
	projectBackers: 'ProjectBacker'
};

User._hasOne = {
	facebookUser: 'FacebookUser',
		image: {
		'class': 'StoredImage',
		primaryKey: 'parentId',
		conditions: {
			parentType: 'User',
			type: 'User'
		}
	},
	stripeUser: 'StripeUser'
};

User.validations = {
	primaryEmailId: {integer: true, min: 0},
	primaryUniversityId: {integer: true, min: 0},

	dateCreated: {},
	dateUpdated: {},
	dateLastLoggedIn: {datetime: true},
	dateOfBirth: {datetime: true},
	
	isActive: {boolean: true},	
	isAdmin: {boolean: true},
	isVerified: {boolean: true},
	
	investorRequirementSingleIncome: {boolean: true},
	investorRequirementJointIncome: {boolean: true},
	investorRequirementNetWorth: {boolean: true},
	investorRequirementFinancialRepresentative: {boolean: true},
	investorRequirementAccredited: {boolean: true},
	
	slug: {},
	name: {required: true, maxlength: 255},
	password: {required: true, maxlength: 255},
	paymentType: {},
};

User._baseConvert = function(numString, fromBase, toBase) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
	var toString = chars.substr(0, toBase);
	var length = numString.length;
	var number = [];
	var result = '';
	
	for (var i = 0; i < length; i++) {
		number[i] = chars.indexOf(numString.charAt(i));
	}
		
	do {
		var divide = 0;
		var newLength = 0;
		
		for (var i = 0; i < length; i++) {
			divide = divide * fromBase + number[i];
			
			if (divide >= toBase) {
				number[newLength++] = parseInt(divide / toBase);
				divide = divide % toBase;
			} else if (newLength > 0) {
				number[newLength++] = 0;
			}
		}
		
		length = newLength;
		result = toString.charAt(divide) + result;
	}
	while (newLength != 0);
	
	return result;
};

User._verificationCodeData = function(verificationCode, callback) {
	var base17 = User._baseConvert(verificationCode, 36, 30);
	var string = User._baseConvert(base17, 17, 36);

	if (string.substr(0, 'verify'.length) != 'verify') {
		callback(null);
		return;
	}
	
	string = string.substr('verify'.length);
	var parts = string.split('email', 2);
	
	if (parts.length == 1) {
		callback(null);
		return;
	}
	
	var userId = parts[0];
	var emailId = parts[1];
	async.parallel({
		user: function(callback) {
			User.find(userId, function(user) {
				callback(null, user)
			});
		},
		email: function(callback) {
			UserEmail.find(emailId, function(email) {
				callback(null, email);
			});
		}
	}, function(error, results) {
		if (error) {
			callback(null);
			return;
		}
		
		callback({
			user: results.user,
			email: results.email
		});
	});
};

User.findByEmail = function(email, callback) {
	async.waterfall([
		function(callback) {
			UserEmail.findOne({
				conditions: {email: email}
			}, function(userEmail) {
				callback(null, userEmail);
			});
		},
		function(userEmail, callback) {
			if (!userEmail) {
				callback(true);
				return;
			}
			
			User.find(userEmail.userId, function(user) {
				callback(null, user);
			});
		}
	], function(error, user) {
		if (error) {
			callback(false);
			return;
		}
		
		callback(user);
	});
};

User.findBySlug = function(slug, callback) {
	User.findOne({
		conditions: {slug: slug}
	}, callback);
};

User.findByVerificationCode = function(verificationCode, callback) {
	User._verificationCodeData(verificationCode, function(data) {
		callback(data && data.user || null);
	});
};

User.forgotPassword = function(paraglide, email, callback) {
	var result = {
		errors: {},
		result: false
	};
	
	if (!Validator.checkEmail(email)) {
		result.errors.email = 'Please enter a valid email address';
		callback(result);
		return;
	}
	
	async.waterfall([
		function(callback) {
			User.findByEmail(email, function(user) {
				if (!user) {
					result.errors.email = 'We couldn\'t find an account with that email address';
					callback(true);
					return;
				}
				
				callback(null, user);
			});
		},
		function(user, callback) {
			user.primaryEmail(function(primaryEmail) {
				if (!primaryEmail) {
					callback(true);
					return;
				}
				
				callback(null, user, primaryEmail);
			});
		},
		function(user, primaryEmail, callback) {
			user.generateVerificationCode(primaryEmail, function(verificationCode) {
				callback(null, user, verificationCode);
			});
		},
		function(user, verificationCode, callback) {
			paraglide.render('account/email.send-password', {
				company: paraglide.getClass().config.company.main.name,
				email: email,
				forgotUser: user,
				verificationCode: verificationCode
			}, function(message) {
				callback(null, user, message);
			});
		},
		function(user, message, callback) {
			Mailer.send({
				to: email,
				subject: paraglide.getClass().config.company.main.name + ' - Reset Password',
				message: message
			}, function(sent) {
				callback(null);
			});
		}
	], function(error) {
		if (error) {
			callback(result);
			return;
		}
		
		result.result = true;
		result.email = email;
		callback(result);
	});
};

User.hashPassword = function(password) {
	var reverse = password.split('').reverse().join('');
	return md5(md5(password) + password + reverse + md5(reverse));
};

User.loggedInUser = function(paraglide, callback) {
	if (!paraglide.request.cookies.userId || !paraglide.request.cookies.password) {
		callback(false);
		return;
	}
	
	async.waterfall([
		function(callback) {
			User.find(paraglide.request.cookies.userId, function(user) {
				if (!user || md5(user.password) != paraglide.request.cookies.password) {
					callback(true);
					return;
				}
				
				callback(null, user);
			});
		}/*,
		function(user, callback) {
			user.facebookUser(function(facebookUser) {
				if (!facebookUser) {
					callback(null, user, facebookUser);
					return;
				}
				
				facebookUser.isLoggedIn(paraglide, function(isLoggedIn) {
					if (!isLoggedIn) {
						callback(true);
						return;
					}
					
					callback(null, user, facebookUser);
				});
			});
		}
		*/
	], function(error, user) {
		if (error) {
			callback(false);
			return;
		}
		
		callback(user);
	});
};

User.requireLogin = function(paraglide, callback) {
	User.loggedInUser(paraglide, function(user) {
		if (user) {
			callback();
			return;
		}
		
		paraglide.redirect('account', 'login', null, {
			redirect: paraglide.request.originalUrl
		});
	});
};

User.tryLogin = function(paraglide, email, password, redirect, callback) {
	if (!email) {
		callback('Please enter an "email address"');
		return;
	}
	
	if (!password) {
		callback('Please enter a "password"');
		return;
	}
	
	User.findByEmail(email, function(user) {
		if (!user) {
			callback('We could not find an account with that email address');
			return;
		}
		
		if (!user.isActive) {
			callback('We could not find an active account with that email address');
			return;
		}
		
		var hashedPassword = User.hashPassword(password);
		
		if (hashedPassword != user.password) {
			callback('You entered an incorrect password');
			return;
		}
		
		user.login(paraglide, redirect);
	});
};

User.prototype = {
	id: null,
	primaryEmailId: 0,
	primaryUniversityId: 0,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	dateLastLoggedIn: new Paragon.DatetimeField(),
	dateOfBirth: new Paragon.DatetimeField(),

	isActive: false,
	isAdmin: false,
	isVerified: false,
	
	investorRequirementSingleIncome: false,
	investorRequirementJointIncome: false,
	investorRequirementNetWorth: false,
	investorRequirementFinancialRepresentative: false,
	investorRequirementAccredited: false,
	
	slug: '',
	name: null,
	password: null,
	paymentType: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	facebookUser: function(callback) {
		if (this._relationshipInstances.facebookUser) {
			callback(this._relationshipInstances.facebookUser);
			return;
		}
		
		var self = this;
		FacebookUser.findOne({
			conditions: {userId: this.id}
		}, function(facebookUser) {
			self._relationshipInstances.facebookUser = facebookUser;
			callback(facebookUser);
		});
	},
	
	image: function(callback) {
		if (this._relationshipInstances.image) {
			callback(this._relationshipInstances.image);
			return;
		}
		
		var self = this;
		StoredImage.findOne({
			conditions: {
				parentId: this.id,
				parentType: 'User',
				type: 'User'
			},
		}, function(image) {
			self._relationshipInstances.image = image;
			callback(image);
		});
	},
	
	linkedInUser: function(callback) {
		if (this._relationshipInstances.linkedInUser) {
			callback(this._relationshipInstances.linkedInUser);
			return;
		}
		
		var self = this;
		LinkedInUser.findOne({
			conditions: {userId: this.id}
		}, function(linkedInUser) {
			self._relationshipInstances.linkedInUser = linkedInUser;
			callback(linkedInUser);
		});
	},
	
	primaryEmail: function(callback) {
		if (this._relationshipInstances.primaryEmail) {
			callback(this._relationshipInstances.primaryEmail);
			return;
		}
		
		var self = this;
		UserEmail.find(this.primaryEmailId, function(primaryEmail) {
			self._relationshipInstances.primaryEmail = primaryEmail;
			callback(primaryEmail);
		});
	},
	
	primaryUniversity: function(callback) {
		if (this._relationshipInstances.primaryUniversity) {
			callback(this._relationshipInstances.primaryUniversity);
			return;
		}
		
		var self = this;
		UserEmail.find(this.primaryUniversityId, function(primaryUniversity) {
			self._relationshipInstances.primaryUniversity = primaryUniversity;
			callback(primaryUniversity);
		});
	},
	
	projectBackers: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.projectBackers) {
			callback(this._relationshipInstances.projectBackers);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(User._hasMany.projectBackers.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.userId = this.id;
		if (!params.order && User._hasMany.projectBackers.order) params.order = User._hasMany.projectBackers.order;
		
		var self = this;
		ProjectBacker.find({
			conditions: params.conditions,
			order: params.order
		}, function(backers) {
			if (_.size(params) == 0) {
				self._relationshipInstances.projectBackers = backers;
			}
			
			if (single) {
				callback(backers[0] || null);
				return;
			}
			
			callback(backers);
		});
	},
	
	projects: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.projects) {
			callback(this._relationshipInstances.projects);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(User._hasMany.projects.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.userId = this.id;
		if (!params.order && User._hasMany.projects.order) params.order = User._hasMany.projects.order;
		
		var self = this;
		Project.find({
			conditions: params.conditions,
			order: params.order
		}, function(projects) {
			if (_.size(params) == 0) {
				self._relationshipInstances.projects = projects;
			}
			
			if (single) {
				callback(projects[0] || null);
				return;
			}
			
			callback(projects);
		});
	},
	
	stripeUser: function(callback) {
		if (this._relationshipInstances.stripeUser) {
			callback(this._relationshipInstances.stripeUser);
			return;
		}
		
		var self = this;
		StripeUser.findOne({
			conditions: {userId: this.id}
		}, function(stripeUser) {
			self._relationshipInstances.stripeUser = stripeUser;
			callback(stripeUser);
		});
	},
	
	/* END magic methods */
	
	get firstName() {
		if (!this.name) {
			return '';
		}
		
		return this.name.split(' ').shift();
	},
	
	get lastName() {
		if (!this.name) {
			return '';
		}
		
		return this.name.split(' ').pop();
	},
	
	canView: function(user, callback) {
		callback(this.isActive);
	},
	
	delete: function(callback) {
		this.emails(function(emails) {
			async.forEach(emails, function(email, callback) {
				email.delete(callback);
			}, function(error) {
				callback(!error);
			});
		});
	},
	
	generateVerificationCode: function(email, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (email) {
					callback(null, email);
					return;
				}
				
				self.primaryEmail(function(primaryEmail) {
					callback(null, primaryEmail);
				});
			}
		], function(error, email) {
			if (!email) {
				callback(false);
				return;
			}
			
			var string = 'verify' + self.id + 'email' + email.id;
			var base17 = User._baseConvert(string, 36, 17);
			var alphanumeric = User._baseConvert(base17, 30, 36);
			callback(alphanumeric);
		});
	},
	
	isAccreditedInvestor: function() {
		if (this.isAdmin) {
			return true;
		}
		
		if (
			!this.investorRequirementSingleIncome
			&& !this.investorRequirementJointIncome
			&& !this.investorRequirementNetWorth
			&& !this.investorRequirementFinancialRepresentative
		) {
			return false;
		}
		
		return this.investorRequirementAccredited;
	},
	
	login: function(paraglide, redirect) {
		if (typeof redirect == 'undefined') redirect = true;
		this.dateLastLoggedIn = new Date();
		var self = this;
		this.save(function(saved) {
			paraglide.response.cookie('userId', self.id);
			paraglide.response.cookie('password', md5(self.password));
			
			if (typeof redirect == 'function') {
				redirect();
				return;
			}
			
			if (typeof redirect == 'string') {
				paraglide.redirectTo(redirect);
				return;
			}
			
			paraglide.redirect('account');
		});
	},
	
	logout: function(paraglide, redirect) {
		if (typeof redirect == 'undefined') redirect = true;
		paraglide.response.clearCookie('userId');
		paraglide.response.clearCookie('password');
		
		if (typeof redirect == 'function') {
			redirect();
			return;
		}
		
		if (typeof redirect == 'string') {
			paraglide.redirectTo(redirect);
			return;
		}
		
		paraglide.redirect();
	},

	register: function(paraglide, data, callback) {
		var userEmail = new UserEmail();
		
		var self = this;
		var save = function() {
			self.isActive = true;
			self.save(function(saved) {
				if (!saved) {
					callback(false);
					return;
				}

				userEmail.save(function(saved) {
					self.primaryEmailId = userEmail.id;
					self.save(function(saved) {
						self.sendEmailVerification(paraglide, false, function() {
							callback(true);
						});
					});
				});
			});
			
		};

		this.setValues(data, null, function() {
			self.password = User.hashPassword(data.password || '');
			self.validate(function(validated) {
				self.validatePasswords(data);

				if (!userEmail.validateEmails(data)) {
					if (userEmail.errors.email) {
						self.errors.email = userEmail.errors.email;
					}

					callback(false);
					return;
				}

				userEmail.email = data.email;
				userEmail.validate(function(validated) {
					if (validated) {
						save();
						return;
					}
					
					if (userEmail.errors.email) {
						self.errors.email = userEmail.errors.email;
					}
					callback(false);
				});
			});
		});
	},
	
	registerEquityInvestor: function(paraglide, data, callback) {
		var self = this;
		this.setValues(data, null, function() {
			if (
				!self.investorRequirementSingleIncome
				&& !self.investorRequirementJointIncome
				&& !self.investorRequirementNetWorth
				&& !self.investorRequirementFinancialRepresentative
			) {
				self.errors.investorRequirement = 'To join as an accredited investor, you must select at least one of the four valid accredited investor requirement checkboxes';
				callback(false);
				return;
			}

			if (!data.investorRequirementAgreeTos) {
				self.errors.investorRequirementAgreeTos = 'You must agree to the Terms of Service in order to join as an accredited investor';
				callback(false);
				return;
			}
		
			if (!self.investorRequirementAccredited) {
				self.errors.investorRequirementAccredited = 'You must check the box qualifying that you are an accredited investor in order to join as an accredited investor';
				callback(false);
				return;
			}
			
			if (!self.dateOfBirth) {
				self.errors.dateOfBirth = 'You must enter your date of birth to join as an accredited investor';
				callback(false);
				return;
			}
			
			var date = (new Date());
			var diff = date - self.dateOfBirth;
			var years = Math.floor(diff / (1000 * 60 * 60 * 24 * 365));

			if (
				years < 18
				|| (
					years == 18
					&& (
						self.dateOfBirth.getMonth() < date.getMonth()
						|| (
							self.dateOfBirth.getMonth() == date.getMonth()
							&& self.dateOfBirth.getDate() > date.getDate()
						)
					)
				)
			) {
				self.errors.dateOfBirth = 'You must be at least 18 years old to join as an accredited investor';
				callback(false);
				return;
			}
		
			self.register(paraglide, data, callback);
		});
	},
	
	resetPassword: function(data, callback) {
		if (typeof data.password == 'undefined' || data.password.length == 0) {
			this.errors.password = 'Please enter the "Password" field';
		}
		
		if (typeof data.passwordVerify == 'undefined' || data.passwordVerify.length == 0) {
			this.errors.passwordVerify = 'Please enter the "Verify Password" field';
		}
		
		if (this.hasErrors()) {
			callback(false);
			return;
		}
		
		if (data.password != data.passwordVerify) {
			this.errors.passwordVerify = 'Your "Password" and "Verification Password" do not match';
			callback(false);
			return;
		}
		
		this.password = User.hashPassword(data.password);
		this.save(function(saved) {
			callback(saved);
		});
	},

	save: function(params, callback) {
		// Supporting polymorphic access
		if (!params) params = {};
		
		if (typeof params == 'function') {
			callback = params;
			params = {};
		}

		var self = this;
		async.waterfall([
			function(callback) {
				self.primaryEmail(function(primaryEmail) {
					callback(null, primaryEmail);
				});
			},
			function(primaryEmail, callback) {
				if (!primaryEmail) {
					callback(null, null);
					return;
				}

				primaryEmail.validate(function(validated) {
					if (!validated) {
						self.errors.email = primaryEmail.errors.email;
						callback(true);
						return;
					}
					callback(null, primaryEmail);
				});
			},
			function(primaryEmail, callback) {
				if (primaryEmail && primaryEmail.hasChanged()) {
					self.isVerified = false;
				}

				Paragon.prototype.save.call(self, function(saved) {
					if (!saved) {
						callback(true, null);
						return;
					}

					if (!primaryEmail) {
						callback(null, null);
						return;
					}

					callback(null, primaryEmail);
				});
			},
			function(primaryEmail, callback) {
				if (!primaryEmail) {
					callback(null, primaryEmail, false);
					return;
				}

				var emailHasChanged = (primaryEmail && primaryEmail.hasChanged());
				primaryEmail.userId = self.id;
				primaryEmail.save(function(saved) {
					callback(null, primaryEmail, emailHasChanged);
				});
			},
			function(primaryEmail, emailHasChanged, callback) {
				if (!emailHasChanged || !params.paraglide) {
					callback(null, primaryEmail);
					return;
				}

				// Dispatch an email
				self.sendEmailVerification(params.paraglide, true, function(mailed) {
					if (!mailed) {
						// @TODO: Some error goes here because the mailer failed
					}
					
					callback(null, primaryEmail);
				});
			},
			function(primaryEmail, callback) {
				if (primaryEmail) {
					if (primaryEmail.id == self.primaryEmailId) {
						callback(null);
						return;
					}

					self.primaryEmailId = primaryEmail.id;
					Paragon.prototype.save.call(self, function(saved) {
						callback(!saved);
						return;
					});
				}

				callback(null);
			}
		], function(error) {
			callback(!error);
		});
	},
	
	sendEmailVerification: function(paraglide, isResend, callback) {
		var self = this;

		this.primaryEmail(function(primaryEmail) {
			if (!primaryEmail) {
				callback(false);
				return;
			}

			self.generateVerificationCode(primaryEmail, function(verificationCode) {
				var emailTemplate = 'account/email.send-verification';
				var templateProperties = {
					company: paraglide.getClass().config.company.main.name,
					email: primaryEmail.email,
					newUser: self,
					verificationCode: verificationCode
				};

				if (isResend) {
					emailTemplate = 'account/email.resend-verification';
				}

				paraglide.render(emailTemplate, templateProperties, function(message) {
					Mailer.send({
						to: primaryEmail.email,
						subject: paraglide.getClass().config.company.main.name + ' - Email Verification',
						message: message
					}, function(mailed) {
						callback(mailed);
					});
				});
			});
		});
	},
	
	setPaymentType: function(params, callback) {
		if (!params || _.size(params) == 0) {
			callback(false);
			return;
		}
		
		var paymentType = new Pay.PaymentType({
			creditCard: {
				name: params.name,
				number: params.creditCardNumber,
				expiration: {
					month: params.expirationMonth,
					year: params.expirationYear
				},
				securityCode: params.securityCode,
			},
			billingAddress: {
				address1: params.address1,
				address2: params.address2,
				city: params.city,
				state: params.state,
				zip: params.zip,
				country: params.country
			}
		});
		
		var self = this;
		paymentType.validate(function(error, validated) {
			if (error) {
				self.errors.paymentType = error.message;
				callback(false);
				return;
			}
			
			paymentType.createHash(function(error, hash) {
				if (error) {
					self.errors.paymentType = error.message;
					callback(false);
					return;
				}
				
				self.paymentType = hash;
				self.save(function(saved) {
					callback(saved);
				});
			});
		});
	},

	setValues: function(vars, params, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				if (typeof vars.email == 'undefined' || vars.email == null || vars.email.length == 0) {
					callback(null);
					return;
				}

				self.primaryEmail(function(primaryEmail) {
					var email = primaryEmail || new UserEmail();
					email.email = vars.email;
					callback(null);
				});
			}
		], function(error) {
			Paragon.prototype.setValues.call(self, vars, params, callback);
		});
	},
	
	thumbnail: function(callback) {
		this.image(function(image) {
			if (image) {
				callback(image);
				return;
			}
			
			var defaultImage = 'images/profile/profile_image.png';
			var thumbnail = new ImageManipulator(defaultImage, function(error) {
				callback(thumbnail);
			});
		});
	},
	
	toOutput: function(callback) {
		var output = Paragon.prototype.toOutput.call(this);
		delete output.dateCreated;
		delete output.dateUpdated;
		delete output.password;
		callback(output);
	},
	
	toString: function() {
		return this.name;
	},
	
	uploadImage: function(data, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				StoredImage.upload(data, 'images/users/', function(image) {
					if (!image) {
						callback(true);
						return;
					}
					
					callback(null, image);
				});
			},
			function(image, callback) {
				self.image(function(existingImage) {
					if (!existingImage) {
						callback(null, image);
						return;
					}
					
					existingImage.delete(function(deleted) {
						callback(null, image);
					});
				});
			},
			function(image, callback) {
				image.parentId = self.id;
				image.parentType = 'User';
				image.type = 'User';
				image.save(function(saved) {
					if (!saved) {
						callback(true);
						return;
					}
					
					callback(null, image);
				});
			}
		], function(error, image) {
			callback(!error && image);
		});
	},
	
	url: function(paraglide, callback) {
		var url = paraglide.url('users', null, this.slug);
		callback(url);
	},

	validate: function(callback) {
		var self = this;
		
		if (this.slug || !this.name) {
			Paragon.prototype.validate.call(self, callback);
			return;
		}
		
		Slug.slugify(this, User, this.name, function(slug) {
			self.slug = slug;
			Paragon.prototype.validate.call(self, callback);
		});
	},

	validatePasswords: function(vars) {
		if (typeof vars.password == 'undefined' || vars.password.length == 0) {
			this.errors.password = 'Please enter the "Password" field';
		} else if (
			typeof vars.password != 'undefined' && vars.password.length > 0
			&& (typeof vars.passwordVerify == 'undefined' || vars.passwordVerify.length == 0)
		) {
			this.errors.passwordVerify = 'Please enter the "Verification Password" field';
		} else if (
			typeof vars.password != 'undefined' && vars.password.length > 0
			&& vars.password != vars.passwordVerify
		) {
			this.errors.password = 'Your "Verification Password" does not match your "Password"';
		}
	
		return !this.hasErrors();
	},
	
	validatePaymentType: function(callback) {
		var paymentType = new Pay.PaymentType({
			hash: this.paymentType
		});
		
		var self = this;
		paymentType.validate(function(error, validated) {
			if (error) {
				self.errors.paymentType = error.message;
				callback(false);
				return;
			}
			
			callback(validated);
		});
	}
};

Paragon.registerModel(User);
