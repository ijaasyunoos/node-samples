var async = require('async');
var querystring = require('querystring');
var request = require('request');
var url = require('url');
var xml2js = require('xml2js');
var Paragon = require('../lib/classes/paragon/paragon');

var linkedInConfig = {};

var LinkedInUser = function() {
	Paragon.apply(this);
};

LinkedInUser._table = 'linkedin_users';

LinkedInUser._belongsTo = {
	user: 'User'
};

LinkedInUser._getApi = function(paraglide) {
	var redirectUrl = paraglide.longUrl('account', 'login');
	return LinkedIn(linkedInConfig.apiKey, linkedInConfig.secretKey, redirectUrl);
};

LinkedInUser.connect = function(paraglide, params, user, callback) {
	if (!params) params = {};
	async.waterfall([
		function(callback) {
			var authUrl = paraglide.longUrl() + paraglide.request.originalUrl;
			var authUrlObj = url.parse(authUrl, true);
			authUrl = authUrlObj.protocol + '//' + authUrlObj.host + authUrlObj.pathname;
			var authUrlQuery = authUrlObj.query;
			delete authUrlQuery.code;
			delete authUrlQuery.state;
			var queryString = querystring.stringify(authUrlQuery);
			
			if (queryString) {
				authUrl += '?' + querystring.stringify(authUrlQuery);
			}

			if (!paraglide.request.query.code) {
				var state = {};
				if (params.redirectUrl) state.redirect_url = params.redirectUrl;
				var connectUrl = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code';
				connectUrl += '&client_id=' + linkedInConfig.apiKey;
				connectUrl += '&redirect_uri=' + encodeURIComponent(authUrl);
				connectUrl += '&state=' + encodeURIComponent(JSON.stringify(state));
				paraglide.redirectTo(connectUrl);
				return;
			}

			request.post('https://www.linkedin.com/uas/oauth2/accessToken', {
				form: {
					code: paraglide.request.query.code,
					grant_type: 'authorization_code',
					redirect_uri: authUrl,
					client_id: linkedInConfig.apiKey,
					client_secret: linkedInConfig.secretKey
				}
			}, function(error, response, body) {
				body = JSON.parse(body);
				
				if (!body.access_token) {
					callback(false);
					return;
				}
				
				callback(null, body.access_token);
			});
		},
		function(accessToken, callback) {
			var url = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)';
			url += '?oauth2_access_token=' + accessToken;
			request.get(url, function(error, response, body) {
				if (error) {
					callback(false);
					return;
				}
				
				xml2js.parseString(body, function(error, person) {
					callback(error || !person || !person.person, person && person.person);
				});
			});
		},
		function(person, callback) {
			LinkedInUser.findByUid(person.id, function(linkedInUser) {
				callback(null, person, linkedInUser);
			});
		},
		function(person, linkedInUser, callback) {
			if (!linkedInUser || !linkedInUser.userId) {
				callback(null, person, linkedInUser)
				return;
			}

			linkedInUser.user(function(existingUser) {
				if (existingUser && user && existingUser.id == user.id) {
					callback(true);
					return;
				}
				
				if (!user) {
					user = existingUser;
				}
				
				callback(null, person, linkedInUser);
			});
		},
		function(person, linkedInUser, callback) {
			if (!user && !person['email-address']) {
				callback(true);
				return;
			}
			
			if (user) {
				callback(null, person, linkedInUser);
				return;
			}
			
			User.findByEmail(person['email-address'], function(existingUser) {
				user = existingUser;
				callback(null, person, linkedInUser);
			});
		},
		function(person, linkedInUser, callback) {
			if (user) {
				user.dateLastAuth = new Date();
				user.name = person['first-name'] + ' ' + person['last-name'];
				user.email = person['email-address'];
				user.isVerified = true;
				user.save(function(saved) {
					callback(null, person, linkedInUser);
				});
				return;
			}
			
			params.name = person['first-name'] + ' ' + person['last-name'];
			params.email = person['email-address'];
			params.emailVerify = person['email-address'];
			params.password = '1';
			params.passwordVerify = '1';
			user = new User();
			user.register(paraglide, params, function(registered) {
				if (!registered) {
					callback(true);
					return;
				}
				
				user.isVerified = true;
				user.save(function(saved) {
					callback(null, person, linkedInUser);
				});
			});
		},
		function(person, linkedInUser, callback) {
			if (!linkedInUser) {
				linkedInUser = new LinkedInUser();
				linkedInUser.userId = user.id;
				linkedInUser.uid = person.id;
				linkedInUser.save(function(saved) {
					callback(null);
				});
				return;
			}
			
			if (linkedInUser.userId == 0) {
				linkedInUser.userId = user.id;
				linkedInUser.save(function(saved) {
					callback(null);
				});
				return;
			}
			
			callback(null, user);
		}
	], function(error) {
		if (error) {
			callback(false);
			return;
		}
		
		callback(user);
	});
};

LinkedInUser.findByUid = function(uid, callback) {
	LinkedInUser.findOne({
		conditions: {uid: uid}
	}, function(linkedInUser) {
		callback(linkedInUser);
	});
};

LinkedInUser.login = function(paraglide, redirect, params, callback) {
	if (typeof redirect == 'undefined') redirect = true;
	if (!params) params = {};
	LinkedInUser.connect(paraglide, params, null, function(user) {
		if (!user) {
			callback(user);
			return;
		}
		
		user.login(paraglide, redirect);
	});
};

LinkedInUser.setApiConfig = function(apiKey, secretKey) {
	linkedInConfig = {
		apiKey: apiKey,
		secretKey: secretKey
	};
};

LinkedInUser.prototype = {
	id: null,
	userId: null,
	
	dateCreated: null,
	
	uid: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	user: function(callback) {
		User.find(this.userId, function(user) {
			callback(user);
		});
	}
	
	/* END magic methods */
};

Paragon.registerModel(LinkedInUser);
module.exports = LinkedInUser;
var User = require('./user');
