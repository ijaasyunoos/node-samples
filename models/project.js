var _ = require('underscore');
var async = require('async');
var url = require('url');

var ImageManipulator = require('../lib/helpers/image_manipulator');
var Paragon = require('../lib/classes/paragon/paragon');
var Paraglide = require('../lib/paraglide/paraglide');
var Slug = require('../lib/helpers/slug');

var Project = module.exports = function() {
	Paragon.apply(this);
};

var ProjectBacker = require('./project_backer');
var ProjectLevel = require('./project_level');
var ProjectUpdate = require('./project_update');
var StoredImage = require('./stored_image');
var University = require('./university');
var User = require('./user');

Project._table = 'projects';

Project._belongsTo = {
	user: 'User'
};

Project._hasMany = {
	backers: {
		'class': 'ProjectBacker',
		order: 'dateCreated'
	},
	levels: {
		'class': 'ProjectLevel',
		order: 'amount'
	},
	updates: {
		'class': 'ProjectUpdate',
		order: '-dateCreated'
	}
};

Project._hasOne = {
	image: {
		'class': 'StoredImage',
		primaryKey: 'parentId',
		conditions: {
			parentType: 'Project',
			type: 'Project'
		}
	}
};

Project.validations = {
	universityId: {required: true, integer: true, min: 1, label: 'University'},
	userId: {required: true, integer: true, min: 1},
	
	dateCreated: {},
	dateUpdated: {},
	dateFundingStarts: {datetime: true, required: true, label: 'Date Funding Starts'},
	dateFundingEnds: {datetime: true, required: true, label: 'Date Funding Ends'},
	dateEstimatedDelivery: {datetime: true, label: 'Date Estimated Delivery'},
	
	isApproved: {boolean: true},
	isCanceled: {boolean: true},
	isCompleted: {boolean: true},
	isEquity: {boolean: true},
	isPrivate: {boolean: true},
	isSubmitted: {boolean: true},
	isSuccessful: {boolean: true},
	isFeatured: {boolean: true},	
	
	amountGoal: {required: true, integer: true, min: 1, label: 'Funding Target'},
	amountRaised: {integer: true},
	amountRaisedPercentage: {integer: true, min: 0},
	numBackers: {integer: true, min: 0},
	
	slug: {required: true},
	title: {required: true},
	subtitle: {required: true},
	location: {required: true},
	description: {required: true},
	background: {required: true},
	purpose: {},
	moreInfo: {label: 'More Info'},
	
	websiteUrl: {label: 'Website URL'},
	presentationUrl: {label: 'Presentation URL'},
	videoUrl: {label: 'Video URL'}
};

Project.findBySlug = function(slug, callback) {
	Project.findOne({
		conditions: {slug: slug}
	}, function(project) {
		callback(project);
	});
};

Project.processTransactions = function(paraglide) {
	console.log('Begin processing transactions.');
	async.waterfall([
		function(callback) {
			console.log('Find completed successful projects...');
			Project.find({
				conditions: {
					isCompleted: true,
					isSuccessful: true
				}
			}, function(projects) {
				console.log('Found ' + projects.length + ' projects.');
				callback(null, projects);
			});
		},
		function(projects, callback) {
			async.forEachSeries(projects, function(project, callback) {
				console.log('Processing transactions for project #' + project.id + ' "' + project.title + '"...');
				project.charge(paraglide, function() {
					console.log('Processed.');
					callback();
				});
			}, function() {
				callback();
			});
		}
	], function(error) {
		console.log('Finished processing transactions.');
		process.exit();
	});
};

Project.prototype = {
	id: 0,
	universityId: 0,
	userId: 0,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	dateFundingStarts: new Paragon.DatetimeField(),
	dateFundingEnds: new Paragon.DatetimeField(),
	dateEstimatedDelivery: new Paragon.DatetimeField(),
	
	isApproved: false,
	isCanceled: false,
	isCompleted: false,
	isEquity: false,
	isPrivate: false,
	isSubmitted: false,
	isSuccessful: false,
	isFeatured: false,
	
	amountGoal: 0,
	amountRaised: 0,
	amountRaisedPercentage: 0,
	numBackers: 0,
	
	slug: null,
	title: null,
	subtitle: null,
	location: null,
	description: null,
	background: null,
	purpose: null,
	moreInfo: null,
	
	websiteUrl: null,
	presentationUrl: null,
	videoUrl: null,
	
	get duration() {
		if (
			!this.dateFundingStarts
			|| !this.dateFundingEnds
			|| isNaN(this.dateFundingStarts.getTime())
			|| isNaN(this.dateFundingEnds.getTime())
		) {
			return null;
		}

		var seconds = (this.dateFundingEnds.getTime() - this.dateFundingStarts.getTime()) / 1000;
		return Math.round(seconds / (60 * 60 * 24));
	},
	
	set duration(days) {
		if (!this.dateFundingStarts) {
			return;
		}

		this.dateFundingEnds = this.dateFundingStarts.getTime() + (1000 * 60 * 60 * 24 * days);
	},
	
	/* TODO: Move these to magic methods in Paragon */
	
	backers: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.backers) {
			callback(this._relationshipInstances.backers);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(Project._hasMany.backers.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.projectId = this.id;
		if (!params.order && Project._hasMany.backers.order) params.order = Project._hasMany.backers.order;
		
		var self = this;
		ProjectBacker.find({
			conditions: params.conditions,
			order: params.order
		}, function(backers) {
			if (_.size(params) == 0) {
				self._relationshipInstances.backers = backers;
			}
			
			if (single) {
				callback(backers[0] || null);
				return;
			}
			
			callback(backers);
		});
	},
	
	image: function(callback) {
		if (this._relationshipInstances.image) {
			callback(this._relationshipInstances.image);
			return;
		}
		
		var self = this;
		StoredImage.findOne({
			conditions: {
				parentId: this.id,
				parentType: 'Project',
				type: 'Project'
			},
		}, function(image) {
			self._relationshipInstances.image = image;
			callback(image);
		});
	},
	
	levels: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};

		if (_.size(params) == 0 && this._relationshipInstances.levels) {
			callback(this._relationshipInstances.levels);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(Project._hasMany.levels.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.projectId = this.id;
		if (!params.order && Project._hasMany.levels.order) params.order = Project._hasMany.levels.order;
		
		var self = this;
		ProjectLevel.find({
			conditions: params.conditions,
			order: params.order
		}, function(levels) {
			if (_.size(params) == 0) {
				self._relationshipInstances.levels = levels;
			}
			
			if (single) {
				callback(levels[0] || null);
				return;
			}
			
			callback(levels);
		});
	},
	
	university: function(callback) {
		if (this._relationshipInstances.university) {
			callback(this._relationshipInstances.university);
			return;
		}
		
		var self = this;
		University.find(this.universityId, function(university) {
			self._relationshipInstances.university = university;
			callback(university);
		});
	},
	
	updates: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};
		
		if (_.size(params) == 0 && this._relationshipInstances.updates) {
			callback(this._relationshipInstances.updates);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		_.each(Project._hasMany.updates.conditions, function(value, key) {
			if (typeof params.conditions[key] == 'undefined') {
				params.conditions[key] = value;
			}
		});
		params.conditions.projectId = this.id;
		if (!params.order && Project._hasMany.updates.order) params.order = Project._hasMany.updates.order;
		
		var self = this;
		ProjectUpdate.find({
			conditions: params.conditions,
			order: params.order
		}, function(updates) {
			if (_.size(params) == 0) {
				self._relationshipInstances.updates = updates;
			}
			
			if (single) {
				callback(updates[0] || null);
				return;
			}
			
			callback(updates);
		});
	},
	
	user: function(callback) {
		if (this._relationshipInstances.user) {
			callback(this._relationshipInstances.user);
			return;
		}
		
		var self = this;
		User.find(this.userId, function(user) {
			self._relationshipInstances.user = user;
			callback(user);
		});
	},
	
	/* END magic methods */
	
	calculateFields: function(fields, params, callback) {
		var self = this;
		var funcs = {};
		
		if (_.indexOf(fields, 'canEdit') > -1) {
			funcs.canEdit = function(callback) {
				self.canEdit(params.user, function(canEdit) {
					callback(null, canEdit);
				});
			};
		}

		if (_.indexOf(fields, 'canManage') > -1) {
			funcs.canManage = function(callback) {
				self.canManage(params.user, function(canManage) {
					callback(null, canManage);
				});
			};
		}
		
		if (_.indexOf(fields, 'fundUrl') > -1) {
			funcs.fundUrl = function(callback) {
				self.url(params.paraglide, 'fund', function(url) {
					callback(null, url);
				});
			};
		}
		
		if (_.indexOf(fields, 'isOtherUniversity') > -1) {
			funcs.isOtherUniversity = function(callback) {
				self.university(function(university) {
					var isOtherUniversity = (
						university
						&& params.paraglide
						&& params.paraglide.data.whitelabelUniversity
						&& params.paraglide.data.whitelabelUniversity.id != university.id
					);
					callback(null, isOtherUniversity);
				});
			};
		}

		if (_.indexOf(fields, 'levels') > -1) {
			funcs.levels = function(callback) {
				self.levels(function(levels) {
					callback(null, levels);
				});
			};
		}
		
		if (_.indexOf(fields, 'numUpdates') > -1) {
			funcs.numUpdates = function(callback) {
				/*
				self.total('updates', function(total) {
					callback(total);
				});
				*/
				ProjectUpdate.find({
					conditions: {
						isPublic: true,
						projectId: self.id
					}
				}, function(updates) {
					callback(null, updates.length);
				});
			};
		}

		if (_.indexOf(fields, 'owner') > -1) {
			funcs.owner = function(callback) {
				self.user(function(owner) {
					callback(null, owner);
				});
			};
		}

		if (_.indexOf(fields, 'thumbnail') > -1) {
			funcs.thumbnail = function(callback) {
				self.thumbnail(function(url) {
					callback(null, url);
				});
			};
		}
		
		if (_.indexOf(fields, 'university') > -1) {
			funcs.university = function(callback) {
				self.university(function(university) {
					callback(null, university);
				});
			};
		}
		
		if (_.indexOf(fields, 'updates') > -1) {
			funcs.updates = function(callback) {
				var conditions = {};
				if (!params.canManage) conditions.isPublic = true;
				self.updates({
					conditions: conditions
				}, function(updates) {
					async.map(updates, function(update, callback) {
						update.url(params.paraglide, function(url) {
							callback(null, {
								update: update,
								updateUrl: url
							});
						});
					}, function(error, updateItems) {
						callback(null, updateItems);
					});
				});
			};
		}
		
		if (_.indexOf(fields, 'updatesUrl') > -1) {
			funcs.updatesUrl = function(callback) {
				self.url(params.paraglide, 'updates', function(url) {
					callback(null, url);
				});
			};
		}

		if (_.indexOf(fields, 'url') > -1) {
			funcs.url = function(callback) {
				self.url(params.paraglide, function(url) {
					callback(null, url);
				});
			};
		}
		
		async.parallel(funcs, function(error, results) {
			callback(error, results);
		});
	},
	
	canEdit: function(user, callback) {
		if (!user) {
			callback(false);
			return;
		}
		
		var self = this;
		async.parallel({
			isOwnedBy: function(callback) {
				self.isOwnedBy(user, function(isOwnedBy) {
					callback(null, isOwnedBy);
				});
			},
			isAdmin: function(callback) {
				callback(null, user.isAdmin);
			}
		}, function(error, results) {
			callback(
				results.isAdmin
				|| (
					results.isOwnedBy
					&& !self.isApproved
					&& !self.isSubmitted
				)
			);
		});
	},
	
	canManage: function(user, callback) {
		if (!user) {
			callback(false);
			return;
		}
		
		var self = this;
		async.parallel({
			isOwnedBy: function(callback) {
				self.isOwnedBy(user, function(isOwnedBy) {
					callback(null, isOwnedBy);
				});
			},
			isAdmin: function(callback) {
				callback(null, user.isAdmin);
			}
		}, function(error, results) {
			callback(results.isAdmin || results.isOwnedBy);
		});
	},
	
	canView: function(user, callback) {
		if (this.isApproved) {
			callback(true);
			return;
		}
		
		var self = this;
		async.parallel({
			isOwnedBy: function(callback) {
				self.isOwnedBy(user, function(isOwnedBy) {
					callback(null, isOwnedBy);
				});
			},
			isAdmin: function(callback) {
				callback(null, user.isAdmin);
			}
		}, function(error, results) {
			callback(results.isAdmin || results.isOwnedBy);
		});
	},
	
	charge: function(paraglide, callback) {
		console.log('  Finding unpaid project backers...');
		this.backers({
			conditions: {
				isCanceled: false,
				isPaid: false
			},
			order: 'dateCreated'
		}, function(backers) {
			console.log('    Found ' + backers.length + ' unpaid project backers.');
			console.log('    Charging project backers...');
			async.forEachSeries(backers, function(backer, callback) {
				console.log('      Charging project backer #' + backer.id + ' for $' + backer.amount + '...');
				backer.charge(paraglide, function(charged) {
					if (!charged) {
						console.log('      Charge for project backer #' + backer.id + ' failed.');
						console.log('        Error: ' + _.pluck(backer.errors, 'message').join(', '));
					} else {
						console.log('      Charged.');
					}
					
					callback();
				});
			}, function(error) {
				console.log('    Charged.');
				console.log('  Done.');
				callback();
			});
		});
	},
	
	fund: function(user, level, amount, callback) {
		var backer = new ProjectBacker();
		backer.projectId = this.id;
		backer.projectLevelId = level.id;
		backer.userId = user.id;
		backer.amount = amount;
		
		var self = this;
		backer.save(function(saved) {
			if (!saved) {
				callback(false);
				return;
			}
			
			async.parallel({
				level: function(callback) {
					level.recalculate(function() {
						callback(null, true);
					});
				},
				project: function(callback) {
					self.recalculate(function() {
						callback(null, true);
					});
				}
			}, function(error) {
				callback(true);
			});
		});
	},
	
	hasPresentation: function() {
		return this.isPresentationPrezi();
	},
	
	hasVideo: function() {
		return this.isVideoVimeo() || this.isVideoYouTube();
	},
	
	isDurationEditable: function() {
		return !this.isApproved;
	},
	
	isOwnedBy: function(user, callback) {
		callback(user && user.id == this.userId);
	},
	
	isStarted: function() {
		var now = (new Date()).getTime();
		var starts = this.dateFundingStarts.getTime();
		return (now >= starts);
	},
	
	isPresentationPrezi: function() {
		if (!this.presentationUrl) {
			return false;
		}
		
		var parsedUrl = url.parse(this.presentationUrl);
		return (parsedUrl.host == 'prezi.com' || parsedUrl.host == 'www.prezi.com');
	},

	isVideoVimeo: function() {
		if (!this.videoUrl) {
			return false;
		}
		
		var parsedUrl = url.parse(this.videoUrl);
		return (parsedUrl.host == 'vimeo.com' || parsedUrl.host == 'www.vimeo.com');
	},
	
	isVideoYouTube: function() {
		if (!this.videoUrl) {
			return false;
		}
		
		var parsedUrl = url.parse(this.videoUrl);
		return (
			parsedUrl.host == 'youtube.com'
			|| parsedUrl.host == 'www.youtube.com'
			|| parsedUrl.host == 'youtu.be'
			|| parsedUrl.host == 'www.youtu.be'
		);
	},
	
	// action and actionId are optional
	longUrl: function(paraglide, action, actionId, callback) {
		if (typeof actionId == 'function') {
			callback = actionId;
			actionId = null;
		}
		
		if (typeof action == 'function') {
			callback = action;
			action = null;
		}
		
		var self = this;
		self.university(function(university) {
			if (!university) {
				var url = paraglide.longUrl('projects', action, [self.id, actionId]);
				callback(url);
				return;
			}
			
			if (
				paraglide.data.whitelabelUniversity
				&& paraglide.data.whitelabelUniversity.id == university.id
			) {
				var url = paraglide.longUrl('projects', null, [self.slug, action, actionId]);
				callback(url);
				return;
			}
			
			var url = paraglide.longUrl('projects', null, [university.slug, self.slug, action, actionId]);
			callback(url);
		});
	},
	
	presentationId: function() {
		if (this.isPresentationPrezi()) {
			var parsedUrl = url.parse(this.presentationUrl, true);
			var pathParts = parsedUrl.pathname.substr(1).split('/');
			return (pathParts.length > 0) ? pathParts[0] : null;
		}
		
		return null;
	},

	recalculate: function(callback) {
		var self = this;
		async.parallel({
			amountRaised: function(callback) {
				self.backers(function(backers) {
					var amountRaised = 0;
					async.forEach(backers, function(backer, callback) {
						amountRaised += backer.amount;
						callback(null);
					}, function(error) {
						callback(null, amountRaised);
					});
				});
			},
			isCompleted: function(callback) {
				if (!self.dateFundingEnds) {
					callback(null, false);
					return;
				}
				
				var now = (new Date()).getTime();
				var end = self.dateFundingEnds.getTime();
				var seconds = (end - now) / 1000;
				
				if (isNaN(seconds)) {
					callback(null, false);
					return;
				}
				
				if (seconds <= 0) {
					callback(null, true);
					return;
				}
	
				callback(null, false);
			},
			numBackers: function(callback) {
				/* TODO: USE THIS CODE WHEN .total IS WORKING
				self.total('backers', function(total) {
					callback(total);
				});
				*/
				ProjectBacker.count({
					conditions: {
						projectId: self.id
					}
				}, function(count) {
					callback(null, count);
				});
			}
		}, function(error, results) {
			self.amountRaised = results.amountRaised;
			self.isCompleted = results.isCompleted;
			self.numBackers = results.numBackers;
			
			self.amountRaisedPercentage = Math.round(100 * self.amountRaised / self.amountGoal);
			self.isSuccessful = (self.amountRaised >= self.amountGoal);

			self.save(function(saved) {
				callback();
			});
		});
	},
	
	setValues: function(values, params, callback) {
		var self = this;
		Paragon.prototype.setValues.call(this, values, params, function() {
			// the amount can only have digits
			self.amountGoal = self.amountGoal.replace(/[^\d+]/g, '');
			
			// the website must be prefixed by http:// or https://
			if (
				self.websiteUrl.length
				&& self.websiteUrl.substr(0, 'http://'.length) != 'http://'
				&& self.websiteUrl.substr(0, 'https://'.length) != 'https://'
			) {
				self.websiteUrl = 'https://' + self.websiteUrl;
			}
			
			callback();
		});
	},

	startsIn: function() {
		var now = (new Date()).getTime();
		var starts = this.dateFundingStarts.getTime();
		var seconds = (starts - now) / 1000;
		
		if (isNaN(seconds)) {
			return 'TBA';
		}
		
		if (seconds <= 0) {
			return '0 seconds';
		}
		
		var days = seconds / (60 * 60 * 24);

		if (days >= 1) {
			return Math.round(days).toString() + ' days';
		}
		
		var hours = seconds / (60 * 60);
		
		if (hours >= 1) {
			return Math.round(hours).toString() + ' hours';
		}
		
		var minutes = seconds / 60;
		
		if (minutes >= 5) {
			return Math.round(minutes).toString() + ' minutes';
		}
		
		if (minutes >= 1) {
			var display = Math.round(minutes).toString() + ' minutes';
			seconds -= minutes * 60;
			display += ', ' + seconds + ' seconds';
			return display;
		}
		
		return seconds.toString() + ' seconds';
	},
	
	thumbnail: function(callback) {
		this.image(function(image) {
			if (image) {
				callback(image);
				return;
			}
			
			var defaultImage = 'images/browse/default_project.png';
			var thumbnail = new ImageManipulator(defaultImage, function(error) {
				callback(thumbnail);
			});
		});
	},
	
	timeRemaining: function() {
		if (!this.dateFundingEnds) {
			return 'TBA';
		}
		
		var now = (new Date()).getTime();
		var end = this.dateFundingEnds.getTime();
		var seconds = (end - now) / 1000;
		
		if (isNaN(seconds)) {
			return 'TBA';
		}
		
		if (seconds <= 0) {
			return '0 seconds';
		}
		
		var days = seconds / (60 * 60 * 24);

		if (days >= 1) {
			return Math.round(days).toString() + ' days';
		}
		
		var hours = seconds / (60 * 60);
		
		if (hours >= 1) {
			return Math.round(hours).toString() + ' hours';
		}
		
		var minutes = seconds / 60;
		
		if (minutes >= 5) {
			return Math.round(minutes).toString() + ' minutes';
		}
		
		if (minutes >= 1) {
			var display = Math.round(minutes).toString() + ' minutes';
			seconds -= minutes * 60;
			display += ', ' + seconds + ' seconds';
			return display;
		}
		
		return seconds.toString() + ' seconds';
	},
	
	uploadImage: function(data, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				StoredImage.upload(data, 'images/projects/', function(image) {
					if (!image) {
						callback(true);
						return;
					}
					
					callback(null, image);
				});
			},
			function(image, callback) {
				self.image(function(existingImage) {
					if (!existingImage) {
						callback(null, image);
						return;
					}
					
					existingImage.delete(function(deleted) {
						callback(null, image);
					});
				});
			},
			function(image, callback) {
				image.parentId = self.id;
				image.parentType = 'Project';
				image.type = 'Project';
				image.save(function(saved) {
					if (!saved) {
						callback(true);
						return;
					}
					
					callback(null, image);
				});
			}
		], function(error, image) {
			callback(!error && image);
		});
	},
	
	// action and actionId are optional
	url: function(paraglide, action, actionId, callback) {
		if (typeof actionId == 'function') {
			callback = actionId;
			actionId = null;
		}
		
		if (typeof action == 'function') {
			callback = action;
			action = null;
		}
		
		var self = this;
		self.university(function(university) {
			if (!university) {
				var url = paraglide.url('projects', action, [self.id, actionId]);
				callback(url);
				return;
			}
			
			if (
				paraglide.data.whitelabelUniversity
				&& paraglide.data.whitelabelUniversity.id == university.id
			) {
				var url = paraglide.url('projects', null, [self.slug, action, actionId]);
				callback(url);
				return;
			}
			
			var url = paraglide.url('projects', null, [university.slug, self.slug, action, actionId]);
			
			if (paraglide.data.isEquity && !self.isEquity) {
				url = paraglide.data.nonEquityBaseUrl + url;
			} else if (!paraglide.data.isEquity && self.isEquity) {
				url = paraglide.data.equityBaseUrl + url;
			}
			
			callback(url);
		});
	},
	
	validate: function(callback) {
		var self = this;
		async.waterfall([
			// set the slug based on the title
			function(callback) {
				if (this.slug) {
					callback(null);
					return;
				}

				Slug.slugify(self, Project, self.title, function(slug) {
					self.slug = slug;
					Paragon.prototype.validate.call(self, callback);
				});
			}
		], function(error) {
			Paragon.prototype.validate.call(self, callback);
		});
	},
	
	videoId: function() {
		if (this.isVideoVimeo()) {
			var parsedUrl = url.parse(this.videoUrl, true);
			return parsedUrl.pathname.substr(1);
		}
		
		if (this.isVideoYouTube()) {
			var parsedUrl = url.parse(this.videoUrl, true);
			return parsedUrl.query.v || parsedUrl.pathname.substr(1);
		}
		
		return null;
	}
};

Paragon.registerModel(Project);
