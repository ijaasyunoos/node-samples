var _ = require('underscore');
var async = require('async');
var fs = require('fs');
var glob = require('glob');
var gm = require('gm');
var im = require('imagemagick');
var md5 = require('MD5');
var os = require('os');

var Paraglide = require('../lib/paraglide/paraglide');
var Paragon = require('../lib/classes/paragon/paragon');

var AWS = require('aws-sdk');
var s3;

// use imagemagick if graphicsmagick isn't installed
gm(1, 1, '#000000').write(os.tmpDir() + '/gm-test.png', function(error) {
	if (error) {
		gm = gm.subClass({imageMagick: true});
	}
});


Paraglide.onInitComplete(function() {
	AWS.config.update({
		accessKeyId: Paraglide.config.amazon.aws.key,
		secretAccessKey: Paraglide.config.amazon.aws.secret
	});
	s3 = new AWS.S3();
});

var StoredImage = module.exports = function() {
	Paragon.apply(this);
};

StoredImage._table = 'stored_images';

StoredImage.validations = {
	dateCreated: {},
	dateUpdated: {},
	
	caption: {},
	extension: {},
	height: {integer: true, min: 0},
	name: {},
	path: {},
	size: {integer: true, min: 0},
	width: {integer: true, min: 0},
	
	parentId: {integer: true, min: 0},
	parentType: {},
	type: {}
};

StoredImage.s3Bucket = 'alumnifunder-images';

StoredImage._baseConvert = function(numString, fromBase, toBase) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyz';
	var toString = chars.substr(0, toBase);
	var length = numString.length;
	var number = [];
	var result = '';
	
	for (var i = 0; i < length; i++) {
		number[i] = chars.indexOf(numString.charAt(i));
	}
		
	do {
		var divide = 0;
		var newLength = 0;
		
		for (var i = 0; i < length; i++) {
			divide = divide * fromBase + number[i];
			
			if (divide >= toBase) {
				number[newLength++] = parseInt(divide / toBase);
				divide = divide % toBase;
			} else if (newLength > 0) {
				number[newLength++] = 0;
			}
		}
		
		length = newLength;
		result = toString.charAt(divide) + result;
	}
	while (newLength != 0);
	
	return result;
};

StoredImage.upload = function(file, path, callback) {
	if ((!file.path || !file.name || !file.type) && !file.authorizedFile) {
		callback(false);
		return;
	}
	
	async.waterfall([
		function(callback) {
//			im.identify(file.path, function(error, features) {
			gm(file.path).identify(function(error, features) {
				if (error) {
					callback(error);
					return;
				}
				
				callback(null, features.size.width, features.size.height);
			});
		},
		function(width, height, callback) {
			var image = new StoredImage();
			image.name = file.name;
			image.extension = 'png';
			image.height = height;
			image.width = width;
			if (path.substr(-1) != '/') path += '/';
			image.path = path;
			image.size = file.size;
			image.save(function(saved) {
				callback(null, image);
			});
		},
		function(image, callback) {
			var location = path + image.id + '.' + image.extension;
			var fullLocation = os.tmpDir() + '/image-' + md5(location);

/*
			im.resize({
				srcPath: file.path,
				dstPath: fullLocation,
				format: 'png',
				width: image.width,
				height: image.height
			}, function(error) {
				image.size = fs.size(fullLocation);
			});
*/
			gm(file.path)
				.resize(image.width, image.height)
				.noProfile()
				.setFormat('png')
				.write(fullLocation, function(error) {
					gm(fullLocation).size(function(error, size) {
						image.size = size;
						image.save(function(saved) {
							callback(null, location, fullLocation, image);
						});
					});
				});
		},
		function(location, fullLocation, image, callback) {
			s3.client.deleteObject({
				Bucket: StoredImage.s3Bucket,
				Key: location
			}, function(error, data) {
				if (error) {
					callback(error);
					return;
				}
				
				fs.readFile(fullLocation, function(error, data) {
					s3.client.putObject({
						Bucket: StoredImage.s3Bucket,
						Key: location,
						Body: data,
						ACL: 'public-read'
					}, function(error, data) {
						if (error) {
							callback(error);
							return;
						}
						
						fs.unlink(fullLocation, function() {
							callback(null, image);
						});
					});
				});
			});
		}
	], function(error, image) {
		callback(!error && image);
	});
};

StoredImage.prototype = {
	id: null,
	
	dateCreated: null,
	dateUpdated: null,
	
	caption: null,
	extension: null,
	height: 0,
	name: null,
	path: null,
	size: 0,
	width: 0,
	
	parentId: 0,
	parentType: null,
	type: null,
	
	_dimensions: function(width, height, minimize) {
		if (width < 0) width = 0;
		if (height < 0) height = 0;
		
		if (!width && !height) {
			return [this.width, this.height];
		}
		
		if (!width && height) {
			width = Math.round(this.width * height / this.height);
		} else if (!height && width) {
			height = Math.round(this.height * width / this.width);
		}
		
		if (minimize) {
			var newHeight = this.height;
			var newWidth = this.width;

			// make sure we meet the minimum height/width
			if (height > newHeight) {
				newHeight = height;
				newWidth = newHeight * this.width / this.height;
			}
	
			if (width > newWidth) {
				newWidth = width;
				newHeight = newWidth * this.height / this.width;
			}
			
			while (newHeight > height || newWidth > width) {
				if (newHeight > height) {
					newWidth = height * this.width / this.height;
					newHeight = height;
				}
				
				if (newWidth > width) {
					newHeight = width * this.height / this.width;
					newWidth = width;
				}
			}
			
			height = Math.round(newHeight);
			width = Math.round(newWidth);
		}
		
		return {
			width: width,
			height: height
		};
	},
	
	delete: function(callback) {
		var path = this.path + this.id + '.' + this.extension;
		var pathPrefix = this.path + this.id;
		
		var self = this;
		async.waterfall([
			function(callback) {
				s3.client.deleteObject({
					Bucket: StoredImage.s3Bucket,
					Key: path
				}, function(error, data) {
					callback(error);
				});
			},
			function(callback) {
				fs.unlink(Paraglide.appPath + path, function() {
					callback(null);
				});
			},
			function(callback) {
				glob(Paraglide.appPath + pathPrefix + '-*', null, function(error, files) {
					if (error) {
						callback(null);
						return;
					}
					
					async.forEach(files, function(file, callback) {
						fs.unlink(file, function(error) {
							callback(null);
						});
					}, function(error) {
						callback(null);
					});
				});
			},
			function(callback) {
				Paragon.prototype.delete.call(self, function(deleted) {
					callback(deleted);
				});
			}
		], function(error) {
			callback(!error);
		});
	},
	
	resize: function(width, height, callback) {
		width = parseInt(width);
		height = parseInt(height);
		var sourcePath = this.path + this.id + '.' + this.extension;
		var newPath = this.path + this.id;
		newPath += '-' + this.sizeHash(width, height);
		newPath += '-' + width + 'x' + height + '.' + this.extension;
		var fullNewPath = os.tmpDir() + '/image-' + md5(newPath);
		var dimensions = this._dimensions(width, height, true);
		var newWidth = dimensions.width;
		var newHeight = dimensions.height;
		
		if (newWidth < width && newHeight >= height) {
			newHeight *= width / newWidth;
			newWidth = width;
		} else if (newWidth >= width && newHeight < height) {
			newWidth *= height / newHeight;
			newHeight = height;
		} else if (newWidth < width && newHeight < height) {
			var widthRatio = width / newWidth;
			var heightRatio = height / newHeight;
			
			if (widthRatio > heightRatio) {
				newHeight *= width / newWidth;
				newWidth = width;
			} else {
				newWidth *= height / newHeight;
				newHeight = height;
			}
		}
		
		newWidth = Math.round(newWidth);
		newHeight = Math.round(newHeight);
		var x0 = Math.round((width - newWidth) / 2);
		var y0 = Math.round((height - newHeight) / 2);
		var x1 = 0;
		var y1 = 0;

		var self = this;
		async.waterfall([
			function(callback) {
				fs.exists(Paraglide.appPath + sourcePath, function(exists) {
					if (!exists) {
						callback(null, null);
						return;
					}
					
					fs.readFile(Paraglide.appPath + sourcePath, function(error, data) {
						callback(error, data);
					});
				});
			},
			function(data, callback) {
				if (data) {
					callback(null, data);
					return;
				}
				
				s3.client.getObject({
					Bucket: StoredImage.s3Bucket,
					Key: sourcePath
				}, function(error, data) {
					callback(error, data && data.Body);
				});
			},
			function(data, callback) {
				gm(data, self.name)
					.resize(newWidth, newHeight)
					.crop(width, height, 0 - x0, 0 - y0)
					.setFormat('png')
					.write(fullNewPath, function(error) {
						callback(error);
					});
			},
			function(callback) {
				s3.client.deleteObject({
					Bucket: StoredImage.s3Bucket,
					Key: newPath
				}, function(error, data) {
					callback(error);
				});
			},
			function(callback) {
				fs.readFile(fullNewPath, function(error, data) {
					callback(error, data);
				});
			},
			function(data, callback) {
				s3.client.putObject({
					Bucket: StoredImage.s3Bucket,
					Key: newPath,
					Body: data,
					ACL: 'public-read'
				}, function(error, data) {
					if (error) {
						callback(error);
						return;
					}
					
					fs.unlink(fullNewPath, function() {
						callback(null);
					});
				});
			}
		], function(error) {
			callback(!error);
		});
	},
	
	sizeHash: function(width, height) {
		var string = 'id' + this.id + 'width' + width + 'height' + height;
		var base17 = StoredImage._baseConvert(string, 36, 17);
		var alphanumeric = StoredImage._baseConvert(base17, 30, 36);
		return alphanumeric;
	},
	
	url: function(width, height, minimize) {
		width = parseInt(width);
		height = parseInt(height);
		if (typeof minimize == 'undefined' || minimize === null) minimize = true;
		if (minimize == 'false' || minimize == '0') minimize = false;
		minimize = !!minimize;
		var url = Paraglide.siteRoot + this.path + this.id;
		
		if (width > 0 || height > 0) {
			var dimensions = this._dimensions(width, height, minimize);
			url += '-' + this.sizeHash(dimensions.width, dimensions.height);
			url += '-' + dimensions.width + 'x' + dimensions.height;
		}
		
		url += '.' + this.extension;
		return url;
	}
};

Paragon.registerModel(StoredImage);
