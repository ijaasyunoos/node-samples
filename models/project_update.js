var async = require('async');
var Mailer = require('../lib/helpers/mailer');
var Paragon = require('../lib/classes/paragon/paragon');
var Project = require('./project');
var User = require('./user');

var ProjectUpdate = function() {
	Paragon.apply(this);
};

ProjectUpdate._table = 'project_updates';

ProjectUpdate._belongsTo = {
	project: 'Project'
};

ProjectUpdate.validations = {
	projectId: {required: true, integer: true, min: 1},
	
	dateCreated: {},
	dateUpdated: {},
	
	hasMailed: {boolean: true},
	isPublic: {boolean: true},

	title: {required: true},
	description: {required: true}
};

ProjectUpdate.prototype = {
	id: null,
	projectId: null,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	
	hasMailed: false,
	isPublic: false,
	
	title: null,
	description: null,
	
	_emailUsers: function(paraglide, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				async.parallel({
					project: function(callback) {
						self.project(function(project) {
							callback(null, project);
						});
					},
					url: function(callback) {
						self.url(paraglide, function(url) {
							callback(null, url);
						});
					}
				}, function(error, results) {
					callback(null, results.project, results.url);
				});
			},
			function(project, url, callback) {
				async.parallel({
					backers: function(callback) {
						project.backers(function(backers) {
							callback(null, backers);
						});
					},
					projectUrl: function(callback) {
						project.url(paraglide, function(url) {
							callback(null, url);
						});
					},
				}, function(error, results) {
					callback(null, project, url, results.projectUrl, results.backers);
				});
			},
			function(project, url, projectUrl, backers, callback) {
				async.map(backers, function(backer, callback) {
					backer.user(function(user) {
						user.primaryEmail(function(primaryEmail) {
							callback(null, {
								backer: backer,
								email: primaryEmail.email,
								user: user
							});
						});
					});
				}, function(error, backerUserEmails) {
					callback(null, project, url, projectUrl, backerUserEmails);
				});
			},
			function(project, url, projectUrl, backerUserEmails, callback) {
				async.forEach(backerUserEmails, function(backerUserEmail, callback) {
					paraglide.render('account/email.project-update', {
						company: paraglide.getClass().config.company.main.name,
						email: backerUserEmail.email,
						paraglide: paraglide,
						project: project,
						projectUrl: projectUrl,
						update: self,
						updateUrl: url,
						user: backerUserEmail.user
					}, function(message) {
						Mailer.send({
							to: backerUserEmail.email,
							subject: paraglide.getClass().config.company.main.name + ' - Project Update: ' + self.title,
							message: message
						}, function(mailed) {
							callback(null, mailed);
						});
					});
				}, function(error) {
					callback(null);
				});
			}
		], function(error) {
			callback();
		});
	},
	
	/* TODO: Move these to magic methods in Paragon */

	project: function(callback) {
		if (this._relationshipInstances.project) {
			callback(this._relationshipInstances.project);
			return;
		}
		
		var self = this;
		Project.find(this.projectId, function(project) {
			self._relationshipInstances.project = project;
			callback(project);
		});
	},
	
	/* END magic methods */

	next: function(callback) {
		ProjectUpdate.findOne({
			conditions: {
				dateCreated: ProjectUpdate.condition('gt', this.dateCreated),
				isPublic: true
			},
			order: 'dateCreated'
		}, function(update) {
			callback(update);
		});
	},
	
	numberTodo: function(callback) {
		callback(0);
		/*
		this.count({
			conditions: {
				dateCreated: this.condition('lt', this.dateCreated),
				isPublic: true,
				projectId: this.projectId
			}
		}, function(count) {
			callback(count);
		});
		*/
		ProjectUpdate.find({
			conditions: {
				dateCreated: this.condition('lt', this.dateCreated),
				isPublic: true,
				projectId: this.projectId
			}
		}, function(updates) {
			callback(updates.length + 1);
		});
	},
	
	previous: function(callback) {
		ProjectUpdate.findOne({
			conditions: {
				dateCreated: ProjectUpdate.condition('lt', this.dateCreated),
				isPublic: true
			},
			order: '-dateCreated'
		}, function(update) {
			callback(update);
		});
	},
	
	publish: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
	
		if (!params) params = {};
		
		var self = this;
		this.isPublic = true;
		this.save(function(saved) {
			if (self.hasMailed || !params.paraglide) {
				callback(saved);
				return;
			}
			
			self._emailUsers(params.paraglide, function() {
				self.hasMailed = true;
				self.save(function(saved) {
					callback(saved);
				})
			});
		});
	},

	url: function(paraglide, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				self.project(function(project) {
					callback(!project, project);
				});
			},
			function(project, callback) {
				project.university(function(university) {
					callback(!university, project, university);
				});
			}
		], function(error, project, university) {
			if (!project) {
				var url = paraglide.url('projects');
				callback(url);
				return;
			}
			
			if (error || !university) {
				var url = paraglide.url('projects', 'updates', [project.id, self.id]);
				callback(url);
				return;
			}
			
			if (
				paraglide.data.whitelabelUniversity
				&& paraglide.data.whitelabelUniversity.id == university.id
			) {
				var url = paraglide.url('projects', null, [project.slug, 'updates', self.id]);
				callback(url);
				return;
			}
			
			var url = paraglide.url('projects', null, [university.slug, project.slug, 'updates', self.id]);
			callback(url);
		});
	}
};

Paragon.registerModel(ProjectUpdate);
module.exports = ProjectUpdate;
