var Paragon = require('../lib/classes/paragon/paragon');

var UserEmail = function() {
	Paragon.apply(this);
};

UserEmail._table = 'user_emails';

UserEmail._belongsTo = {
	user: 'User'
};

UserEmail.validations = {
	userId: {},
	
	email: {required: true, email: true, maxLength: 255}
};

UserEmail.prototype = {
	id: null,
	userId: null,
	
	email: null,

	hasChanged: function() {
		return (this.email != this._fieldValues.email);
	},

	toString: function() {
		return this.email;
	},
	
	validate: function(callback) {
		var self = this;
		Paragon.prototype.validate.call(this, function() {
			UserEmail.findOne({
				conditions: {email: self.email}
			}, function(existingEmail) {
				if (existingEmail && self.id != existingEmail.id) {
					self.errors.email = 'Another user already has this email address';
				}
				
				callback(!self.hasErrors());
			});
		});
	},

	validateEmails: function(vars) {
		if (!vars.email) {
			this.errors.email = 'Please enter the "Email" field';
		} else if (vars.email && !vars.emailVerify) {
			this.errors.emailVerify = 'Please enter the "Verification Email" field';
		} else if (vars.email && vars.email != vars.emailVerify) {
			this.errors.email = 'Your "Verification Email" does not match your "Email"';
		}

		return !this.hasErrors();
	}
};

Paragon.registerModel(UserEmail);
module.exports = UserEmail;
