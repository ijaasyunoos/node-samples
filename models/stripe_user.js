var async = require('async');
var querystring = require('querystring');
var request = require('request');
var Paragon = require('../lib/classes/paragon/paragon');
var Stripe = require('stripe');

var stripeConfig = {};

var StripeUser = function() {
	Paragon.apply(this);
};

StripeUser._table = 'stripe_users';

StripeUser._belongsTo = {
	user: 'User'
};

StripeUser._getApi = function(paraglide) {
	var config = {};
	return Stripe(stripeConfig.secretKey);
};

StripeUser.auth = function(paraglide, params, callback) {
	var facebook = StripeUser._getApi(paraglide);
	facebook.api('/me', params, null, function(error, me) {
		if (error || !me) {
			callback(error || !me || !me.id);
			return;
		}
		
		callback(me);
	});
};

StripeUser.connect = function(paraglide, params, user, callback) {
	var stripe = StripeUser._getApi(paraglide);
	if (!params) params = {};
	async.waterfall([
		function(callback) {
			if (!paraglide.request.query.code) {
				var state = {};
				if (params.redirectUrl) state.redirect_url = params.redirectUrl;
				var connectUrl = 'https://connect.stripe.com/oauth/authorize?response_type=code';
				connectUrl += '&client_id=' + stripeConfig.clientId;
				connectUrl += '&state=' + encodeURIComponent(JSON.stringify(state));
				paraglide.redirectTo(connectUrl);
				return;
			}

			request.post('https://connect.stripe.com/oauth/token', {
				form: {
					code: paraglide.request.query.code,
					grant_type: 'authorization_code'
				},
				headers: {
					Authorization: 'Bearer ' + stripeConfig.secretKey,
				}
			}, function(error, response, body) {
				body = JSON.parse(body);
				
				if (!body.access_token) {
					callback(new Error('You\'ve chosen not to connect or create a Stripe account.'));
					return;
				}
				
				callback(null, body);
			});
		},
		function(auth, callback) {
			StripeUser.findByUid(auth.stripe_user_id, function(stripeUser) {
				callback(null, auth, stripeUser);
			});
		},
		function(auth, stripeUser, callback) {
			if (!stripeUser || !stripeUser.userId) {
				callback(null, auth, stripeUser)
				return;
			}

			stripeUser.user(function(existingUser) {
				if (existingUser && existingUser.id != user.id) {
					callback(new Error('Another user is already using this Stripe account.'));
					return;
				}
				
				callback(null, auth, stripeUser);
			});
		},
		function(auth, stripeUser, callback) {
			if (!stripeUser) {
				stripeUser = new StripeUser();
				stripeUser.userId = user.id;
				stripeUser.uid = auth.stripe_user_id;
			}
			
			stripeUser.accessToken = auth.access_token;
			stripeUser.publishableKey = auth.stripe_publishable_key;
			stripeUser.save(function(saved) {
				callback(null);
			});
		}
	], function(error) {
		if (error) {
			callback(error);
			return;
		}
		
		if (paraglide.request.query.state) {
			var state = JSON.parse(paraglide.request.query.state);

			if (state && state.redirect_url) {
				paraglide.redirectTo(state.redirect_url);
				return;
			}
		}
		
		callback(null, user);
	});
};

StripeUser.findByUid = function(uid, callback) {
	StripeUser.findOne({
		conditions: {uid: uid}
	}, function(facebookUser) {
		callback(facebookUser);
	});
};

StripeUser.login = function(paraglide, redirect, params, callback) {
	if (typeof redirect == 'undefined') redirect = true;
	if (!params) params = {};
	StripeUser.connect(paraglide, params, null, function(user) {
		if (!user) {
			callback(false);
			return;
		}
		
		user.login(paraglide, redirect);
	});
};

StripeUser.setApiConfig = function(secretKey, publishableKey, clientId) {
	stripeConfig = {
		secretKey: secretKey,
		publishableKey: publishableKey,
		clientId: clientId
	};
};

StripeUser.prototype = {
	id: null,
	userId: null,
	
	dateCreated: null,
	
	uid: null,
	accessToken: null,
	publishableKey: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	user: function(callback) {
		User.find(this.userId, function(user) {
			callback(user);
		});
	}
	
	/* END magic methods */
};

Paragon.registerModel(StripeUser);
module.exports = StripeUser;
var User = require('./user');
