var _ = require('underscore');
var async = require('async');
var Paragon = require('../lib/classes/paragon/paragon');

var ProjectBacker = module.exports = function() {
	Paragon.apply(this);
};

var Payment = require('./payment');
var Project = require('./project');
var ProjectLevel = require('./project_level');
var User = require('./user');

ProjectBacker._table = 'project_backers';

ProjectBacker._belongsTo = {
	level: 'ProjectLevel',
	project: 'Project',
	user: 'User'
};

ProjectBacker._hasMany = {
	payments: {
		'class': 'Payment',
		order: 'date_created'
	}
};

ProjectBacker.validations = {
	projectId: {required: true, integer: true, min: 1},
	projectLevelId: {required: true, integer: true, min: 1},
	userId: {required: true, integer: true, min: 1},
	
	dateCreated: {},
	dateUpdated: {},
	
	isCanceled: {boolean: true},
	isPaid: {boolean: true},
	isRefunded: {boolean: true},
	
	amount: {required: true, integer: true, min: 1}
};

ProjectBacker.prototype = {
	id: null,
	projectId: null,
	projectLevelId: null,
	userId: null,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	
	isCanceled: false,
	isPaid: false,
	isRefunded: false,
	
	amount: 0,

	/* TODO: Move these to magic methods in Paragon */
	
	level: function(callback) {
		if (this._relationshipInstances.projectLevel) {
			callback(this._relationshipInstances.projectLevel);
			return;
		}
		
		var self = this;
		ProjectLevel.find(this.projectLevelId, function(projectLevel) {
			self._relationshipInstances.projectLevel = projectLevel;
			callback(projectLevel);
		});
	},
	
	payments: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.payments) {
			callback(this._relationshipInstances.payments);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		params.conditions.projectBackerId = this.id;
		if (!params.order && ProjectBacker._hasMany.payments.order) params.order = ProjectBacker._hasMany.payments.order;
		
		var self = this;
		Payment.find({
			conditions: params.conditions,
			order: params.order
		}, function(payments) {
			if (_.size(params) == 0) {
				self._relationshipInstances.payments = payments;
			}
			
			if (single) {
				callback(payments[0] || null);
				return;
			}
			
			callback(payments);
		});
	},
	
	project: function(callback) {
		if (this._relationshipInstances.project) {
			callback(this._relationshipInstances.project);
			return;
		}
		
		var self = this;
		Project.find(this.projectId, function(project) {
			self._relationshipInstances.project = project;
			callback(project);
		});
	},

	user: function(callback) {
		if (this._relationshipInstances.user) {
			callback(this._relationshipInstances.user);
			return;
		}
		
		var self = this;
		User.find(this.userId, function(user) {
			self._relationshipInstances.user = user;
			callback(user);
		});
	},
	
	/* END magic methods */
	
	charge: function(paraglide, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				var payment = new Payment();
				payment.projectBackerId = self.id;
				payment.amount = self.amount;
				payment.save(function(saved) {
					if (!saved) {
						callback(true, false);
						return;
					}
					
					payment.charge(paraglide, function(error, success) {
						callback(error, success);
					});
				});
			}
		], function(error, success) {
			if (error) {
				self.errors.isPaid = error;
				callback(false);
				return;
			}
			
			self.isPaid = true;
			self.save(function() {
				callback(true);
			});
		});
	}
};

Paragon.registerModel(ProjectBacker);
