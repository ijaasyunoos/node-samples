var async = require('async');
var Mailer = require('../lib/helpers/mailer');
var Paragon = require('../lib/classes/paragon/paragon');
var Pay = require('../lib/classes/pay/pay');

var Payment = module.exports = function() {
	Paragon.apply(this);
};

var ProjectBacker = require('./project_backer');

Payment._table = 'payments';

Payment._belongsTo = {
	backer: 'ProjectBacker'
};

Payment.validations = {
	projectBackerId: {required: true, integer: true, min: 1},
	
	dateCreated: {},
	dateUpdated: {},
	
	isCanceled: {boolean: true},
	isPaid: {boolean: true},
	isRefunded: {boolean: true},
	isRejected: {boolean: true},
	
	amount: {required: true, integer: true, min: 1},
	
	message: {}
};

Payment.prototype = {
	id: null,
	projectBackerId: null,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	
	isCanceled: false,
	isPaid: false,
	isRefunded: false,
	isRejected: false,
	
	amount: 0,
	
	message: null,

	/* TODO: Move these to magic methods in Paragon */
	
	backer: function(callback) {
		if (this._relationshipInstances.projectBacker) {
			callback(this._relationshipInstances.projectBacker);
			return;
		}
		
		var self = this;
		ProjectBacker.find(this.projectBackerId, function(projectBacker) {
			self._relationshipInstances.projectBacker = projectBacker;
			callback(projectBacker);
		});
	},
	
	/* END magic methods */
	
	charge: function(paraglide, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				self.backer(function(backer) {
					callback(null, backer);
				});
			},
			function(backer, callback) {
				backer.user(function(user) {
					if (!user.paymentType) {
						callback(new Error('Missing payment details'));
						return;
					}
					
					callback(null, backer, user);
				});
			},
			function(backer, user, callback) {
				var payment = new Pay.Payment({
					paymentType: user.paymentType
				});
				payment.charge(self.amount, 'USD', function(error, success) {
					callback(error, success);
				});
			}
		], function(error, success) {
			if (success) {
				self.isPaid = true;
			} else {
				self.isRejected = true;
				self.message = error.message;
			}
			
			self.save(function(saved) {
				self.sendEmailConfirmation(paraglide, function() {
					callback(error, !error && success);
				});
			});
		});
	},
	
	sendEmailConfirmation: function(paraglide, callback) {
		var self = this;
		async.waterfall([
			function(callback) {
				self.backer(function(projectBacker) {
					callback(null, projectBacker);
				});
			},
			function(projectBacker, callback) {
				async.parallel({
					level: function(callback) {
						projectBacker.level(function(level) {
							callback(null, level);
						});
					},
					project: function(callback) {
						projectBacker.project(function(project) {
							callback(null, project);
						});
					},
					user: function(callback) {
						projectBacker.user(function(user) {
							callback(null, user);
						});
					}
				}, function(error, results) {
					callback(null, projectBacker, results.project, results.user, results.level);
				});
			},
			function(projectBacker, project, user, level, callback) {
				async.parallel({
					primaryEmail: function(callback) {
						user.primaryEmail(function(primaryEmail) {
							callback(null, primaryEmail);
						});
					},
					url: function(callback) {
						project.longUrl(paraglide, function(url) {
							callback(null, url);
						});
					}
				}, function(error, results) {
					callback(null, projectBacker, project, results.url, user, level, results.primaryEmail);
				});
			}
		], function(error, projectBacker, project, url, user, level, primaryEmail) {
			var emailTemplate = 'account/email.payment-confirmation';
			var subject = 'Payment Success';
			
			if (self.isRejected) {
				emailTemplate = 'account/email.payment-rejection';
				subject = 'Payment Failed';
			}
			
			paraglide.render(emailTemplate, {
				company: paraglide.getClass().config.company.main.name,
				email: primaryEmail.email,
				level: level,
				paraglide: paraglide,
				payment: self,
				project: project,
				projectBacker: projectBacker,
				projectUrl: url,
				user: user
			}, function(message) {
				Mailer.send({
					to: primaryEmail.email,
					subject: paraglide.getClass().config.company.main.name + ' - ' + subject,
					message: message
				}, function(mailed) {
					callback(mailed);
				});
			});
		});
	}
};

Paragon.registerModel(Payment);
