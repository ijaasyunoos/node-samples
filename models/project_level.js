var _ = require('underscore');
var async = require('async');
var url = require('url');
var Paragon = require('../lib/classes/paragon/paragon');

var Project = require('./project');
var ProjectBacker = require('./project_backer');

var ProjectLevel = module.exports = function() {
	Paragon.apply(this);
};

var Project = require('./project');

ProjectLevel._table = 'project_levels';

ProjectLevel._belongsTo = {
	project: 'Project'
};

ProjectLevel._hasMany = {
	backers: {
		'class': 'ProjectBacker',
		order: 'dateCreated'
	}
};

ProjectLevel.validations = {
	projectId: {required: true, integer: true, min: 1},
	
	dateCreated: {},
	dateUpdated: {},
	dateEstimatedDelivery: {},
	
	canSellUnlimited: {boolean: true},
	
	amount: {required: true, integer: true, min: 1},
	numAllowed: {integer: true, min: 0},
	numSold: {integer: true, min: 0},
	
	title: {required: true},
	description: {required: true}
};

ProjectLevel.prototype = {
	id: null,
	projectId: null,
	
	dateCreated: new Paragon.DatetimeField(),
	dateUpdated: new Paragon.DatetimeField(),
	dateEstimatedDelivery: new Paragon.DatetimeField(),
	
	canSellUnlimited: false,
	
	amount: null,
	numAllowed: null,
	numSold: 0,
	
	title: null,
	description: null,
	
	/* TODO: Move these to magic methods in Paragon */
	
	backers: function(params, callback) {
		if (typeof params == 'function') {
			callback = params;
			params = null;
		}
		
		var single = false;
		
		if (typeof params == 'number' || typeof params == 'string') {
			params = {conditions: {id: params}};
			single = true;
		}
		
		if (params == null) params = {};


		if (_.size(params) == 0 && this._relationshipInstances.projectBackers) {
			callback(this._relationshipInstances.projectBackers);
			return;
		}
		
		if (!params.conditions) params.conditions = {};
		params.conditions.projectId = this.id;
		if (!params.order && Project._hasMany.projectBackers.order) params.order = Project._hasMany.projectBackers.order;
		
		var self = this;
		ProjectBacker.find({
			conditions: params.conditions,
			order: params.order
		}, function(projectBackers) {
			if (_.size(params) == 0) {
				self._relationshipInstances.projectBackers = projectBackers;
			}
			
			if (single) {
				callback(projectBackers[0] || null);
				return;
			}
			
			callback(projectBackers);
		});
	},
	
	project: function(callback) {
		if (this._relationshipInstances.project) {
			callback(this._relationshipInstances.project);
			return;
		}
		
		var self = this;
		Project.find(this.projectId, function(project) {
			self._relationshipInstances.project = project;
			callback(project);
		});
	},
	
	/* END magic methods */
	
	isSoldOut: function() {
		return (this.numRemaining() == 0);
	},
	
	numRemaining: function() {
		if (this.canSellUnlimited) {
			return 99999;
		}
		
		return Math.max(this.numAllowed - this.numSold, 0);
	},
	
	recalculate: function(callback) {
		var self = this;
		ProjectBacker.count({
			conditions: {
				projectLevelId: self.id
			}
		}, function(count) {
			self.numSold = count;
			self.save(function(saved) {
				callback();
			});
		});
	}
};

Paragon.registerModel(ProjectLevel);
