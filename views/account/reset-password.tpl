<div class="content changePassword">
	{{#forgottenUser.hasErrors}}
		{{#forgottenUser.errorsArray}}
			{{> common/form/error}}
		{{/forgottenUser.errorsArray}}
		<br />
	{{/forgottenUser.hasErrors}}
	<form method="post">
		<label>
			<span class="label primary">Password</span>
			<input type="password" class="showAll" name="password" value="" autocomplete="off" maxlength="255" />
		</label>
		<label>
			<span class="label primary">Verify Password</span>
			<input type="password" class="showAll" name="password_verify" value="" autocomplete="off" maxlength="255" />
		</label>
		<hr class="spacer border solid short">
		<div class="swing">
			<div class="bd">
				<div class="swingleft">
					<label class="submit">
						<input type="submit" class="button" value="Save" />
					</label>
				</div>
			</div>
		</div>
	</form>
</div>
