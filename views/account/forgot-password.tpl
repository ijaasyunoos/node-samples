{{#self.hasErrors}}
	{{#self.errorsArray}}
		{{> common/form/error}}
	{{/self.errorsArray}}
	<br />
{{/self.hasErrors}}
<div class="content swing border solid forgotPassword">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<div class="subtitle">
					<h2>Fill in your email address to get a new password</h2>
				</div>
				<form method="post">
					<label>
						<span class="label primary">Email Address</span>
						<input type="text" class="showAll toggle-label" name="email" value="{{self.post.email}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer border solid short" />
					<label class="submit">
						<input type="submit" class="button" value="REQUEST PASSWORD" />
					</label>
				</form>
			</div>
		</div>
	</div>
</div>
