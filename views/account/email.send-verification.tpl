Hi {{newUser.name}},
<br /><br />
Thank you for signing up for an account. 
<br /><br />
Please click the following link to verify your account:<br />
<a href="{{#paraglide.longUrl}}account,verify,{{verificationCode}}{{/paraglide.longUrl}}" target="_blank">
	{{#paraglide.longUrl}}account,verify,{{verificationCode}}{{/paraglide.longUrl}}
</a>
<br /><br />
-{{company}}
