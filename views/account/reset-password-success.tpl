<div class="content swing border solid forgotPassword success">
	<div class="subtitle">
		<h2>Your password has been reset.</h2>
		<br /><br />
		<a href="{{#self.url}}login{{/self.url}}" class="psuedobutton">
			<span>Back to Login.</span>
		</a>
	</div>
</div>
