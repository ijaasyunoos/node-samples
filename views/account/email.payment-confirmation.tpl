<div style="background:#3BAED0;margin:0;padding:0;height:100%;width:100%;">
<table width="45%" border="0" cellspacing="0" cellpadding="20" background="{{paraglide.longUrl}}/images/email/bg_line_blue.png" style="min-width:333px;margin:0 auto;">
	<tr>
		<td style="padding:10px 0px 0px;">
			<a style="color:#FFFFFF;display:inline-block;" href="http://www.alumnifunder.com" alt="alumni funder">
				<img src="{{paraglide.longUrl}}/images/email/logo_white.png" alt="Logo" />
			</a>
			<ul style="float:right;position:relative;right:0px;top:50px;background-image:url('{{paraglide.longUrl}}/images/email/social_icons_white.png');list-style-type:none;width:100px;padding-left:0px;height:24px;">
				<li style="display:inline-block;list-style-type:none;height:24px;padding-right:4px;"><a style="text-decoration:none;display:block;" href="https://twitter.com/AlumniFunder"><span style="padding:10px 13px;height:24px;"></span></a></li>
				<li style="display:inline-block;list-style-type:none;height:24px;padding-right:4px;"><a style="text-decoration:none;display:block;" href="http://www.linkedin.com/company/2743250"><span style="padding:10px 13px;height:24px;"></span></a></li>
				<li style="display:inline-block;list-style-type:none;height:24px;padding-right:4px;"><a style="text-decoration:none;display:block;" href="https://www.facebook.com/AlumniFunder"><span style="padding:10px 13px;height:24px;"></span></a></li>
			</ul>
		</td>
	</tr>
	<tr>
		<td style="padding:10 0 0;">
			<hr style="color:#FFFFFF;padding:0px;" noshade="noshade" />
		</td>
	</tr>
	<tr>
		<td style="padding:10 0 0;">
			<p style="color:#FFFFFF;font-family:'Trebuchet MS','Helvetica','Arial';">
				Dear {{user.name}},
				<br /><br />
				Great success! A project you supported on AlumniFunder has met or surpassed it's target amount.
				<br /><br />
				You have been charged your full pledge amount of "${{#currencyFormat}}{{level.amount}}{{/currencyFormat}}", and we would like to point out that this entire amount (minus pesky credit card processing fees) has been transferred to the people running <a style="color:#FFFFFF;" href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{{projectUrl}}}">{{project.title}}</a>.
				<br /><br />
				Thank you for participating in the AlumniFunder experiment! We appreciate your support for entrepreneurial endeavors within your alumni network and hope you'll check back soon for more exciting campaigns and projects.
				<br /><br />
				If you have any questions please feel free to email us at <a style="color:#FFFFFF;" href="mailto:support@alumnifunder.com">support@alumnifunder.com</a>.
				<br /><br />
				Onward & Upward,<br />
				The AlumniFunder Team
			</p>
		</td>
	</tr>
	<tr>
		<td style="padding:10 0 0;"><hr style="color:#FFFFFF;" noshade="noshade" /></td>
	</tr>
	<tr>
		<td style="padding:10 0 0;">
			<a style="color:#FFFFFF;font-family:'Trebuchet MS','Helvetica','Arial';" href="#">contact us</a><span style="color:#FFFFFF;font-family:'Trebuchet MS','Helvetica','Arial';"> | </span><a style="color:#FFFFFF;font-family:'Trebuchet MS','Helvetica','Arial';" href="{{#paraglide.longUrl}}account,edit{{/paraglide.longUrl}}">unsubscribe</a>
		</td>
	</tr>
</table>
</div>