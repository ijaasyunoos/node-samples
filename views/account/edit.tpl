<div class="content viewProfile">
{{#user.hasErrors}}
	{{#user.errorsArray}}
		{{> common/form/error}}
	{{/user.errorsArray}}
	<br />
{{/user.hasErrors}}
{{#error}}
	{{> common/form/error}}
	<br />
{{/error}}
<div class="swing noborder content" style="border:0;">
	<div class="bd">
		<div class="swingleft">
			<hr class="spacer short border actual-solid" />
			<hr class="spacer short" />
			<form method="post" enctype="multipart/form-data" action="{{#self.url}}edit{{/self.url}}" class="grid-contain-with-border-bottom">
				<span class="highlight title">Account Details</span>
				<hr class="spacer short" />
				<hr class="spacer short border solid" />
				<div class="inputs wide">
					<hr class="spacer short" />
					<label>
						<span class="label primary">Your Name</span>
						<hr class="spacer short" />
						<input type="text" class="showAll toggle-label" name="name" value="{{user.name}}" autocomplete="off" maxlength="255" />
					</label>
				</div>
				<hr class="spacer short" />
				<label>
					<span class="label primary">Profile Picture</span>
					<hr class="spacer short" />
					<div class="profile login">
						<img src="{{#thumbnail.url}}150,150{{/thumbnail.url}}" /><br />
						<input type="file" name="image" />
					</div>
				</label>
				<hr class="spacer short" />
				<hr class="spacer short border solid" />
				<hr class="spacer short" />
				<div class="inputs wide">
					<hr class="spacer short" />
					<label>
						<span class="label primary">Select your University</span>
						<select name="primary_university_id" class="toggle-label">
							<option value="0">Other</option>
							{{#universities}}
								<option value="{{university.id}}" {{#isSelected}}selected="selected"{{/isSelected}}}}>{{university.name}}</option>
							{{/universities}}
						</select>
					</label>
				</div>
				<hr class="spacer short" />
				<hr class="spacer short" />
				<label>
					<span class="label primary">Email Address</span>
					<input type="text" class="showAll toggle-label" name="email" value="{{primaryEmail.email}}" autocomplete="off" maxlength="255" />
				</label>
				<hr class="spacer short" />
				<hr class="spacer short" />
				<label class="submit">
					<input type="submit" class="button" value="SAVE" />
				</label>
				<label class="submit">
					<a class="cancel" href="{{#self.url}}edit{{/self.url}}" class="button">Cancel</a>
				</label>
			</form>
			<hr class="spacer short border actual-solid" />
			<hr class="spacer short" />
			<form method="post" action="{{#self.url}}edit{{/self.url}}" class="grid-contain-with-border-bottom">
				<span class="highlight title">Change Password</span>
				<hr class="spacer short" />
				<hr class="spacer short border solid" />
				<label>
					<span class="label primary">New Password</span>
					<input type="password" class="showAll toggle-label" name="password" value="" autocomplete="off" maxlength="255" />
				</label>
				<label>
					<span class="label primary">Verify Password</span>
					<input type="password" class="showAll toggle-label" name="password_verify" value="" autocomplete="off" maxlength="255" />
				</label>
				<hr class="spacer short" />
				<label class="submit">
					<input type="submit" class="button" value="SAVE" />
				</label>
				<label class="submit">
					<a class="cancel" href="{{#self.url}}edit{{/self.url}}" class="button">Cancel</a>
				</label>
			</form>
			<hr class="spacer short border actual-solid" />
			<hr class="spacer short" />
			<span class="highlight title">Social Connections</span>
			<hr class="spacer short" />
			<div class="connections">
				<ul class="share">
					{{#facebookUser}}
						<li>
							<form method="post" action="{{#self.url}}edit{{/self.url}}">
								<input type="hidden" name="disconnect" value="facebook" />
								<button class="fb" type="submit">Disconnect Facebook</button>
							</form>
						</li>
					{{/facebookUser}}
					{{^facebookUser}}
						<li>
							<a href="#" class="fb_button fb_button_medium" onclick="facebookConnect('?connect=facebook')">
								<span id="fb_login_text" class="fb_button_text">Connect with Facebook</span>
							</a>
						</li>
					{{/facebookUser}}
					{{#linkedInUser}}
						<li>
							<form method="post" action="{{#self.url}}edit{{/self.url}}">
								<input type="hidden" name="disconnect" value="linkedin" />
								<button class="fb" type="submit">Disconnect LinkedIn</button>
							</form>
						</li>
					{{/linkedInUser}}
					{{^linkedInUser}}
						<li>
							<a class="foundicon social-icon" href="?connect=linkedin">
								<span class="icon foundicon-linkedin"></span>
								<span class="label">Connect with LinkedIn</span>
							</a>
						</li>
					{{/linkedInUser}}
				</ul>
			</div>
			<!-- <label class="Twitterid">
				<span class="label primary">Twitter Id</span>
				<input type="text" class="showAll toggle-label" name="twitterid" value="{{self.post.twitterid}}" autocomplete="off" maxlength="255" />
			</label> -->
			<hr class="spacer short border actual-solid" />
			<hr class="spacer short" />
			<span class="highlight title">Stripe Connection</span>
			{{#stripeUser}}
				<form method="post" action="{{#self.url}}edit{{/self.url}}" class="grid-contain-with-border-bottom">
					<hr class="spacer short" />
					<input type="hidden" name="disconnect" value="stripe" />
					<button type="submit" class="stripe">Disconnect Stripe</button>
				</form>
			{{/stripeUser}}
			{{^stripeUser}}
				<form method="post" action="{{#self.url}}edit{{/self.url}}" class="grid-contain-with-border-bottom">
					<hr class="spacer short" />
					<input type="hidden" name="connect" value="stripe" />
					<input type="image" src="{{{siteRoot}}}images/stripe/connect.png" alt="Connect with Stripe" />
				</form>
			{{/stripeUser}}
			<hr class="spacer short" />
			<hr class="spacer short" />
			<hr class="spacer short" />
		</div>
	</div>
</div>