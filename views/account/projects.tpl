{{#projects.length}}
	<div class="top">
		<h1 class="column left">
			<div class="body">
				Your Projects
			</div>
		</h1>
		<div class="column right"></div>
	</div>
	<div class="projectpage">
		<div class="middle">
			<div class="column left">
				<h2>You've Started {{projects.length}} Project{{^hasOneProject}}s{{/hasOneProject}}</h2>
			</div>
		</div>
		<div class="slider">
			<div class="slide">
				<div class="projects list">
					<ul>
						{{#projects}}
						{{> projects/partial.project}}
						{{/projects}}
					</ul>
				</div>
			</div>
		</div>
	</div>
{{/projects.length}}
{{^projects.length}}
	<div class="middle">
		<div class="column left">
			<h2>You have not created any projects yet.</h2>
		</div>
	</div>
{{/projects.length}}
{{#projectsBacked.length}}
	<div class="projectpage">
		<div class="middle">
			<div class="column left">
				<h2>You've Backed {{projectsBacked.length}} Project{{^hasOneProjectBacked}}s{{/hasOneProjectBacked}}</h2>
			</div>
		</div>
		<div class="slider">
			<div class="slide">
				<div class="projects list">
					<ul>
						{{#projectsBacked}}
							{{> projects/partial.project}}
						{{/projectsBacked}}
					</ul>
				</div>
			</div>
		</div>
	</div>
{{/projectsBacked.length}}
<hr class="spacer" />
<hr class="spacer" />