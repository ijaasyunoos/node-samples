Verify your email address.
<br /><br />
<form method="post" class="styled-form">
	<input type="hidden" name="resend" value="1" />
	<input type="submit" class="button" value="Resend Your Verification Email" />
</form>
