<div class="title swing border">
	<div class="bd grid-contain-without-border-bottom">
		<div class="swingleft">
			<h1>Register to fund &amp; create projects</h1>
		</div>
		<div class="donut">
			<div class="sprite"></div>
		</div>
	</div>
</div>
<div class="content register swing border">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<div class="subtitle">
					<h2>Register with Facebook or old fashioned email</h2>
				</div>
				<div class="subtitle">
					<a class="foundicon big register-facebook" href="#facebook" onclick="facebookConnectOnLogin('{{{redirect}}}')">
						<span class="icon foundicon-facebook"></span>
						<span class="label">Register with Facebook</span>
					</a>
					<a class="foundicon big register-linkedin" href="#linkedin" onclick="linkedinConnectOnLogin('{{{redirect}}}')">
						<span class="icon foundicon-linkedin"></span>
						<span class="label">Register with LinkedIn</span>
					</a>
				</div>
				<form method="post">
					{{#newUser.hasErrors}}
						{{#newUser.errorsArray}}
							{{> common/form/error}}
						{{/newUser.errorsArray}}
						<br />
					{{/newUser.hasErrors}}
					<label>
						<span class="label primary">Name</span>
						<input type="text" class="showAll toggle-label" name="name" value="{{self.post.name}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Email Address</span>
						<input type="text" class="showAll toggle-label" name="email" value="{{self.post.email}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Verify Email Address</span>
						<input type="text" class="showAll toggle-label" name="email_verify" value="{{self.post.email_verify}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Password</span>
						<input type="password" class="showAll toggle-label" name="password" value="" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Verify Password</span>
						<input type="password" class="showAll toggle-label" name="passwordVerify" value="" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Select your University</span>
						<select name="primary_university_id" class="toggle-label">
							<option value="0">Other</option>
							{{#universities}}
								<option value="{{university.id}}" {{#isSelected}}selected="selected"{{/isSelected}}}}>{{university.name}}</option>
							{{/universities}}
						</select>
					</label>
					<div class="swing">
						<div class="bd">
							<div class="swingleft">
								&nbsp;
								<label class="submit">
									<input type="submit" class="button" value="REGISTER" />
								</label>
							</div>
							<a href="{{#self.url}}login{{/self.url}}?redirect={{redirect}}" class="forgot">
								<span>Already have an account? Log in.</span>
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="info side">
			<p>One-third of the 1,250 business incubators in the United States are at universities</p>
		</div>
	</div>
</div>