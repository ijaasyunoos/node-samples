Hi {{newUser.name}},
<br /><br /> 
Your email address has changed.
<br /><br />
Please click the following link to verify your new email address:<br />
<a href="{{#paraglide.longUrl}}account,verify,{{verificationCode}}{{/paraglide.longUrl}}" target="_blank">
	{{#paraglide.longUrl}}account,verify,{{verificationCode}}{{/paraglide.longUrl}}
</a>
<br /><br />
-{{company}}
