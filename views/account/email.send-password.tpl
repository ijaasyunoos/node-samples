Hi {{forgotUser.name}},
<br /><br />
Thank you for signing up for an account. 
<br /><br />
Please click the following link to reset the password on your account:<br />
<a href="{{#paraglide.longUrl}}account,reset-password,{{verificationCode}}{{/paraglide.longUrl}}" target="_blank">
	{{#paraglide.longUrl}}account,reset-password,{{verificationCode}}{{/paraglide.longUrl}}
</a>
<br /><br />
-{{company}}
