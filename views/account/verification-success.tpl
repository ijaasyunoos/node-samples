<div class="content swing border solid forgotPassword verification success">
	<div class="subtitle">
		<h2>Your email has been verified.</h2>
		<br /><br />
		<a href="{{#self.url}}{{/self.url}}" class="psuedobutton">
			<span>
				Go to your account.
			</span>
		</a>
	</div>
</div>
