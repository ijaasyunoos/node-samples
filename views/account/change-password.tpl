<div class="content changePassword">
	{{#user.hasErrors}}
		{{#user.errorsArray}}
			{{> common/form/error}}
		{{/user.errorsArray}}
		<br />
	{{/user.hasErrors}}
	<form method="post">
		<label>
			<span class="label primary">Password</span>
			<input type="password" class="showAll" name="password" value="" autocomplete="off" maxlength="255" />
		</label>
		<label>
			<span class="label primary">Verify Password</span>
			<input type="password" class="showAll" name="password_verify" value="" autocomplete="off" maxlength="255" />
		</label>
		<hr class="spacer border solid short">
		<div class="swing">
			<div class="bd">
				<div class="swingleft">
					<label>
						<input type="submit" class="button" value="Save" />
					</label>
					<a href="{{#self.url}}edit{{/self.url}}" class="psuedobutton">
						<span>Cancel</span>
					</a>
				</div>
			</div>
		</div>
	</form>
</div>
