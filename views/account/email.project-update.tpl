A project you funded on
<a href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}">{{company}}</a>,
"<a href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{{projectUrl}}}">{{project.title}}</a>",
has published a new update:
<br /><br />
<a href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{{updateUrl}}}">{{update.title}}</a>
<br /><br />
{{#preserveLinebreaks}}{{update.description}}{{/preserveLinebreaks}}
