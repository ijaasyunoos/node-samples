Manage Stripe Connection
<br /><br />
{{#user.hasErrors}}
	{{#user.errorsArray}}
		{{> common/form/error}}
	{{/user.errorsArray}}
	<br />
{{/user.hasErrors}}
{{#stripeUser}}
	<form method="post">
		Stripe Connect
		<input type="hidden" name="disconnect" value="1" />
		<input type="submit" value="Disconnect" />
	</form>
{{/stripeUser}}
{{^stripeUser}}
	Stripe Connect
	<form method="post">
		<input type="hidden" name="connect" value="1" />
		<input type="image" src="{{{siteRoot}}}images/stripe/connect.png" alt="Connect with Stripe" />
	</form>
{{/stripeUser}}

