<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>Investor Signup</h1>
		</div>
		<div class="donut">
			<div class="sprite"></div>
		</div>
	</div>
</div>
<div class="content register swing border">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<div class="subtitle">
					<h2>Please enter your details</h2>
				</div>
				<form method="post">
					{{#newUser.hasErrors}}
						{{#newUser.errorsArray}}
							{{> common/form/error}}
						{{/newUser.errorsArray}}
						<br />
					{{/newUser.hasErrors}}
					<label>
						<span class="label primary">Name</span>
						<input type="text" class="showAll toggle-label" name="name" value="{{self.post.name}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Date of Birth</span>
						<input class="date-with-year" type="text" class="showAll toggle-label" name="date_of_birth" value="{{self.post.dateOfBirth}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Email Address</span>
						<input type="text" class="showAll toggle-label" name="email" value="{{self.post.email}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Verify Email Address</span>
						<input type="text" class="showAll toggle-label" name="email_verify" value="{{self.post.emailVerify}}" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Password</span>
						<input type="password" class="showAll toggle-label" name="password" value="" autocomplete="off" maxlength="255" />
					</label>
					<label>
						<span class="label primary">Verify Password</span>
						<input type="password" class="showAll toggle-label" name="passwordVerify" value="" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short border solid" />
					<div class="quotebox">
						<p>We need to ask you a few quick questions before we can share information about the companies... AlumniFunder has lots of great companies with interesting investment opportunities. But in order to protect our community, we only show details to Accredited Investors.</p>
					</div>
					<hr class="spacer short" />
					<hr class="spacer short border solid" />
					<div class="swing">
						<div class="bd">
							<div class="swingleft">
								<h2>Investor Accreditation</h2>
							</div>
							<label>
								<span class="label primary">check at least one box</span>
							</label>
						</div>
					</div>
					<hr class="spacer short border solid" />
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_single_income" value="1" {{#newUser.investorRequirementSingleIncome}}checked="checked"{{/newUser.investorRequirementSingleIncome}} autocomplete="off" />
						<p>I have made $200,000 or more in each of the two most recent years and believe I will make at least that much this year.</p>
					</label>
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_joint_income" value="1" {{#newUser.investorRequirementJointIncome}}checked="checked"{{/newUser.investorRequirementJointIncome}} autocomplete="off" />
						<p>I have a joint income with my spouse that has exceeded $300,000 for each of the last 2 years and I expect it will that again this year.</p>
					</label>
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_net_worth" value="1" {{#newUser.investorRequirementNetWorth}}checked="checked"{{/newUser.investorRequirementNetWorth}} autocomplete="off" />
						<p>I have an individual net worth, or joint net worth with my spouse, that exceeds $1,000,000 today excluding my primary residence.</p>
					</label>
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_financial_representative" value="1" {{#newUser.investorRequirementFinancialRepresentative}}checked="checked"{{/newUser.investorRequirementFinancialRepresentative}} autocomplete="off" />
						<p>I am a representative of a bank, insurance company, registered investment company, business development, or small business investment company interested in investment opportunities on AlumniFunder.</p>
					</label>
					<hr class="spacer short border solid clear" />
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_agree_tos" value="1" {{#self.post.investorRequirementAgreeTos}}checked="checked"{{/self.post.investorRequirementAgreeTos}} autocomplete="off" />
						<p>
							Yes, I agree to the Alumnifunder
							<a target="_blank" href="{{#paraglide.url}}pages,investor-terms{{/paraglide.url}}">Terms of Use</a>,
							and promise to keep the information I learn about these companies confidential.
						</p>
					</label>
					<label class="questions">
						<input type="checkbox" class="showAll toggle-label" name="investor_requirement_accredited" value="1" {{#newUser.investorRequirementAccredited}}checked="checked"{{/newUser.investorRequirementAccredited}} autocomplete="off" />
						<p>Yes, I certify that I am an accredited investor.</p>
					</label>
					<hr class="spacer short border solid clear" />
					<div class="swing">
						<div class="bd">
							<div class="swingleft">
								<label class="submit">
									<input type="submit" class="button" value="REGISTER" />
								</label>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>