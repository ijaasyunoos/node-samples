<div class="content swing border solid login">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<hr class="spacer border short">
				{{#self.hasErrors}}
					{{#self.errorsArray}}
						{{> common/form/error}}
					{{/self.errorsArray}}
				{{/self.hasErrors}}
				<form method="post">
					<div class="grid-contain-with-border-bottom">
						<label>
							<span class="label primary">Email Address</span>
							<input type="text" class="showAll toggle-label" name="email" value="{{self.post.email}}" autocomplete="off" maxlength="255" />
						</label>
						<label>
							<span class="label primary">Password</span>
							<input type="password" class="showAll toggle-label" name="password" value="" autocomplete="off" maxlength="255" />
						</label>
					</div>
					<hr class="spacer border solid short">
					<div class="swing">
						<div class="bd">
							<div class="swingleft">
								<label class="submit">
									<input type="submit" class="button" value="LOGIN" />
								</label>
								<a href="{{#self.url}}register{{/self.url}}?redirect={{redirect}}" class="psuedobutton">
									<span>REGISTER</span>
								</a>
							</div>
							<a href="{{#self.url}}forgot-password{{/self.url}}" class="forgot">
								<span>Request Password</span>
							</a>
						</div>
					</div>
				</form>
			</div>
			<hr class="spacer" />
			<hr class="spacer" />
			<hr class="spacer" />
		</div>
		{{^isEquity}}
			<div class="info side vanilla">
				<div class="external">
					<a href="#facebook" class="foundicon big social-icon" onclick="facebookConnectOnLogin('{{{redirect}}}')">
						<span class="icon foundicon-facebook"></span>
						<span class="label">Login with Facebook</span>
					</a>
				</div>
				<br />
				<div class="external">
					<a class="foundicon big social-icon" href="{{#paraglide.url}}account,login,,connect=linkedin{{/paraglide.url}}">
						<span class="icon foundicon-linkedin"></span>
						<span class="label">Login with LinkedIn</span>
					</a>
				</div>
			</div>
		{{/isEquity}}
	</div>
</div>