Manage Social Connections
<br /><br />
{{#user.hasErrors}}
	{{#user.errorsArray}}
		{{> common/form/error}}
	{{/user.errorsArray}}
	<br />
{{/user.hasErrors}}
{{#facebookUser}}
	<form method="post">
		Facebook Connect
		<input type="hidden" name="disconnect" value="facebook" />
		<input type="submit" value="Disconnect" />
	</form>
{{/facebookUser}}
{{^facebookUser}}
	Facebook Connect
	<a href="#" class="fb_button fb_button_medium" onclick="facebookConnect('?connect=facebook')">
		<span id="fb_login_text" class="fb_button_text">Connect with Facebook</span>
	</a>
{{/facebookUser}}
