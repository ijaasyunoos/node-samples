<div class="title stretch">
	<h1>Contact Us</h1>
</div>

<div class="content swing noborder">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<hr class="spacer" />
				<p>
					Welcome to the AlumniFunder public beta, and thanks for participating in this grand experiment! If you encounter any technical issues with our site please send an email to <a href="mailto:engineering@alumnifunder.com">engineering@alumnifunder.com</a>.</p>
				<p>Otherwise if you'd like to contact us for whatever reason please refer to our contact information below.</p>
				<p>AlumniFunder, Inc.<br />2218 1/2 5th Street<br />Santa Monica, CA 90405<br /><a href="mailto:support@alumnifunder.com">support@alumnifunder.com</a></p>
			</div>
		</div>
	</div>
</div>