<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>All Companies</h1>
		</div>
	</div>
</div>

{{#companies.length}}
	<h2>{{companies.length}} companies</h2>
	<hr class="spacer medium" />
	<div class="content wide">
		<hr class="spacer short border solid" />
		<div class="projects list">
			<ul>
				{{#companies}}
					{{> projects/partial.company}}
				{{/companies}}
			</ul>
			<hr class="spacer" />
		</div>
	</div>
{{/companies.length}}
