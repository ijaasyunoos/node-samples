<li class="project">
	<a href="{{{companyUrl}}}" class="link" title="{{company.title}}"></a>
	<div class="img-overlay unfocused">
		<img src="{{#thumbnail.url}}226,0,0{{/thumbnail.url}}" alt="project">
	</div>
	<h4>
		{{company.title}}
	</h4>
	<div class="details">
		<ul>
			<li class="detail name">
				<a href="{{#paraglide.url}}users,{{owner.id}}{{/paraglide.url}}" class="owner">{{owner.name}}</a>
			</li>
			<li class="detail amount">
				${{#numberFormat}}{{company.amountGoal}}{{/numberFormat}}
			</li>
			<li class="detail description">
				{{#preserveLinebreaks}}{{company.timeRemaining}}{{/preserveLinebreaks}}
			</li>
		</ul>
		<div class="progress">
			<span class="percent">{{company.amountRaisedPercentage}}%</span>
			<div class="fill" style="height:{{company.amountRaisedPercentage}}%"></div>
		</div>
	</div>
</li>