<div class="content viewProfile">
	<div class="title stretch">
		<div class="outer-title">
			<img src="{{#thumbnail.url}}150,150{{/thumbnail.url}}" />
			<h1>{{profileUser.firstName}}<br />{{profileUser.lastName}}</h1>
		</div>
	</div>
	{{#isAdmin}}
		<form method="post"> 
			<input type="hidden" name="login" value="1" />
			<input type="submit" value="Login as this user" />
		</form>
	{{/isAdmin}}
	<form method="post">
		<div class="content swing">
			<div class="bd">
				<hr class="spacer short" />
				<div class="swingleft">
					<span class="highlight title">{{profileUser.firstName}}'s Projects</span>
				</div>
				<div>
					<a href="#">
						<span class="vanilla title projects numbers">{{projects.length}}</span>
					</a>
					<a class="plus" href="#">
						<img src="{{{siteRoot}}}images/plus.png" />
					</a>
				</div>
			</div>
		</div>
		<hr class="spacer short" />
		<hr class="spacer short border solid" />
		<div class="content">
			<div class="projects list">
				<ul>
					{{#projects}}
						{{> projects/partial.project}}
					{{/projects}}
					{{^projects}}
						<hr class="spacer short" />
						<h2 class="message">This user has not created any projects.</h2>
					{{/projects}}
				</ul>
			</div>
			{{#projectsBacked.length}}
				<div class="swingleft">
					<span class="highlight title">Projects {{profileUser.firstName}} has backed</span>
				</div>
				<hr class="spacer short border solid" />
				<hr class="spacer short" />
				<div class="projects list">
					<ul>
						{{#projectsBacked}}
							{{> projects/partial.project}}
						{{/projectsBacked}}
					</ul>
					<hr class="spacer short" />
				</div>
			{{/projectsBacked.length}}
			{{#updates.length}}
				<div class="swingleft">
					<span class="highlight title">{{profileUser.firstName}}'s updates</span>
				</div>
				<hr class="spacer short border solid" />
				<hr class="spacer short" />
				<div class="newsfeeds">
					<ul>
						{{#updates}}
							<li class="feed">
								<ul>
									<li class="title">
										{{title}}
									</li>
									<li class="content">
										{{description}}
									</li>
								</ul>
							</li>
						{{/updates}}
					</ul>
					<hr class="spacer short" />
				</div>
			{{/updates.length}}
		</div>
		<hr class="spacer short" />
		<hr class="spacer short" />
	</form>
</div>
