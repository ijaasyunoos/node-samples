<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>AlumniFunder</title>
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/extra.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/main.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.datepicker.css" />
	<link rel="image_src" type="image/jpeg" href="{{{siteRoot}}}images/logo_fb.png" />
	<!-- JAVASCRIPT -->
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.position.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.slider.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.timepicker.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.svg/jquery.svg.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.cycle.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/pixastic/pixastic.core.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/pixastic/pixastic.jquery.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/main.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/facebook.js"></script>
	<!--[if lt IE 9]>
		<script src="{{{siteRoot}}}scripts/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35078774-1']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</head>
<!-- 
<body class="{{bodyClass}} controller-{{paraglide.controller}} action-{{paraglide.action}}">
-->
<body class="{{bodyClass}} controller-{{paraglide.controller}} action-{{paraglide.action}}">
	<script type="text/javascript">
		var baseUrl = '{{{siteRoot}}}';
		var grid_color = '{{sectionColor}}';
	</script>
	{{{PAGE_CONTENT}}}
	<!-- footer -->
	<div class="footerb">
		<div class="wrap footer swing border">
			<div class="bd">
				<div class="navigation swingleft">
					<ul>
						<li>
							<a href="{{#paraglide.url}}pages,terms{{/paraglide.url}}">terms and conditions</a> |
						</li>
						<li>
							<a href="{{#paraglide.url}}pages,privacy{{/paraglide.url}}">privacy policy</a> |
						</li>
						<li>
							<a href="{{#paraglide.url}}contact{{/paraglide.url}}">contact us</a>
						</li>
					</ul>
				</div>
				<div class="copyright">&copy; AlumniFunder {{currentYear}}</div>
			</div>
		</div>
	</div>
	<div id="fb-root"></div>
</body>
</html>