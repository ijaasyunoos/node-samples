<div class="header">
	<div class="wrap swing">
		<div class="bd">
			<div class="navigation swingleft">
				<span>Claremont Competition</span>
			</div>
			<div class="social">
				<ul>
					<li>
						<a class="foundicon small" href="https://twitter.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-twitter"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="http://www.linkedin.com/company/2743250" target="_blank">
							<span class="icon foundicon-linkedin"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="https://www.facebook.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-facebook"></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main">
	<div class="content wrap">
		<div class="logo">
			<a href="{{paraglide.url}}">
				<img src="{{{siteRoot}}}images/logo_white.png" alt="alumni funder" />
			</a>
		</div>
		<!-- CONTENT -->
		<div class="title swing border">
			<div class="bd">
				<div class="swingleft">
					<h1>$1000 for the most<br />disruptive project</h1>
				</div>
				<div class="register impulse">
					<!--{{#paraglide.url}}account,register{{/paraglide.url}}-->
					<a href="http://www.cmc.alumnifunder.com/account/register">
						<span>register now</span>
					</a>
				</div>
			</div>
		</div>
		<div class="content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="subtitle">
						<hr class="spacer short">
						<h2>Calling all Claremont McKenna innovators and doers. Join the crowdfunding experiment by uploading your project to CMC's private crowdfunding portal by March 1st and automatically be entered to win $1,000 to jumpstart your campaign.</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft arial" style="border:0;">
					<div class="col left">
						<p>There are no constraints to the type of project or startup vision that you upload, just that it be awesome! Consider this a crowdfunding "Pitch" competition, with the end result being a shot at $1,000 and a chance to raise your project's full funding from CMC alumni.</p>
						<p>Projects can be startups looking for seed capital, social entrepreneurship endeavors, or on-campus development initiatives. In circumstances where there is an excess of funds for a specific student project, Kravis Leadership Institute (KLI) will apply this excess towards the funding of student entrepreneurial projects.</p>
					</div>
					<div class="col right">
						<p>The stellar panel of alumni judges will be announcing the winner on Friday, March 7th!</p>
						<p>To enter this contest, simply click the register button on the right to create an account on Claremont McKenna's private crowdfunding platform, then upload your project (this will require a Prezi or a Video) by March 1st and you'll be automatically entered to win. It's that easy.</p>
						<p>Please email contest@alumnifunder.com with any questions!</p>
					</div>
				</div>
				<div class="info side vanilla login">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="col third">
						<div class="judges">
							<h4 class="title">Judges</h4><br />
						</div>
					</div>
					<div class="col rest">
						<div class="judges">
							<ul>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/tina-daniels.png" />                        
										</li>
										<li class="name">
											<span>Tina Daniels ’93</span>
										</li>
										<li class="company">
											<span>Chief Revenue Officer,<br>Syncapse</span>
										</li>
									</ul>
								</li>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/bret-jorgensen.png" />                          
										</li>
										<li class="name">
											<span>Bret W. Jorgensen ’81</span>
										</li>
										<li class="company">
											<span>Chairman, Crossover Health</span>
										</li>
									</ul>
								</li>							
							</ul>
							<br>
							<ul>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/kip-halman.png" />
										</li>
										<li class="name">
											<span>Kip Hallman ’81 P’14</span>
										</li>
										<li class="company">
											<span>Chairman & CEO, CFMG, Inc.</span>
										</li>
									</ul>
								</li>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/richard-chino.png" />
										</li>
										<li class="name">
											<span>Richard Chino ’90</span>
										</li>
										<li class="company">
											<span>Pasadena Angels,<br>Board Member,<br>C2FO (Pollenware)</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="info side vanilla login">
					<div class="register impulse">
						<!--register-->
						<a href="http://www.cmc.alumnifunder.com/account/register">
							<span>register now</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>