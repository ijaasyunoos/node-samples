<div class="header">
	<div class="wrap swing">
		<div class="bd">
			<div class="navigation swingleft">
				<span>Georgetown Competition</span>
			</div>
			<div class="social">
				<ul>
					<li>
						<a class="foundicon small" href="https://twitter.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-twitter"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="http://www.linkedin.com/company/2743250" target="_blank">
							<span class="icon foundicon-linkedin"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="https://www.facebook.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-facebook"></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main">
	<div class="content wrap">
		<div class="logo">
			<a href="{{paraglide.url}}">
				<img src="{{{siteRoot}}}images/logo_white.png" alt="alumni funder" />
			</a>
		</div>
		<!-- CONTENT -->
		<div class="title swing border">
			<div class="bd">
				<div class="swingleft">
					<h1>$1000 for the most<br />disruptive project</h1>
				</div>
				<div class="register impulse">
					<a href="{{#paraglide.url}}account,register{{/paraglide.url}}">
						<span>register now</span>
					</a>
				</div>
			</div>
		</div>
		<div class="content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="subtitle">
						<h2>Calling all Georgetown innovators and doers. Join the crowdfunding experiment by uploading your project to AlumniFunder by March 20th and be entered to win $1,000 to jumpstart your campaign.</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft" style="border:0;">
					<div class="col left">
						<p>There are no constraints to the type of project or company that you upload, just that it be awesome! Consider this a crowdfunding "Pitch" competition, with the end result being a shot at $1,000 and a chance that your full project will be funded.</p>
						<p>Projects could be startups looking for seed capital, social entrepreneurship endeavors, or even on-campus development initiatives.</p>
					</div>
					<div class="col right">
						<p>Our stellar panel of judges will be announcing the winner on March 25th!</p>
						<p>To enter this contest, simply click the register button on the right to create an AlumniFunder account, then upload your project (this will require a Prezi or a Video) by March 20th and you'll be automatically entered to win. It's that easy!</p>
						<p>Please email contest@alumnifunder.com with any questions!</p>
					</div>
				</div>
				<div class="info side vanilla login">
					&nbsp;
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="col third">
						<div class="judges">
							<h4 class="title">Judges</h4><br />
						</div>
					</div>
					<div class="col rest">
						<div class="judges">
							<ul>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/ellie-wheeler.png" />                          
										</li>
										<li class="name">
											<span>Ellie Wheeler</span>
										</li>
										<li class="company">
											<span>Senior Associate, <br />Greycroft Partners</span>
										</li>
									</ul>
								</li>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/brendan-hughes.png" />
										</li>
										<li class="name">
											<span>Brendan Hughes</span>
										</li>
										<li class="company">
											<span>Senior Associate,<br />Cooley LLP</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="info side vanilla login">
					<div class="register impulse">
						<a href="register">
							<span>register now</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>  