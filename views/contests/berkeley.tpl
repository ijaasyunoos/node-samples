<div class="header">
	<div class="wrap swing">
		<div class="bd">
			<div class="navigation swingleft">
				<span>Berkeley Competition</span>
			</div>
			<div class="social">
				<ul>
					<li>
						<a class="foundicon small" href="https://twitter.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-twitter"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="http://www.linkedin.com/company/2743250" target="_blank">
							<span class="icon foundicon-linkedin"></span>
						</a>
					</li>
					<li>
						<a class="foundicon small" href="https://www.facebook.com/AlumniFunder" target="_blank">
							<span class="icon foundicon-facebook"></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main">
	<div class="content wrap">
		<div class="logo">
			<a href="{{paraglide.url}}">
				<img src="{{{siteRoot}}}images/logo_white.png" alt="alumni funder" />
			</a>
			<a href="http://www.veritasprep.com">
				<img src="{{{siteRoot}}}images/veritas_logo.png" class="second" alt="veritas logo" />
			</a>
		</div>
		<!-- CONTENT -->
		<div class="title swing border">
			<div class="bd">
				<div class="swingleft">
					<h1><span class="cash">$1000</span> for the most<br />disruptive project</h1>
				</div>
				<div class="register impulse">
					<a href="{{#paraglide.url}}account,register{{/paraglide.url}}">
						<span>register now</span>
					</a>
				</div>
			</div>
		</div>
		<div class="content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="subtitle">
						<hr class="spacer short" />
						<h2>DEADLINE EXTENDED to accomodate for finals! Calling all Berkeley innovators and doers. Join the crowdfunding experiment by uploading your project to AlumniFunder by May 19th and be entered to win $1,000 to jumpstart your campaign.</h2>
					</div>
				</div>
				<div class="swingright">
					<hr class="spacer short" />
					<a href="http://www.veritasprep.com">
						<img alt="veritas" src="{{{siteRoot}}}images/veritas.png" />
					</a>
					<div class="subtitle sponsor">
						<h2>Thanks to our sponsors, Veritas Prep, who are underwriting this contest!</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft" style="border:0;">
					<div class="col left">
						<p>There are no constraints to the type of project or company that you upload, just that it be awesome! Consider this a crowdfunding "Pitch" competition, with the end result being a shot at $1,000 and a chance that your full project will be funded.</p>
						<p>Projects could be startups looking for seed capital, social entrepreneurship endeavors, or even on-campus development initiatives.</p>
					</div>
					<div class="col right">
						<p>Our stellar panel of judges will be announcing the winner on May 23rd!</p>
						<p>To enter this contest, simply click the register button on the right to create an AlumniFunder account, then upload your project (this will require a Prezi or a Video) by May 19th and you'll be automatically entered to win. It's that easy!</p>
						<p>Please email contest@alumnifunder.com with any questions!</p>
					</div>
				</div>
				<div class="swingright" style="border:0;">
					<p>Veritas Prep is the largest privately owned GMAT preparation and admissions consulting firm. They have helped more than 50,000 students gain admittance to the world's most elite business schools.</p>
				</div>
			</div>
		</div>
		<div class="sub content register swing border">
			<div class="bd">
				<div class="swingleft">
					<div class="col third">
						<div class="judges">
							<h4 class="title">Judges</h4><br />
						</div>
					</div>
					<div class="col rest">
						<div class="judges">
							<ul>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/libby-leffler.png" />                          
										</li>
										<li class="name">
											<span>Libby Leffler</span>
										</li>
										<li class="company">
											<span>Strategic Partner Manager, <br />Facebook</span>
										</li>
									</ul>
								</li>
								<li class="judge">
									<ul>
										<li class="image">
											<img src="{{{siteRoot}}}images/judges/146x146/kevin-casey.png" />
										</li>
										<li class="name">
											<span>Kevin Casey</span>
										</li>
										<li class="company">
											<span>Founder + CEO, <br />New Avenue</span>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="info side vanilla login">
					<div class="register impulse">
						<a href="register">
							<span>register now</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>