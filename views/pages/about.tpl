<div class="title swing border">
	<div class="bd grid-contain-without-border-bottom">
		<div class="swingleft">
			<h1>Our<br /> Manifesto</h1>
		</div>
		<div class="donut">
			<div class="sprite"></div>
		</div>
	</div>
</div>
<div class="content swing border">
	<div class="bd">
		<div class="swingleft">
			<div class="reg top about">
				<div class="subtitle">
					<h2>&quot;Crowdfunding is just a tool.&quot;</h2>
				</div>
				<p>AlumniFunder exists to build a marketplace for disruptive initiatives, where youthfully minded doers can seek biased capital and advice from a "crowd" with shared intellectual and collegiate experiences.</p>
				<p>Crowdfunding, however, is not a panacea. It is simply a technological tool for pooling resources. AlumniFunder aims to use this technology to facilitate access to capital for smart, hungry, foolish yet capable dreamers. People who are willing to put their ideas and ultimately themselves in front of their alumni community in the hope of executing on their projects and ultimately, their companies.</p>
				<p>AlumniFunder strives to create an alternative path to creative, social, charitable, and business success, one that fosters a diverse ecosystem of entrepreneurship in all its myriad forms within established alumni networks, extending the mission of the university beyond its physical walls.</p>
			</div>
		</div>
		<div class="info side">
			<p>Whereas crowdfunding has been around for hundreds of years, and Kickstarter pioneered the notion of crowdfunding creative projects, Congress recently signed into law a bill that legalizes the crowdfunding of business endeavors in exchange for equity. This should, depending on SEC rulemaking, be in effect early next year.</p>
		</div>
	</div>
</div>
<div class="content swing border">
	<div class="bd">
		<div class="swingleft">
			<div class="reg about">
				<div class="subtitle">
					<h2>Our Team</h2>
				</div>
				<div class="team">
					<ul>
						<!-- Ryan Meyer -->
						<li class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/ryan.jpg" alt="ryan" />
								</li>
								<li class="name">
									<span>S. Ryan Meyer</span>
								</li>
								<li class="title">
									<span>Founder</span>
								</li>
								<li class="about">
									<p>Attended Georgetown University (as a merit-based John Carroll Scholar) and graduated from its School of Foreign Service in 1999.</p>
									<p>Ryan spent nearly a decade with Dai Nippon Printing (DNP), a Fortune Global 500 company, ultimately running their automotive division in the US.</p>
									<p>His team was awarded the company’s highest honor for performance in 2010. Concurrent to his work with DNP, Ryan co-founded Minds-in-Motion Labs, a children's brain-training company.</p>
								</li>
							</ul>
						</li>
						<!-- Jerome -->
						<li class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/jerome.jpg" alt="jerome" />
								</li>
								<li class="name">
									<span>Jerome Iveson</span>
								</li>
								<li class="title">
									<span>Visionary Designer</span>
								</li>
								<li class="about">
									<p>Jerome is the founder and lead product designer at Thrive a fast growing SaaS company based in the UK (www.ThriveSolo.com).</p>
									<p>In what seems a lifetime ago Jerome worked as a graphic designer. Jerome has started two companies, done quite a bit of consultancy, successfully raised VC funding and somewhere in-between managed to sire two children.</p>
									<p>His first business Distil traded successfully for 11 years working with regional and international clients.</p>
									<p>Specialising in branding, graphic design and web design. His current company Thrive was born from his desire to try something new and his passion for design and the creative industries.</p>
								</li>
							</ul>
						</li>
						<!-- Brandon -->
						<li class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/brandon.jpg" alt="brandon" />
								</li>
								<li class="name">
									<span>Brandon Goldman</span>
								</li>
								<li class="title">
									<span>Technical Co-Founder</span>
								</li>
								<li class="about">
									<p>Brandon is a veteran software engineer, with a rock solid resume that includes a very early position at Box. Prior to AlumniFunder, Brandon was VP of engineering at Miso Media, a Google Ventures backed company.</p>
									<p>Always on the bleeding edge of technology, Brandon coded AlumniFunder in Node.js. He still finds ample time to develop a wide range of projects at www.brandongoldman.com.</p>
								</li>
							</ul>
						</li>
						<!-- Charlotte -->
						<li style="clear:both" class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/charlotte.jpg" alt="charlotte" />
								</li>
								<li class="name">
									<span>Charlotte Crivelli</span>
								</li>
								<li class="title">
									<span>Public Relations Maven</span>
								</li>
								<li class="about">
									<p>Charlotte has worked in public relations and marketing for tech companies for more than 11 years on global companies such as Bloomberg, Intel, iRobot, Adobe and Palm to name a few.</p>
									<p>Charlotte has worked at some of the biggest agencies, such as Edelman, and companies in the world on in cities such as New York, London and Sydney.</p>
									<p>Now, Charlotte works at the international, award-winning agency that she co-founded, Klick Communications.</p>
								</li>
							</ul>
						</li>
						<!-- Ludovic -->
						<li class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/ludovic.jpg" alt="ludovic" />
								</li>
								<li class="name">
									<span>Ludovic Henry</span>
								</li>
								<li class="title">
									<span>Developer</span>
								</li>
								<li class="about">
									<p>Ludovic spent the summer away from France interning at Mogreet in Silicon Beach, and quite literally formed the backbone of AlumniFunder by helping us make the initial working prototype during a Startup Weekend in Santa Monica.</p>
									<p>Ludo's talents are prodigious, and he will surely be disrupting the French startup scene once he graduates University.</p>
								</li>
							</ul>
						</li>
						<!-- Ijaas -->
						<li class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/ijaas.jpg" alt="ijaas" />
								</li>
								<li class="name">
									<span>Ijaas Yunoos</span>
								</li>
								<li class="title">
									<span>UX Specialist</span>
								</li>
								<li class="about">
									<p>As a self-thought designer and developer Ijaas brings an out of the box creative approach to the team.</p>
									<p>He is constantly working towards improving the user interface of AlumniFunder with usability as the key focus while staying true to the unique design philosophy created by the team.</p>
								</li>
							</ul>
						</li>
						<!-- Drew -->
						<li style="clear:both" class="grid-contain-without-border-bottom">
							<ul class="member">
								<li class="image">
									<img src="{{{siteRoot}}}images/team/182x182/drew.jpg" alt="drew" />
								</li>
								<li class="name">
									<span>Drew Kitch</span>
								</li>
								<li class="title">
									<span>CSS & HTML Pirate</span>
								</li>
								<li class="about">
									<p>Another veteran from Box, Drew took off from Silicon Valley to work in wilderness therapy for three years.</p>
									<p>After spending a summer at Facebook and a fall at Dev Bootcamp, Drew is back in the tech scene and putting his skills to work assisting on the front end development of Alumnifunder.</p>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="info side vanilla">
			&nbsp;
		</div>
	</div>
</div>