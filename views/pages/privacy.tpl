<div class="title stretch">
	<h1>Our Privacy<br />Policy</h1>
</div>
<div class="content swing noborder">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<hr class="spacer" />
				<p>
					This privacy policy ("Policy") describes how AlumniFunder, Inc. and its related companies ("Company") collect, use and share personal information of consumer users of this website, http://alumnifunder.com (the "Site"). This Policy also applies to any of our other websites that post this Policy. This Policy does not apply to websites that post different statements.
					<br /><br />
					<strong>WHAT WE COLLECT</strong>
					<br /><br />
					We get information about you in a range of ways.	
					<br /><br />
					Information You Give Us.	We collect your‎ name,‎ email address,‎ phone number,‎ username,‎ password,‎ demographic information (such as your gender and occupation), University Degree and other information you directly give us on our Site.
					Information We Get from Others.	 We may get information about you from other sources. We may add this to information we get from this Site.
Information Automatically Collected.	We automatically log information about you and your computer. For example, when visiting our Site, we log‎ your computer operating system type,‎ browser type,‎ browser language,‎ the website you visited before browsing to our Site,‎ pages you viewed,‎ how long you spent on a page,‎ access times,‎ Internet protocol (IP) address, and information about your use of and actions on our Site.
					Cookies.	We may log information using "cookies." Cookies are small data files stored on your hard drive by a website. Cookies help us make our Site and your visit better. We use cookies to see which parts of our Site people use and like and to count visits to our Site.
					<br /><br />
					Web Beacons.	We may log information using digital images called Web beacons on our Site or in our emails. We use Web beacons to manage cookies, count visits, and to learn what marketing works and what does not.	 We also use Web Beacons to tell if you open or act on our emails.
					<br /><br />
					<strong>USE OF PERSONAL INFORMATION</strong>
					<br /><br />
					We use your personal information as follows:
					<br /><br />
					<strong>SHARING OF PERSONAL INFORMATION</strong>
					<br /><br />
					We may share personal information as follows:
					<br /><br />
					We may share personal information with your consent. For example, you may let us share personal information with others for their own marketing uses. Those uses will be subject to their privacy policies.
					<br />
					We may share personal information when we do a business deal, or negotiate a business deal, involving the sale or transfer of all or a part of our business or assets. These deals can include any merger, financing, acquisition, or bankruptcy transaction or proceeding.
					<br />
					We may share personal information for legal, protection, and safety purposes.
					<br />
					We may share information to comply with laws.
					<br />
					We may share information to respond to lawful requests and legal processes.
					<br />
					We may share information to protect the rights and property of AlumniFunder, Inc., our agents, customers, and others. This includes enforcing our agreements, policies, and terms of use.
					<br />
					We may share information in an emergency. This includes protecting the safety of our employees and agents, our customers, or any person.
					<br />
					We may share information with those who need it to do work for us.
					<br />
					We may also share aggregated and/or anonymized data with others for their own uses.
					<br /><br />
					<strong>INFORMATION CHOICES AND CHANGES</strong>
					<br /><br />
					Our marketing emails tell you how to "opt-out." If you opt out, we may still send you non-marketing emails. Non-marketing emails include emails about your accounts and our business dealings with you.
					You may send requests about personal information to our Contact Information below.	You can request to change contact choices, opt-out of our sharing with others, and update your personal information.
					You can typically remove and reject cookies from our Site with your browser settings. Many browsers are set to accept cookies until you change your settings. If you remove or reject our cookies, it could affect how our Site works for you.
					<br /><br />
					<strong>SECURITY OF YOUR PERSONAL INFORMATION.</strong>
					<br /><br />
					We take steps to help protect personal information. No company can fully prevent security risks, however.	 Mistakes may happen. Bad actors may defeat even the best safeguards.
					<br /><br />
					<strong>CONTACT INFORMATION.</strong>
					<br /><br />
					We welcome your comments or questions about this privacy policy.	You may also contact us at our address:
					<br />
					AlumniFunder, Inc.<br />
					2218 1/2 5th ST<br />
					Santa Monica, CA	90405
					<br /><br />
					<strong>CHANGES TO THIS PRIVACY POLICY.</strong>
					<br /><br />
					We may change this privacy policy. If we make any changes, we will change the Last Updated date below.
					<br /><br />
					This privacy policy was last updated on September 29, 2012.
				</p>
			</div>
		</div>
	</div>
</div>