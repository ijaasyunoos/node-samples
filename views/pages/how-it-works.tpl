<div class="title swing border">
	<div class="bd grid-contain-without-border-bottom">
		<div class="swingleft">
			<h1>How Alumni<br />Funder Works</h1>
		</div>
		<div class="donut">
			<div class="sprite"></div>
		</div>
	</div>
</div>
<div class="content swing border">
	<div class="bd">
		<div class="swingleft">
			<div class="how">
				<div class="subtitle">
					<h2>It's really, really simple</h2>
				</div>
				<ol>
					<li class="grid-contain-with-border-bottom">
						<div>
							AlumniFunder exists to bolster a deeper relationship between students, alumni, and other alumni, by providing a platform to fund creative, innovative, and idealistically disruptive projects within the established university community. Whether it be a project to enhance the student experience on campus, raising capital to build a new science lab, or funding consumer development of a bleeding-edge robotics product, AlumniFunder seeks to facilitate access to crowdfunded capital for worthy endeavors.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							People registering on the AlumniFunder site are split into two groups, students and alumni who are looking to create a project (“Doers”) and alumni and the general public who are looking to browse and fund projects ("Alumni").
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							Doers are required to register their project with an appropriate .EDU email address, so as to verify the appropriate alumni network.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							Just so there’s no confusion, AlumniFunder is not formally affiliated with any university.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							Projects are put forth for a limited amount of time (30 days to 60 days), and have a well-defined scope. Funds raised are authorized by Stripe, then transferred to the project team upon completion of the project. If the project fails to hit the target, then the authorized amounts will not be charged to the funder's credit card.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							All projects will be subject to standards of decency and are subject to review by the AlumniFunder team.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							Successful projects will use the latest media technologies (we recommend Prezi and Vimeo) to visually explain the scope and the need for the project.
						</div>
					</li>
					<li class="grid-contain-with-border-bottom">
						<div>
							Our Core Values are as follows: We believe in building and supporting an ecosystem of innovation on University campuses across the world. We believe in supporting deeper alumni engagement with students, each other, and the alma mater. We believe that using technology to pool and direct alumni resources towards socially disruptive projects, small businesses, and startups will drive innovation and job creation. We believe that creating a more direct, transparent channel for alumni to connect financially with students and other alumni will drive engagement and enrich the university's mission in the world. We believe the power of good ideas can change the status quo for the better.
						</div>
					</li>
				</ol>
			</div>
		</div>
		<div class="info side">
			<p>
				"The Council for Aid to Education (CAE) earlier this year reported that donors gave more than $30 billion to U.S. colleges and universities last year – an 8 percent increase over 2010."<br />-Forbes
			</p>
		</div>
	</div>
</div>
<hr class="spacer short" />
<hr class="spacer short" />
