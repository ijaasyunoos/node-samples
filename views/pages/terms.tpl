<div class="title stretch">
	<h1>Terms &amp;<br />Conditions</h1>
</div>

<div class="content swing noborder">
	<div class="bd">
		<div class="swingleft">
			<div class="reg">
				<hr class="spacer short" />
				<h2>Terms of Use</h2>
				
				<hr />
				
				<p>Please read these Terms of Use (the "Agreement" or "Terms of Use") carefully
				before using the services offered by AlumniFunder, Inc. (“AlumniFunder” or
				the “Company”). This Agreement sets forth the legally binding terms and
				conditions for your use of the website at www.AlumniFunder.com, all other
				sites owned and operated by AlumniFunder that redirect to www.AlumniFunder
				.com, and all subdomains (collectively, the “Site”), and the service owned
				and operated by the Company (together with the Site, the “Service”). By
				using the Service in any manner, including, but not limited to, visiting or
				browsing the Site or contributing content, information, or other materials
				or services to the Site, you agree to be bound by this Agreement.</p>

				<h2>Summary of Service</h2>

				<hr />

				<p>AlumniFunder is a platform where certain users ("Project Creators") run
				campaigns to fund creative projects by offering rewards to raise money from
				other users (“Backers”). Through the Site, email, websites, and other media, 
				the Service makes accessible various content, including, but not limited
				to, videos, photographs, images, artwork, graphics, audio clips, comments, 
				data, text, software, scripts, projects, other material and information, 
				and associated trademarks and copyrightable works (collectively, 
				“Content”). Project Creators, Backers, and other visitors to and users of
				the Service (collectively, “Users”) may have the ability to contribute, add, 
				create, upload, submit, distribute, facilitate the distribution of, 
				collect, post, or otherwise make accessible ("Submit") Content. “User
				Submissions” means any Content Submitted by Users.</p>

				<h2>Acceptance of Terms</h2>

				<hr />

				<p>The Service is offered subject to acceptance of all of the terms and
				conditions contained in these Terms of Use, including the Privacy Policy
				available at <a href="{{#self.url}}privacy{{/self.url}}" title="alumnifunder privacy">http://www.alumnifunder.com/pages/privacy</a>, and all other
				operating rules, policies, and procedures that may be published on the Site
				by the Company, which are incorporated by reference. These Terms of Use
				apply to every user of the Service. In addition, some services offered
				through the Service may be subject to additional terms and conditions
				adopted by the Company. Your use of those services is subject to those
				additional terms and conditions, which are incorporated into these Terms of
				Use by this reference.</p>

				<p>The Company reserves the right, at its sole discretion, to modify or replace
				these Terms of Use by posting the updated terms on the Site. It is your
				responsibility to check the Terms of Use periodically for changes. Your
				continued use of the Service following the posting of any changes to the
				Terms of Use constitutes acceptance of those changes.</p>

				<p>The Company reserves the right to change, suspend, or discontinue the
				Service (including, but not limited to, the availability of any feature, 
				database, or Content) at any time for any reason. The Company may also
				impose limits on certain features and services or restrict your access to
				parts or all of the Service without notice or liability.</p>

				<p>The Service is available only to individuals who are at least 18 years old
				(and at least the legal age in your jurisdiction). You represent and warrant
				that if you are an individual, you are at least 18 years old and of legal
				age in your jurisdiction to form a binding contract, and that all
				registration information you submit is accurate and truthful. The Company
				reserves the right to ask for proof of age from you and your account may be
				suspended until satisfactory proof of age is provided. The Company may, in
				its sole discretion, refuse to offer the Service to any person or entity and
				change its eligibility criteria at any time. This provision is void where
				prohibited by law and the right to access the Service is revoked in those
				jurisdictions.</p>

				<h2>Rules and Conduct</h2>

				<hr />

				<p>As a condition of use, you promise not to use the Service for any purpose
				that is prohibited by the Terms of Use or law. The Service is provided only
				for your own personal, non-commercial use (except as allowed by the terms
				set forth in the section of these Terms of Use titled, "Projects: 
				Fundraising and Commerce"). You are responsible for all of your activity in
				connection with the Service. You shall not, and shall not permit any third
				party using your account to, take any action, or Submit Content, that:</p>

				<p>infringes any patent, trademark, trade secret, copyright, right of
				publicity, or other right of any other person or entity, or violates any law
				or contract;</p>

				<p>you know is false, misleading, or inaccurate;	</p>

				<p>is unlawful, threatening, abusive, harassing, defamatory, libelous, 
				deceptive, fraudulent, tortious, obscene, offensive, profane, or invasive
				of another's privacy;	 </p>

				<p>constitutes unsolicited or unauthorized advertising or promotional material
				or any junk mail, spam, or chain letters;	 </p>

				<p>contains software viruses or any other computer codes, files, or programs
				that are designed or intended to disrupt, damage, limit, or interfere with
				the proper function of any software, hardware, or telecommunications
				equipment or to damage or obtain unauthorized access to any system, data, 
				password, or other information of the Company or any third party;	 </p>

				<p>is made in breach of any legal duty owed to a third party, such as a
				contractual duty or a duty of confidence; </p>

				<p>or impersonates any person or entity, including any employee or
				representative of the Company.</p>

				<p>Additionally, you shall not: (i) take any action that imposes or may impose
				(as determined by the Company in its sole discretion) an unreasonable or
				disproportionately large load on the Company’s or its third-party providers
				’ infrastructure; (ii) interfere or attempt to interfere with the proper
				working of the Service or any activities conducted on the Service; (iii) 
				bypass any measures the Company may use to prevent or restrict access to the
				Service (or other accounts, computer systems, or networks connected to the
				Service); (iv) run Maillist, Listserv, or any form of auto-responder or
				"spam" on the Service; or (v) use manual or automated software, devices, or
				other processes to "crawl" or "spider" any page of the Site.</p>

				<p>You shall not directly or indirectly: (i) decipher, decompile, disassemble, 
				reverse engineer, or otherwise attempt to derive any source code or
				underlying ideas or algorithms of any part of the Service, except to the
				extent applicable laws specifically prohibit such restriction; (ii) modify, 
				translate, or otherwise create derivative works of any part of the
				Service; or (iii) copy, rent, lease, distribute, or otherwise transfer any
				of the rights that you receive hereunder. You shall abide by all applicable
				local, state, national, and international laws and regulations.</p>

				<p>Project Creators agree to not abuse other users' personal information. Abuse
				is defined as using personal information for any purpose other than those
				explicitly specified in the Project Creator’s Project, or is not related to
				fulfilling delivery of a product or service explicitly specified in the
				Project Creator’s Project.</p>

				<h2>Registration</h2>

				<hr />

				<p>You may view Content on the Site without registering, but as a condition of
				using certain aspects of the Service, you may be required to register with
				the Company and select a screen name ("User ID") and password. You shall
				provide accurate, complete, and updated registration information. Failure to
				do so shall constitute a breach of the Terms of Use, which may result in
				immediate termination of your account. You shall not use as a User ID, 
				domain name, or project name any name or term that (i) is the name of
				another person, with the intent to impersonate that person; (ii) is subject
				to any rights of another person, without appropriate authorization; or (iii) 
				is offensive, vulgar, or obscene. The Company reserves the right in its
				sole discretion to refuse registration of or cancel a User ID, domain name, 
				and project name. You are solely responsible for activity that occurs on
				your account and shall be responsible for maintaining the confidentiality of
				your password for the Site. You shall never use another User account without
				the other User’s express permission. You will immediately notify the Company
				in writing of any unauthorized use of your account, or other known account
				-related security breach.</p>

				<h2>Projects: Fundraising and Commerce</h2>

				<hr />

				<p>AlumniFunder is a platform where Project Creators run campaigns to fund
				creative projects by offering rewards to raise money from Backers. By
				creating a fundraising campaign on AlumniFunder, you as the Project Creator
				are offering the public the opportunity to enter into a contract with you
				. By backing a fundraising campaign on AlumniFunder, you as the Backer
				accept that offer and the contract between Backer and Project Creator is
				formed. AlumniFunder is not a party to that agreement between the Backer and
				Project Creator. All dealings are solely between Users.</p>

				<p>By backing or creating a fundraising campaign on AlumniFunder, you agree to
				be bound by this entire Agreement, including the following terms:</p>

				<p>Backers agree to provide their payment information at the time they pledge
				to a campaign. The payment will be collected at or after the campaign
				deadline and only if the amount of money pledged as of the deadline is at
				least equal to the fundraising goal. The amount Backers pledge is the amount
				they will be charged.</p>

				<p>Backers consent to AlumniFunder and its payments partners authorizing or
				reserving a charge on their payment card or other payment method for any
				amount up to the full pledge at any time between the pledge and collection
				of the funds.</p>

				<p>Backers agree to have sufficient funds or credit available at the campaign
				deadline to ensure that the pledge will be collectible.</p>

				<p>Backers may increase, decrease, or cancel their pledge at any time during
				the fundraising campaign, except that they may not cancel or reduce their
				pledge if the campaign is in its final 24 hours and the cancellation or
				reduction would drop the campaign below its goal.</p>

				<p>The Estimated Delivery Date listed on each reward is not a promise to
				fulfill by that date, but is merely an estimate of when the Project Creator
				hopes to fulfill by.</p>

				<p>Project Creators agree to make a good faith attempt to fulfill each reward
				by its Estimated Delivery Date.</p>

				<p>For all campaigns, AlumniFunder gives to the Project Creator each Backer’s
				User ID and pledge amount. For successful campaigns, AlumniFunder
				additionally gives to the Project Creator each Backer’s name and
				email.			 </p>

				<p>For some rewards, the Project Creator needs further information from
				Backers, such as a mailing address or t-shirt size, to enable the Project
				Creator to deliver the rewards. The Project Creator shall request the
				information directly from Backers at some point after the fundraising
				campaign is successful. To receive the reward, Backers agree to provide the
				requested information to the Project Creator within a reasonable amount of
				time.</p>

				<p>AlumniFunder does not offer refunds. A Project Creator is not required to
				grant a Backer’s request for a refund unless the Project Creator is unable
				or unwilling to fulfill the reward.</p>

				<p>Project Creators are required to fulfill all rewards of their successful
				fundraising campaigns or refund any Backer whose reward they do not or
				cannot fulfill.</p>

				<p>Project Creators may cancel or refund a Backer’s pledge at any time and for
				any reason, and if they do so, are not required to fulfill the reward.</p>

				<p>Because of occasional failures of payments from Backers, AlumniFunder cannot
				guarantee the receipt by Project Creators of the amount pledged minus fees.</p>

				<p>AlumniFunder and its payments partners will remove their fees before
				transmitting proceeds of a campaign. Fees may vary depending on region and
				other factors.</p>

				<p>AlumniFunder reserves the right to cancel a pledge at any time and for any
				reason.</p>

				<p>AlumniFunder reserves the right to reject, cancel, interrupt, remove, or
				suspend a campaign at any time and for any reason. AlumniFunder is not
				liable for any damages as a result of any of those actions. AlumniFunder’s
				policy is not to comment on the reasons for any of those
				actions.						 </p>

				<p>Project Creators should not take any action in reliance on having their
				project posted on the Site or having any of the money pledged until they
				have the ability to withdraw and spend the money. There may be a delay
				between the end of a successful fundraising campaign and access to the
				funds. </p>

				<p>AlumniFunder is not liable for any damages or loss incurred related to
				rewards or any other use of the Service. AlumniFunder is under no obligation
				to become involved in disputes between any Users, or between Users and any
				third party arising in connection with the use of the Service. This
				includes, but is not limited to, delivery of goods and services, and any
				other terms, conditions, warranties, or representations associated with
				campaigns on the Site. AlumniFunder does not oversee the performance or
				punctuality of projects. The Company does not endorse any User Submissions. 
				You release AlumniFunder, its officers, employees, agents, and successors
				in rights from claims, damages, and demands of every kind, known or unknown, 
				suspected or unsuspected, disclosed or undisclosed, arising out of or in
				any way related to such disputes and the Service.</p>

				<h2>Fees and Payments</h2>

				<hr />

				<p>Joining and using AlumniFunder is free. The Site does not charge a fee for its
				services, however, we may charge fees for certain services in the future. If 
				fees are implemented in the future, and you use a service that has a fee you 
				will have an opportunity to review and accept the fees that you will be 
				charged. Changes to fees will only be effective after we provide you with 
				notice by posting the changes on the Site.</p>

				<p>Funds pledged by Backers are collected by Stripe. AlumniFunder is not 
				responsible for the performance of Stripe.</p>

				<h2>Third-Party Sites</h2>

				<hr />

				<p>The Service may permit you to link to other websites or resources on the
				internet, and other websites or resources may contain links to the Site. 
				When you access third-party websites, you do so at your own risk. Those
				other websites are not under the Company's control, and you acknowledge that
				the Company is not liable for the content, functions, accuracy, legality, 
				appropriateness, or any other aspect of those other websites or resources. 
				The inclusion on another website of any link to the Site does not imply
				endorsement by or affiliation with the Company. You further acknowledge and
				agree that the Company shall not be liable for any damage related to the use
				of any content, goods, or services available through any third-party website
				or resource.</p>

				<h2>Content and License</h2>

				<hr />

				<p>You agree that the Service contains Content provided by the Company and its
				partners and Users and that the Content may be protected by copyrights, 
				trademarks, service marks, patents, trade secrets, or other rights and
				laws. You shall abide by and maintain all copyright and other legal notices, 
				information, and restrictions contained in any Content accessed through
				the Service.</p>

				<p>The Company grants to each User of the Service a worldwide, non-exclusive, 
				non-sublicensable and non-transferable license to use and reproduce the
				Content, solely for personal, non-commercial use. Use, reproduction, 
				modification, distribution, or storage of any Content for other than
				personal, non-commercial use is prohibited without prior written permission
				from the Company, or from the copyright holder. You shall not sell, license, 
				rent, or otherwise use or exploit any Content for commercial use or in any
				way that violates any third-party right.</p>

				<h2>Intellectual Property</h2>

				<hr />

				<p>By Submitting User Submissions on the Site or otherwise through the Service, 
				you agree to the following terms:</p>

				<ol>
					<li><p>The Company will not have any ownership rights over your User
					Submissions. However, the Company needs the following license to perform
					and market the Service on your behalf and on behalf of its other Users and
					itself. You grant to the Company the worldwide, non-exclusive, perpetual, 
					irrevocable, royalty-free, sublicensable, transferable right to (and to
					allow others acting on its behalf to) (i) use, edit, modify, prepare
					derivative works of, reproduce, host, display, stream, transmit, playback, 
					transcode, copy, feature, market, sell, distribute, and otherwise fully
					exploit your User Submissions and your trademarks, service marks, slogans, 
					logos, and similar proprietary rights (collectively, the "Trademarks") in
					connection with (a) the Service, (b) the Company’s (and its successors' and
					assigns’) businesses, (c) promoting, marketing, and redistributing part or
					all of the Site (and derivative works thereof) or the Service in any media
					formats and through any media channels (including, without limitation, 
					third-party websites); (ii) take whatever other action is required to
					perform and market the Service; (iii) allow its Users to stream, transmit, 
					playback, download, display, feature, distribute, collect, and otherwise
					use the User Submissions and Trademarks in connection with the Service; 
					and (iv) use and publish, and permit others to use and publish, the User
					Submissions, Trademarks, names, likenesses, and personal and biographical
					materials of you and the members of your group, in connection with the
					provision or marketing of the Service. The foregoing license grant to the
					Company does not affect your other ownership or license rights in your User
					Submissions, including the right to grant additional licenses to your User
					Submissions.</p></li>
					<li><p>You are publishing your User Submission, and you may be identified
					publicly by your name or User ID in association with your User Submission.</p></li>
					<li><p>You grant to each User a non-exclusive license to access your User
					Submissions through the Service, and to use, edit, modify, reproduce, 
					distribute, prepare derivative works of, display and perform such User
					Submissions solely for personal, non-commercial use.</p></li>
					<li><p>You further agree that your User Submissions will not contain third
					-party copyrighted material, or material that is subject to other third
					-party proprietary rights, unless you have permission from the rightful
					owner of the material or you are otherwise legally entitled to post the
					material and to grant AlumniFunder all of the license rights granted
					herein. </p></li>
					<li><p>You will pay all royalties and other amounts owed to any person or
					entity based on your Submitting User Submissions to the Service or the
					Company’s publishing or hosting of the User Submissions as contemplated by
					these Terms of Use.</p></li>
					<li><p>The use or other exploitation of User Submissions by the Company and
					Users as contemplated by this Agreement will not infringe or violate the
					rights of any third party, including without limitation any privacy rights,
					publicity rights, copyrights, contract rights, or any other intellectual
					property or proprietary rights.</p></li>
					<li><p>The Company shall have the right to delete, edit, modify, reformat, 
					excerpt, or translate any of your User Submissions.</p></li>
					<li><p>All information publicly posted or privately transmitted through the
					Site is the sole responsibility of the person from which that content
					originated.</p></li>
					<li><p>The Company will not be liable for any errors or omissions in any
					Content.</p></li>
					<li><p>The Company cannot guarantee the identity of any other Users with whom
					you may interact while using the Service.</p></li>
					<li><p>All Content you access through the Service is at your own risk and you
					will be solely responsible for any resulting damage or loss to any party.</p></li>
					<li><p>In accordance with the Digital Millennium Copyright Act, AlumniFunder
					has adopted a policy of, in appropriate circumstances, terminating User
					accounts that are repeat infringers of the intellectual property rights of
					others. AlumniFunder also may terminate User accounts even based on a
					single infringement.</p></li>
				</ol>

				<h2>Copyright Notifications</h2>

				<hr />

				<p>AlumniFunder will remove infringing materials in accordance with the DMCA if
				properly notified that Content infringes copyright. If you believe that your
				work has been copied in a way that constitutes copyright infringement, 
				please notify AlumniFunder's Copyright Agent in writing. Your notice must
				contain the following information (please confirm these requirements with
				your legal counsel, or see the U.S. Copyright Act, 17 U.S.C. §512(c)(3), for
				more information):</p>

				<ol>
					<li>an electronic or physical signature of the person authorized to act on
					behalf of the owner of the copyright interest;</li>
					<li>a description of the copyrighted work that you claim has been infringed;</li>
					<li>a description of where the material that you claim is infringing is
					located on the Site, sufficient for AlumniFunder to locate the material;</li>
					<li>your address, telephone number, and email address;</li>
					<li>a statement by you that you have a good faith belief that the disputed
					use is not authorized by the copyright owner, its agent, or the law; and</li>
					<li>a statement by you that the information in your notice is accurate and,
					under penalty of perjury, that you are the copyright owner or authorized
					to act on the copyright owner's behalf.</li>
				</ol>

				<p>If you believe that your work has been removed or disabled by mistake or
				misidentification, please notify AlumniFunder’s Copyright Agent in writing. 
				Your counter-notice must contain the following information (please confirm
				these requirements with your legal counsel or see the U.S. Copyright Act, 17
				U.S.C. §512(g)(3), for more information): </p>

				<ol>
					<li>a physical or electronic signature of the user of the Services;</li>
					<li>identification of the material that has been removed or to which access
					has been disabled and the location at which the material appeared before it
					was removed or access to it was disabled; a statement made under penalty of
					perjury that the subscriber has a good faith belief that the material was
					removed or disabled as a result of mistake or misidentification of the
					material; and the subscriber's name, address, telephone number, and a
					statement that the subscriber consents to the jurisdiction of the Federal
					District Court for the judicial district in which the address is located, 
					or if the subscriber's address is outside of the United States, for any
					judicial district in which the service provider may be found, and that the
					user will accept service of process from the person who provided
					notification under subscriber (c)(1)(C) or an agent of such person.</li>
					<li>Under the Copyright Act, any person who knowingly materially
					misrepresents that material is infringing or was removed or disabled by
					mistake or misidentification may be subject to liability.</li>
				</ol>

				<p>If you fail to comply with these notice requirements, your notification or
				counter-notification may not be valid.</p>

				<p>Our designated copyright agent for notice of alleged copyright infringement is:</p>

				<p>AlumniFunder, Inc. 
				Attn: Robyn Gould
				1333 2nd Street
				Suite 400
				Santa Monica, CA 90401
				Email: copyright@AlumniFunder.com </p>

				<h2>Termination</h2>

				<hr />

				<p>The Company may terminate your access to the Service, without cause or
				notice, which may result in the forfeiture and destruction of all
				information associated with your account. If you wish to terminate your
				account, you may do so by following the instructions on the Site. Any fees
				paid to the Company are non-refundable. All provisions of the Terms of Use
				that by their nature should survive termination shall survive termination, 
				including, without limitation, ownership provisions, warranty disclaimers, 
				indemnity, and limitations of liability.</p>

				<h2>Warranty Disclaimer</h2>

				<hr />

				<p>The Company has no special relationship with or fiduciary duty to you. You
				acknowledge that the Company has no duty to take any action regarding any of
				the following: which Users gain access to the Site; what Content Users
				access through the Site; what effects the Content may have on Users; how
				Users may interpret or use the Content; or what actions Users may take as a
				result of having been exposed to the Content. The Company cannot guarantee
				the authenticity of any data or information that Users provide about
				themselves or their campaigns and projects. You release the Company from all
				liability for your having acquired or not acquired Content through the Site.
				The Site may contain, or direct you to websites containing, information
				that some people may find offensive or inappropriate. The Company makes no
				representations concerning any Content on the Site, and the Company is not
				liable for the accuracy, copyright compliance, legality, or decency of
				material contained on the Service.</p>

				<p>The Company does not guarantee that any Content will be made available
				through the Service. The Company has no obligation to monitor the Service or
				Content. The Company reserves the right to, at any time, for any reason, and
				without notice: (i) cancel, reject, interrupt, remove, or suspend a campaign
				or project; (ii) remove, edit, or modify any Content, including, but not
				limited to, any User Submission; and (iii) remove or block any User or User
				Submission. AlumniFunder reserves the right not to comment on the reasons
				for any of these actions.</p>

				<p>The Service is provided “as is” and “as available” and is without warranty
				of any kind, express or implied, including, but not limited to, the implied
				warranties of title, non-infringement, merchantability, and fitness for a
				particular purpose, and any warranties implied by any course of performance
				or usage of trade, all of which are expressly disclaimed. The Company, and
				its directors, employees, agents, suppliers, partners, and content providers
				do not warrant that: (a) the Service will be secure or available at any
				particular time or location; (b) any defects or errors will be corrected;
				(c) any content or software available at or through the Service is free of
				viruses or other harmful components; or (d) the results of using the Service
				will meet your requirements. Your use of the Service is solely at your own
				risk. Some states or countries do not allow limitations on how long an
				implied warranty lasts, so the above limitations may not apply to you.</p>

				<p>The Company makes no guaranty of confidentiality or privacy of any
				communication or information transmitted on the Site or any website linked
				to the Site. The Company will not be liable for the privacy of email
				addresses, registration and identification information, disk space,
				communications, confidential or trade-secret information, or any other
				Content stored on the Company’s equipment, transmitted over networks
				accessed by the Site, or otherwise connected with your use of the
				Service.		</p>

				<p>Electronic Communications Privacy Act Notice (18 USC §2701-2711): THE
				COMPANY MAKES NO GUARANTY OF CONFIDENTIALITY OR PRIVACY OF ANY COMMUNICATION
				OR INFORMATION TRANSMITTED ON THE SITE OR ANY WEBSITE LINKED TO THE SITE.
				The Company will not be liable for the privacy of email addresses,
				registration and identification information, disk space, communications,
				confidential or trade-secret information, or any other Content stored on
				the Company’s equipment, transmitted over networks accessed by the Site, or
				otherwise connected with your use of the Service.</p>

				<h2>Indemnification</h2>

				<hr />

				<p>You shall defend, indemnify, and hold harmless the Company, its affiliates,
				and each of its and its affiliates’ employees, contractors, directors,
				suppliers, and representatives from all liabilities, claims, and expenses,
				including reasonable attorneys' fees and other legal costs, that arise
				from or relate to your use or misuse of, or access to, the Service and
				Content, or otherwise from your User Submissions, violation of the Terms of
				Use, or infringement by you, or any third party using your account, of any
				intellectual property or other right of any person or entity. The Company
				reserves the right to assume the exclusive defense and control of any matter
				otherwise subject to indemnification by you, in which event you will assist
				and cooperate with the Company in asserting any available defenses.</p>

				<h2>Limitation of Liability</h2>

				<hr />

				<p>In no event shall the Company, nor its directors, employees, agents,
				partners, suppliers, or content providers, be liable under contract, tort,
				strict liability, negligence, or any other legal or equitable theory with
				respect to the service (i) for any lost profits, data loss, cost of
				procurement of substitute goods or services, or special, indirect,
				incidental, punitive, or consequential damages of any kind whatsoever,
				substitute goods or services (however arising), (ii) for any bugs,
				viruses, trojan horses, or the like (regardless of the source of
				origination), or (iii) for any direct damages in excess of (in the
				aggregate) one hundred U.S. dollars ($100.00). some states or countries do
				not allow the exclusion or limitation of incidental or consequential
				damages, so the above limitations and exclusions may not apply to you.</p>

				<h2>International</h2>

				<hr />

				<p>Accessing the Service is prohibited from territories where the Content is
				illegal. If you access the Service from other locations, you do so at your
				own initiative and are responsible for compliance with local laws.</p>

				<h2>Electronic Delivery, Notice Policy, and Your Consent</h2>

				<hr />

				<p>By using the Services, you consent to receive from AlumniFunder all
				communications including notices, agreements, legally required disclosures,
				or other information in connection with the Services (collectively,
				"Contract Notices") electronically. AlumniFunder may provide the
				electronic Contract Notices by posting them on the Site. If you desire to
				withdraw your consent to receive Contract Notices electronically, you must
				discontinue your use of the Services.</p>

				<h2>Governing Law</h2>

				<hr />

				<p>These Terms of Service (and any further rules, policies, or guidelines
				incorporated by reference) shall be governed by and construed in accordance
				with the laws of the State of California and the United States, without
				giving effect to any principles of conflicts of law, and without application
				of the Uniform Computer Information Transaction Act or the United Nations
				Convention of Controls for International Sale of Goods. You agree that the
				Company and its Services are deemed a passive website that does not give
				rise to personal jurisdiction over AlumniFunder or its parents,
				subsidiaries, affiliates, successors, assigns, employees, agents,
				directors, officers or shareholders, either specific or general, in any
				jurisdiction other than the State of California. You agree that any action
				at law or in equity arising out of or relating to these terms, or your use
				or non-use of the Services, shall be filed only in the state or federal
				courts located in Los Angeles County in the State of California and you
				hereby consent and submit to the personal jurisdiction of such courts for
				the purposes of litigating any such action. You hereby irrevocably waive any
				right you may have to trial by jury in any dispute, action, or
				proceeding.		 </p>

				<h2>Integration and Severability</h2>

				<hr />

				<p>These Terms of Use and other referenced material are the entire agreement
				between you and the Company with respect to the Service, and supersede all
				prior or contemporaneous communications and proposals (whether oral, written
				or electronic) between you and the Company with respect to the Service and
				govern the future relationship. If any provision of the Terms of Use is
				found to be unenforceable or invalid, that provision will be limited or
				eliminated to the minimum extent necessary so that the Terms of Use will
				otherwise remain in full force and effect and enforceable. The failure of
				either party to exercise in any respect any right provided for herein shall
				not be deemed a waiver of any further rights hereunder.</p>

				<h2>Miscellaneous</h2>

				<hr />

				<p>The Company shall not be liable for any failure to perform its obligations
				hereunder where the failure results from any cause beyond the Company’s
				reasonable control, including, without limitation, mechanical, electronic, 
				or communications failure or degradation. The Terms of Use are personal to
				you, and are not assignable, transferable, or sublicensable by you except
				with the Company's prior written consent. The Company may assign, transfer, 
				or delegate any of its rights and obligations hereunder without consent. 
				No agency, partnership, joint venture, or employment relationship is
				created as a result of the Terms of Use and neither party has any authority
				of any kind to bind the other in any respect. In any action or proceeding to
				enforce rights under the Terms of Use, the prevailing party will be entitled
				to recover costs and attorneys' fees. All notices under the Terms of Use
				will be in writing and will be deemed to have been duly given when received, 
				if personally delivered or sent by certified or registered mail, return
				receipt requested; when receipt is electronically confirmed, if transmitted
				by facsimile or e-mail; or the day after it is sent, if sent for next day
				delivery by recognized overnight delivery service.</p>

				<p>Updated: January 2013</p>
			</div>
		</div>
	</div>
</div>