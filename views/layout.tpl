<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	{{#project}}
		<meta property="og:url" content="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{{projectUrl}}}" />
		<meta property="og:title" content="{{title}}" />
		<meta property="og:description" content="{{description}}" />
		<meta property="og:image" content="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{#thumbnail.url}}200,200,0{{/thumbnail.url}}" />
		<meta property="og:site_name" content="AlumniFunder" />
	{{/project}}
	<title>AlumniFunder</title>
	<!-- CSS -->
	<!--<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/screen.css" media="screen" />-->
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/extra.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/main.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.core.css" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.theme.css" />
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}jquery-ui/themes/base/jquery.ui.datepicker.css" />
	<link rel="image_src" type="image/jpeg" href="{{{siteRoot}}}images/logo_fb.png" />
	<!-- JAVASCRIPT -->
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.position.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}jquery-ui/ui/jquery.ui.slider.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.timepicker.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.svg/jquery.svg.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/jquery.cycle.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/pixastic/pixastic.core.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/pixastic/pixastic.jquery.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/main.js"></script>
	<script type="text/javascript" src="{{{siteRoot}}}scripts/facebook.js"></script>
	{{*
	<script type="text/javascript" src="{{{siteRoot}}}scripts/linkedin.js"></script>
	*}}
	<!--[if lt IE 9]>
		<script src="{{{siteRoot}}}scripts/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35078774-1']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</head>
<body class="{{bodyClass}} controller-{{paraglide.controller}} action-{{paraglide.action}}">
	<script type="text/javascript">
		var baseUrl = '{{{siteRoot}}}';
		var grid_color = '{{sectionColor}}';
	</script>

	{{^hideNav}}
		<div class="header">
			{{#user.isAdmin}}
				<div class="admin bd">
					<div class="wrap swing admin">
						<div id="admin-header">
							<ul>
								<li>
									<strong>Admin</strong>
								</li>
								<li>
									<a href="{{#paraglide.url}}projects,unapproved{{/paraglide.url}}"><span>Unapproved Projects Queue</span></a>
								</li>
								<li>
									<a href="{{#paraglide.url}}universities{{/paraglide.url}}"><span>Universities</span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			{{/user.isAdmin}}
			<div class="wrap swing">
				<div class="bd">
					<div class="navigation swingleft">
						<ul>
							<li class="{{#isPage}}main,index,on{{/isPage}}">
								<a href="{{paraglide.url}}" class="home">
									<span>Home</span>
								</a>
							</li>
							<li class="{{#isPage}}pages,how-it-works,on{{/isPage}}">
								<a href="{{#paraglide.url}}pages,how-it-works{{/paraglide.url}}" class="how">
									<span>How It works</span>
								</a>
							</li>
							<li class="{{#isPage}}pages,about,on{{/isPage}}">
								<a href="{{#paraglide.url}}pages,about{{/paraglide.url}}" class="about">
									<span>About us</span>
								</a>
							</li>
							<li class="{{#isPage}}projects,index,on{{/isPage}}">
								<a href="{{#paraglide.url}}projects{{/paraglide.url}}" class="browse">
									<span>Browse</span>
								</a>
							</li>
							{{#user}}
								<li class="{{#isPage}}projects,create,on{{/isPage}}">
									<a href="{{#paraglide.url}}projects,create{{/paraglide.url}}" class="create">
										<span>Create</span>
									</a>
								</li>
								<li class="{{#isPage}}account,projects,on{{/isPage}}">
									<a href="{{#paraglide.url}}account,projects{{/paraglide.url}}" class="projects">
										<span>Your Projects</span>
									</a>
								</li>
								<li class="{{#isPage}}account,profile,on{{/isPage}}">
									<a href="{{#paraglide.url}}account,profile{{/paraglide.url}}" class="profile">
										<span>Your Profile</span>
									</a>
								</li>
								<li class="{{#isPage}}account,edit,on{{/isPage}}">
									<a href="{{#paraglide.url}}account,edit{{/paraglide.url}}" class="profile">
										<span>Edit Profile</span>
									</a>
								</li>
								<li class="{{#isPage}}account,logout,on{{/isPage}}">
									<a href="{{#paraglide.url}}account,logout{{/paraglide.url}}" class="logout">
										<span>Logout</span>
									</a>
								</li>
							{{/user}}
							{{^user}}
								<li class="{{#isPage}}account,register,on{{/isPage}}">
									{{#isEquity}}
										<a href="{{paraglide.url}}" class="register">
									{{/isEquity}}
									{{^isEquity}}
										<a href="{{#paraglide.url}}account,register{{/paraglide.url}}" class="register">
									{{/isEquity}}
										<span>Register</span>
									</a>
								</li>
								<li class="{{#isPage}}account,login,on{{/isPage}}">
									<a href="{{#paraglide.url}}account,login{{/paraglide.url}}" class="login">
										<span>Login</span>
									</a>
								</li>
							{{/user}}
						</ul>
					</div>
					<div class="social">
						<ul>
							<li>
								<a class="foundicon small" href="https://twitter.com/AlumniFunder" target="_blank">
									<span class="icon foundicon-twitter"></span>
								</a>
							</li>
							<li>
								<a class="foundicon small" href="http://www.linkedin.com/company/2743250" target="_blank">
									<span class="icon foundicon-linkedin"></span>
								</a>
							</li>
							<li>
								<a class="foundicon small" href="https://www.facebook.com/AlumniFunder" target="_blank">
									<span class="icon foundicon-facebook"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	{{/hideNav}}
	<div class="main">
		<div class="content wrap">
			<div class="logo">
				<a href="{{paraglide.url}}">
					{{#whitelabelUniversity}}
						{{whitelabelUniversity.name}} Crowdfunding
						<br />Powered by AlumniFunder
					{{/whitelabelUniversity}}
					{{^whitelabelUniversity}}
						<img src="{{{siteRoot}}}images/logo.png" alt="alumni funder" />
					{{/whitelabelUniversity}}
				</a>
				{{#user}}
					{{#project.id}}
						{{#university}}
							<div class="project-affiliation">
								<a href="{{#self.url}}universities,{{university.slug}}{{/self.url}}">{{university.name}} project</a>
							</div>
						{{/university}}
					{{/project.id}}
				{{/user}}
				{{^user}}
					{{^isEquity}}
						{{^isLoginProcess}}
							<div class="quick-login">
								<a href="#facebook" class="foundicon normal social-icon" onclick="facebookConnectOnLogin('{{{redirect}}}')">
									<span class="icon foundicon-facebook"></span>
									<span class="label">Login with Facebook</span>
								</a>
							</div>
							<div class="quick-login quick-login-secondary">
								<a class="foundicon normal social-icon" href="{{#paraglide.url}}account,login,,connect=linkedin{{/paraglide.url}}">
									<span class="icon foundicon-linkedin"></span>
									<span class="label">Login with LinkedIn</span>
								</a>
							</div>
						{{/isLoginProcess}}
					{{/isEquity}}
				{{/user}}
			</div>
			{{{PAGE_CONTENT}}}
		</div>
	</div>
	{{^hideNav}}
		<!-- footer -->
		<div class="footerb">
			<div class="wrap footer swing border">
				<div class="bd">
					<div class="navigation swingleft">
						<ul>
							<li>
								<a href="{{#paraglide.url}}pages,terms{{/paraglide.url}}">
									terms and conditions
								</a> |
							</li>
							<li>
								<a href="{{#paraglide.url}}pages,privacy{{/paraglide.url}}">
									privacy policy
								</a> |
							</li>
							<li>
								<a href="{{#paraglide.url}}contact{{/paraglide.url}}">
									contact us
								</a>
							</li>
						</ul>
					</div>
					<div class="copyright">&copy; AlumniFunder {{currentYear}}</div>
				</div>
			</div>
		</div>
		<div id="fb-root"></div>
		{{/hideNav}}
	</body>
</html>