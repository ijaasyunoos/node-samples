<div class="title swing border grid-contain-without-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<h1>Invest in a<br /> Younger You...</h1>
		</div>
		<div class="register impulse">
			{{^user}}
				<a class="arrow-link arrow-link-gray" href="{{#paraglide.url}}account,register{{/paraglide.url}}">
					<span>register now</span><i></i>
				</a>
			{{/user}}
			{{#user}}
				<a class="arrow-link arrow-link-gray" href="{{#paraglide.url}}account,profile{{/paraglide.url}}">
					<span>Your Profile</span><i></i>
				</a>
			{{/user}}
		</div>
	</div>
</div>

<div class="content swing border grid-contain-without-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<hr class="spacer medium" />
			<a href="#prezi-player" class="prezi-image"><img src="{{{siteRoot}}}images/home/prezi.jpg" width="625" height="398" /></a>
			<div id="prezi-player" class="prezi-player" style="display:none">
				<object id="prezi_sj-3a5exwgzn" name="prezi_sj-3a5exwgzn" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="627" height="400">
					<param name="movie" value="http://prezi.com/bin/preziloader.swf"/>
					<param name="allowfullscreen" value="true"/>
					<param name="allowFullScreenInteractive" value="true"/>
					<param name="allowscriptaccess" value="always"/>
					<param name="wmode" value="direct"/>
					<param name="bgcolor" value="#ffffff"/>
					<param name="flashvars" value="prezi_id=sj-3a5exwgzn&amp;lock_to_path=0&amp;color=ffffff&amp;autoplay=autostart&amp;autohide_ctrls=0"/>
					<embed id="preziEmbed_sj-3a5exwgzn" name="preziEmbed_sj-3a5exwgzn" src="http://prezi.com/bin/preziloader.swf" type="application/x-shockwave-flash" allowfullscreen="true" allowFullScreenInteractive="true" allowscriptaccess="always" width="627" height="400" bgcolor="#ffffff" flashvars="prezi_id=sj-3a5exwgzn&amp;lock_to_path=0&amp;color=ffffff&amp;autoplay=autostart&amp;autohide_ctrls=0">
					</embed>
				</object>
			</div>
			<hr class="spacer medium" />
		</div>
		
		<div class="side">
			<hr class="spacer medium" />
			<a class="arrow-link" href="{{#paraglide.url}}projects,universities,georgetown{{/paraglide.url}}">
				<span>See projects from Georgetown beta</span><i></i>
			</a>
			<a class="arrow-link" href="{{#paraglide.url}}projects,universities,princeton{{/paraglide.url}}">
				<span>See projects from Princeton beta</span><i></i>
			</a>
			<div class="box-light">
				Enter your project<br> into one of our<br> competitions and<br> win <strong>$1000</strong>
			</div>
			<a class="arrow-link arrow-link-small arrow-link-gray" href="{{#paraglide.url}}contests,georgetown{{/paraglide.url}}">
				<span>GEORGETOWN COMPETITION</span><i></i>
			</a>
			<a class="arrow-link arrow-link-small arrow-link-gray" href="{{#paraglide.url}}contests,princeton{{/paraglide.url}}">
				<span>PRINCETON COMPETITION</span><i></i>
			</a>
			<a class="arrow-link arrow-link-small arrow-link-gray" href="{{#paraglide.url}}contests,berkeley{{/paraglide.url}}">
				<span>BERKELEY COMPETITION</span><i></i>
			</a>
			<hr class="spacer medium" />
		</div>
	</div>
</div>
<div class="content swing border solid potential grid-contain-without-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<hr class="spacer medium" />
			<h3>Top Five Entrepreneurial<br> Colleges In 2012</h3>
			<hr class="spacer short" />
				<div class="chart-v">
					<div class="bar">
						<div class="num" style="margin-top:0;">1</div>
						<div class="fill" style="height:85px;"></div>
						<div class="label">Stanford University</div>
					</div>
					<div class="bar">
						<div class="num" style="margin-top:17px;">2</div>
						<div class="fill" style="height:68px;"></div>
						<div class="label">Massachusetts Institute of Technology</div>
					</div>
					<div class="bar">
						<div class="num" style="margin-top:34px;">3</div>
						<div class="fill" style="height:51px;"></div>
						<div class="label">Harvard University</div>
					</div>
					<div class="bar">
						<div class="num" style="margin-top:51px;">4</div>
						<div class="fill" style="height:34px;"></div>
						<div class="label">California Institute of Technology</div>
					</div>
					<div class="bar">
						<div class="num" style="margin-top:68px;">5</div>
						<div class="fill" style="height:17px;"></div>
						<div class="label">University of California, Berkeley</div>
					</div>
				</div>
			<div class="stat">
				FORBES &amp; LINKEDIN DATA
			</div>
			<hr class="spacer medium" />
		</div>
		<div class="side">
			<hr class="spacer short" />
			<div class="pie">$2.24BN</div>
			<div class="pie-stats">
				<span>$30bn</span>
				<p>Private contributions to America's higher education institutions increased 8.2percent to $30.30 billion in 2011... The top 20 institutions raised $8.24 billion.</p>
			</div>
		</div>
		<hr class="spacer short" />
	</div>
</div>
<div class="content swing border solid potential grid-contain-without-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<div class="subtitle">
				<h2>The potential of crowd funding</h2>
			</div>
			<div class="columns-2">
				<p>The notion of leveraging technology to source funding for a wide range of artistic, altruistic, commercial, and disruptive projects across a wide spectrum of people is in its nascent stages. The coming years will see crowdfunding expand to supplant many traditional avenues to institutional and individual sources of capital. Many naysayers see a potential for fraud, doubting the sophistication of the crowd to aggregate small sources of capital toward a singular, worthy endeavor.</p>
				<p style="padding-left:2%">AlumniFunder is crowdfunding, tailored to work within and to build upon the existing, traditional alumni relationships, and to provide visibility to innovative projects and companies across those relationships and to the public, so everyone can participate in funding disruption and supporting entrepreneurship at the venerable institutions teaching innovation.</p>
			</div>
		</div>
		<div class="side">
			<div class="subtitle">
				<h2>Why AlumniFunder?</h2>
			</div>
			<p>AlumniFunder verifies that each project creator has attended or is attending one of the launch universities, so as to create transparency in crowdfunding. We will be adding many more universities as the year unfolds, as well as creative functionality and resources that highlight the best aspects of crowdfunding as a service, both for “Doers” and “Funders” alike.</p>
		</div>
		<hr class="spacer short" />
	</div>
</div>

<div class="content swing border solid grid-contain-with-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<div class="reg top">
				<hr class="spacer medium" />
				<h3>Top Contributions<br /> By University 2011</h3>
				<hr class="spacer short" />
				<div class="chart">
					<ul>
						<li>
							<b class="name">Stanford</b>
							<div class="bar" style="width:98%">$709m</div>
						</li>
						<li>
							<b class="name">Harvard</b>
							<div class="bar" style="width:88%;">$639m</div>
						</li>
						<li>
							<b class="name">Yale</b>
							<div class="bar" style="width:78%;">$580m</div>
						</li>
						<li>
							<b class="name">MIT</b>
							<div class="bar" style="width:68%;">$534m</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="side">
			<hr class="spacer medium" />
			<div class="subscribe">
				<p>Sign up to our<br> email list to keep<br> up to date with<br> the latest projects</p>
				<form method="post" action="http://alumnifunder.us5.list-manage.com/subscribe/post?u=fa009dd855964ce476c6beea6&amp;id=5c491d5b38">
					<div class="field">
						<input type="text" name="EMAIL" placeholder="ENTER YOUR EMAIL ADDRESS" />
					</div>
					<input type="submit" class="button" value="SIGN UP" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="content swing border dotted grid-contain-with-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<div class="reg stat">
				COUNCIL FOR AID TO EDUCATION (CAE)
			</div>
		</div>
	</div>
</div>
