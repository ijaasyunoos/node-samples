<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>Invest in a<br /> Younger You...</h1>
		</div>
		<div class="register impulse">
			{{^user}}
				<a class="arrow-link arrow-link-gray" href="{{#paraglide.url}}account,register{{/paraglide.url}}">
					<span>register now</span><i></i>
				</a>
			{{/user}}
			{{#user}}
				<a class="arrow-link arrow-link-gray" href="{{#paraglide.url}}account,profile{{/paraglide.url}}">
					<span>Your Profile</span><i></i>
				</a>
			{{/user}}
		</div>
	</div>
</div>

<div class="content swing border solid potential">
	<div class="bd">
		<div class="swingleft">
			<div class="subtitle">
				<h2>The potential of crowd funding</h2>
			</div>
			<div class="columns-2">
				<p>The notion of leveraging technology to source funding for a wide range of artistic, altruistic, commercial, and disruptive projects across a wide spectrum of people is in its nascent stages. The coming years will see crowdfunding expand to supplant many traditional avenues to institutional and individual sources of capital. Many naysayers see a potential for fraud, doubting the sophistication of the crowd to aggregate small sources of capital toward a singular, worthy endeavor.</p>
				<p style="padding-left:2%">AlumniFunder is crowdfunding, tailored to work within and to build upon the existing, traditional alumni relationships, and to provide visibility to innovative projects and companies across those relationships and to the public, so everyone can participate in funding disruption and supporting entrepreneurship at the venerable institutions teaching innovation.</p>
			</div>
		</div>
		<div class="side">
			<div class="subtitle">
				<h2>Why AlumniFunder?</h2>
			</div>
			<p>AlumniFunder verifies that each project creator has attended or is attending one of the launch universities, so as to create transparency in crowdfunding. We will be adding many more universities as the year unfolds, as well as creative functionality and resources that highlight the best aspects of crowdfunding as a service, both for “Doers” and “Funders” alike.</p>
		</div>
		<hr class="spacer short" />
	</div>
</div>

<div class="content swing border solid">
	<div class="bd">
		<div class="swingleft">
			<div class="reg top">
				<hr class="spacer medium" />
			</div>
		</div>
		<div class="side">
			<hr class="spacer medium" />
			<div class="subscribe">
				<p>Sign up to our<br> email list to keep<br> up to date with<br> the latest projects</p>
				<form method="post" action="http://alumnifunder.us5.list-manage.com/subscribe/post?u=fa009dd855964ce476c6beea6&amp;id=5c491d5b38">
					<div class="field">
						<input type="text" name="EMAIL" placeholder="ENTER YOUR EMAIL ADDRESS" />
					</div>
					<input type="submit" class="button" value="SIGN UP" />
				</form>
			</div>
		</div>
	</div>
</div>
