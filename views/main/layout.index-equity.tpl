<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>AlumniFunder</title>
	<link rel="stylesheet" type="text/css" href="{{{siteRoot}}}styles/equity-landing.css" media="screen" />
</head>
<body>
{{{PAGE_CONTENT}}}
</body>
</html>