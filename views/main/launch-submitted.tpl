<div class="title border">
	<h1>Thanks for Registering<br /> for the AlumniFunder<br /> Experiment</h1>
</div>
<div class="content wide border">
	<div class="reg top thanks">
		<div class="social">
			<ul>
				<li>
					<a href="https://twitter.com/AlumniFunder" target="_blank">
						<span class="foundicon-twitter"></span>
					</a>
				</li>
				<li>
					<a href="http://www.linkedin.com/company/2743250" target="_blank">
						<span class="foundicon-linkedin"></span>
					</a>
				</li>
				<li>
					<a href="https://www.facebook.com/AlumniFunder" target="_blank">
						<span class="foundicon-facebook"></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>