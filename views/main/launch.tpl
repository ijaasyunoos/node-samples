<div class="title border">
	<h1>Invest in a<br /> Younger You...</h1>
</div>

<div class="content wide border">

	<div class="reg top">
		<div class="subtitle">
			<h2>Our public beta is coming soon. Please register your interest below.</h2>
		</div>
		<div class="form">
			<form method="post">
				{{#newUser.hasErrors}}
					{{#newUser.errorsArray}}
					{{> common/form/error}}
					{{/newUser.errorsArray}}
						<br />
				{{/newUser.hasErrors}}
				<div class="inputs wide">
					<hr class="spacer short" />
					<div class="side">
						<label class="big">
							<span class="label primary"><b>Your Name</b></span>
							<input type="text" class="showAll toggle-label" name="name" value="{{self.post.name}}" autocomplete="off" maxlength="255" />
						</label>
						<hr class="spacer short" />
						<label class="big">
							<span class="label primary"><b>Your Email Address</b></span>
							<input type="text" class="showAll toggle-label" name="email" value="{{self.post.email}}" autocomplete="off" maxlength="255" />
						</label>
					</div>
					<div class="side">
						<label class="submit">
							<input type="submit" class="button" value="REGISTER" />
						</label>
					</div>
				</div>
			</form>

			<div class="social">
				<ul>
					<li>
						<a href="https://twitter.com/AlumniFunder" target="_blank">
							<span class="foundicon-twitter"></span>
						</a>
					</li>
					<li>
						<a href="http://www.linkedin.com/company/2743250" target="_blank">
							<span class="foundicon-linkedin"></span>
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/AlumniFunder" target="_blank">
							<span class="foundicon-facebook"></span>
						</a>
					</li>
				</ul>
			</div>

		</div>
	</div>
</div>