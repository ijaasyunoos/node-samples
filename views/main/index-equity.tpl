<div class="bg">
	<div class="landing-box">
		<img src="{{{siteRoot}}}images/equity/AF_blue_logo.png" alt="Logo" />
		<a href="{{{equityBaseUrl}}}{{#paraglide.url}}account,register-equity-investor{{/paraglide.url}}">
			<span>Investors</span>
			<i class="arrow-link"></i>
		</a>
		<a href="{{{equityBaseUrl}}}{{#paraglide.url}}projects/entrepreneurs{{/paraglide.url}}">
			<span>Entrepreneurs</span>
			<i class="arrow-link"></i>
		</a>
		<a href="{{{nonEquityBaseUrl}}}{{#paraglide.url}}crowdfunders{{/paraglide.url}}">
			<span>Crowdfunders</span>
			<i class="arrow-link"></i>
		</a>
	</div>
</div>
