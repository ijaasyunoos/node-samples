<div class="chart">
	<div class="progress">
		<span class="percent">{{project.amountRaisedPercentage}}%</span>
		<div class="fill" style="height: {{project.amountRaisedPercentage}}%"></div>
	</div>
</div>
<div class="progress">
	<ul>
		<li>
			<span class="title">Target</span>
			<span class="info">${{#numberFormat}}{{project.amountGoal}}{{/numberFormat}}</span>
		</li>
		<li>
			<span class="title">Pledged</span>
			<span class="info">${{#numberFormat}}{{project.amountRaised}}{{/numberFormat}}</span>
		</li>
		{{#project.isStarted}}
			{{^project.isCompleted}}
				<li>
					<span class="title">Time Remaining</span>
					<span class="info remaining">{{project.timeRemaining}}</span>
				</li>
			{{/project.isCompleted}}
		{{/project.isStarted}}
		{{^project.isStarted}}
			<li>
				<span class="title">Starts in</span>
				<span class="info">{{project.startsIn}}</span>
			</li>
		{{/project.isStarted}}
	</ul>
</div>

{{#levels.length}}
	<div class="levels">
		<ul>
			{{#levels}}
				<li class="level">
					<ul>
						<li class="title">{{title}}</li>
						<li class="info">${{#numberFormat}}{{amount}}{{/numberFormat}}</li>
						<li class="description">
							{{#preserveLinebreaks}}{{description}}{{/preserveLinebreaks}}
						</li>
						<li class="contribute-label">
							{{#isSoldOut}}
								<label class="soldout">
									Sold out!
								</label>
							{{/isSoldOut}}
							{{^isSoldOut}}
								<form method="post" action="{{fundUrl}}">
									<input type="hidden" name="level" value="{{id}}" />
									<label class="contribute-label">
										{{#canSellUnlimited}}
											<input class="button" type="submit" value="Fund" />
										{{/canSellUnlimited}}
										{{^canSellUnlimited}}
											<input class="button" type="submit" value="Fund ({{numRemaining}}/{{numAllowed}} remaining)" />
										{{/canSellUnlimited}}
									</label>
								</form>
							{{/isSoldOut}}
						</li>
					</ul>
				</li>
			{{/levels}}
		</ul>
	</div>
{{/levels.length}}