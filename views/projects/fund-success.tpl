<div class="fund success">
	<div class="title stretch">
		<h1>Thanks for funding<br />one of our projects.</h1>
	</div>
	<div class="content noborder">
		<hr class="spacer short" />
		<div class="swing">
			<div class="bd">
				<div class="swingleft">
					<h2><a href="{{{projectUrl}}}">{{project.title}}</a></h2>
				</div>
				<hr class="spacer short" />
				<hr class="spacer short" />
			</div>
		</div>
		<div class="funding">
			<ul class="title">
				<li class="pledge">
					<span>Your Pledge</span>
				</li>
				<li class="target">
					<span>Target</span>
				</li>
				<li class="total">
					<span>Total Pledged</span>
				</li><li class="days">
					<span>Time to Go</span>
				</li>
			</ul>
			<ul class="content">
				<li class="pledge">
					<span>${{#numberFormat}}{{amount}}{{/numberFormat}}</span>
				</li>
				<li class="target">
					<span>${{#numberFormat}}{{project.amountGoal}}{{/numberFormat}}</span>
				</li>
				<li class="total">
					<span>${{#numberFormat}}{{project.amountRaised}}{{/numberFormat}}</span>
				</li>
				<li class="days">
					<span>{{project.timeRemaining}}</span>
				</li>
			</ul>
		</div>
		<hr class="spacer short" />
		<div class="share">
			<a class="psuedobutton small arial" href="{{#self.url}}{{/self.url}}">
				<span class="label">FUND ANOTHER PROJECT</span>
			</a>
			<br /><br />
			<a href="https://twitter.com/share" class="twitter-share-button" data-via="alumnifunder" data-size="large">Tweet</a>
			<br /><br />
			<div class="fb-like" data-href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{{projectUrl}}}" data-send="true" data-show-faces="true"></div>
		</div>
		<hr class="spacer short" />
		<hr class="spacer short" />
	</div>
</div>