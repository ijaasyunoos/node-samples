{{#project.hasErrors}}
	{{#project.errorsArray}}
		{{> common/form/error}}
	{{/project.errorsArray}}
	<br />
{{/project.hasErrors}}
{{#user.hasErrors}}
	{{#user.errorsArray}}
		{{> common/form/error}}
	{{/user.errorsArray}}
	<br />
{{/user.hasErrors}}
<div class="title stretch border">
	<h1>Please Enter Your<br /> Payment Details</h1>
</div>
<div class="content border payments">
	<form method="post">
		<input type="hidden" name="level" value="{{level.id}}" />
		<input type="hidden" name="amount" value="{{amount}}" />
		<hr class="spacer short" />
<!--
    <label class="checkboxLabel">
      <input type="checkbox" name="storeDetails" {{#self.post.save_payment_info}}checked="checked"{{/self.post.save_payment_info}} />
      <span class="label primary">Store your payment details for future use?</span>
      <span class="error">
        <span class="bd"></span>
      </span>
    </label>

    <hr class="spacer short" />
    <hr class="spacer short border solid" />
    <hr class="spacer short" />
-->
		<span class="required_label">REQUIRED FIELDS <b>*</b></span>
		<label class="field">
			<span class="label primary">Name</span>
			<input placeholder="name" type="text" class="showAll toggle-label" name="name" value="{{self.post.name}}" autocomplete="off" maxlength="255" />
			<span class="required">*</span>
		</label>
		<div class="address">
			<label class="field">
				<span class="label primary">Address</span>
					<input placeholder="address1" type="text" class="showAll toggle-label" name="address1" value="{{self.post.address1}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>
			<label class="field">
				<span class="label primary">&nbsp;</span>
				<input placeholder="address2" type="text" class="showAll toggle-label" name="address2" value="{{self.post.address2}}" autocomplete="off" maxlength="255" />
			</label>
			<label class="field">
				<span class="label primary">&nbsp;</span>
				<input placeholder="city" type="text" class="showAll toggle-label" name="city" value="{{self.post.city}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>
			<label class="field">
				<span class="label primary">&nbsp;</span>
				<input placeholder="state" type="text" class="showAll toggle-label" name="state" value="{{self.post.state}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>
			<label class="field">
				<span class="label primary">&nbsp;</span>
				<input placeholder="zip" type="text" class="showAll toggle-label" name="zip" value="{{self.post.zip}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>                                                    
		</div>
		<label class="field">
			<span class="label primary">Credit Card Number</span>
			<input type="text" class="showAll toggle-label" name="credit_card_number" value="{{self.post.creditCardNumber}}" autocomplete="off" maxlength="255" /><span class="required">*</span>
		</label>
		<div class="cropped">
			<label class="field">
				<span class="label primary">Expiration</span>
				<input placeholder="month" type="text" class="showAll toggle-label" name="expiration_month" value="{{self.post.expirationMonth}}" autocomplete="off" maxlength="255" />
			</label>
			<label class="field">
				<input placeholder="year" type="text" class="showAll toggle-label" name="expiration_year" value="{{self.post.expirationYear}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>                                        
		</div>
		<div class="cropped">
			<label class="field">
				<span class="label primary">Security Code</span>
				<input type="text" class="showAll toggle-label" name="security_code" value="{{self.post.securityCode}}" autocomplete="off" maxlength="255" />
				<span class="required">*</span>
			</label>
		</div>
		<hr class="spacer short" />
		<hr class="spacer short border solid" />
		<hr class="spacer short" />

    <!-- amount to alumni fund --->
    <label class="money">
      <span class="label primary">Amount to Alumnifund</span>
      <input placeholder="" type="text" class="showAll toggle-label" name="fundAmount" value="{{amount}}" autocomplete="off" maxlength="255" />
    </label>

    <hr class="spacer short" />
    <hr class="spacer short border solid" />
    <hr class="spacer short" />    
<!--    
    Level: {{level.title}}<br />
    Amount: ${{amount}}<br />
-->
		<label class="submit">
			<input type="hidden" name="pay" value="1" />
			<input type="submit" class="button" value="PAY NOW" />
		</label>
	</form>
</div>