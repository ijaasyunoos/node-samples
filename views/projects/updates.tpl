<div class="projectpage project">
	<div class="title swing border">
		<div class="bd">
			<div class="swingleft unBrowse">
				<h1>{{project.title}}</h1>
			</div>
			<div class="options buttons">
				{{#canManage}}
					<ul>
						<li>
							<a href="{{#self.url}}create-update,{{project.id}}{{/self.url}}" class="psuedobutton big">
								<span>Add an Update</span>
							</a>
						</li>
					</ul>
				{{/canManage}}
				<div class="register impulse">
					<a href="{{fundUrl}}" class="button">Fund Project</a>
				</div>
			</div>
		</div>
	</div>
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft unBrowse">
				<div class="updates">
					<ul>
						{{#updates}}
							<li class="update">
								{{> projects/partial.updates}}
							</li>
						{{/updates}}
						{{^updates}}
							<li><p class="no-updates">There are no updates for this project yet.</p></li>
						{{/updates}}
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>