<ul>
	<li class="title">
		<span class="highlight number">{{update.number}}</span> <!-- #TODO# no count in place -->
		<div class="inner-title">
			<a href="{{updateUrl}}" class="highlight title">{{update.title}}</a>
			<div class="inner-subtitle">
				<b class="highlight subtitle date">{{#dateFormat}}{{update.dateUpdated}}{{/dateFormat}}</b>
				<!-- #comments hidden#
				<a href="#"><b class="highlight subtitle">5 comments</b></a> -->
				{{#canManage}}
					{{^update.isPublic}}
						<strong class="highlight subtitle">Not Published</strong>
					{{/update.isPublic}}
				{{/canManage}}
			</div>
		</div>
	</li>
	<!-- comments hidden
	<li class="comments summary">
		<a href="#"><span class="highlight subsubtitle update-button">5 comments</span></a>
	</li>
	-->
</ul>