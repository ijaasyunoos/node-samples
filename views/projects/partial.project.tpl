<li class="project">
	{{#isOtherUniversity}}
		<a href="{{{nonWhitelabelBaseUrl}}}{{{projectUrl}}}" class="link" title="{{project.title}}" target="_blank"></a>
	{{/isOtherUniversity}}
	{{^isOtherUniversity}}
		<a href="{{{projectUrl}}}" class="link" title="{{project.title}}"></a>
	{{/isOtherUniversity}}
	<div class="img-overlay unfocused grid-contain-without-border-bottom">
		<img src="{{#thumbnail.url}}226,0,0{{/thumbnail.url}}" alt="Project" />
	</div>
	<h4 class="grid-contain-with-border-bottom">{{project.title}}</h4>
	<div class="details">
		<ul>
			<li class="detail name">
				<a href="{{{ownerUrl}}}" class="owner">{{owner.name}}</a>
			</li>
			<li class="detail amount">
				${{#numberFormat}}{{project.amountGoal}}{{/numberFormat}}
			</li>
			<li class="detail description">
				{{#preserveLinebreaks}}{{project.timeRemaining}}{{/preserveLinebreaks}}
			</li>
		</ul>
		<div class="progress">
			<span class="percent">{{project.amountRaisedPercentage}}%</span>
			<div class="fill" style="height:{{project.amountRaisedPercentage}}%"></div>
		</div>
	</div>
</li>