<div class="projectpage">
	<div class="title swing border">
		<div class="bd top">
			<div class="swingleft">
				{{#canManage}}
					{{#needsPaymentAccount}}
						<div class="warning">Important! You won't be able to accept any payments until you connect your<br />AlumniFunder account with Stripe, our payment provider.
							<form method="post" action="{{{projectUrl}}}">
								<input type="hidden" name="connect" value="stripe" />
								<input type="image" src="{{{siteRoot}}}images/stripe/connect.png" alt="Connect with Stripe" />
							</form>
						</div>
					{{/needsPaymentAccount}}
					{{^levels.length}}
						<div class="warning">
							Important! Before you can submit your project for approval, you must<br />
							<h2><a href="{{#self.url}}edit-levels,{{project.id}}{{/self.url}}">create at least one funding level</a></h2>
						</div>
					{{/levels.length}}
				{{/canManage}}
				<h1>{{project.title}}</h1>
				<h2 class="subtitle">{{project.subtitle}}</h2>
			</div>
			<div class="register impulse">
				{{^project.isApproved}}
					<h2>Note: This project is not approved yet.</h2>
					{{#user.isAdmin}}
						{{#project.isSubmitted}}
							<form method="post">
								<label class="submit">
									<input type="hidden" name="approve" value="1" />
									<input type="submit" class="button" value="Approve Project" />
								</label>
							</form>
							<form method="post">
								<label class="submit">
									<input type="hidden" name="reject" value="1" />
									<input type="submit" class="button" value="Reject Project" />
								</label>
							</form>
						{{/project.isSubmitted}}
					{{/user.isAdmin}}
					{{#canManage}}
						{{^project.isSubmitted}}
							{{^needsPaymentAccount}}
								<form method="post">
									<label class="submit">
										<input type="hidden" name="submit" value="1" />
										<input type="submit" class="button" value="Submit Project for Approval"></input>
									</label>
								</form>
							{{/needsPaymentAccount}}
						{{/project.isSubmitted}}
					{{/canManage}}
				{{/project.isApproved}}
				{{#canManage}}
					<a class="edit" href="{{#self.url}}edit-levels,{{project.id}}{{/self.url}}">Edit Funding Levels</a>
					{{#canEdit}}
						<a class="edit" href="{{#self.url}}edit,{{project.id}}{{/self.url}}">Edit Details</a>
					{{/canEdit}}
				{{/canManage}}
				{{#user.isAdmin}}
					{{#project.isEquity}}
						<form method="post">
							<label class="submit">
								<input type="hidden" name="convert_to_non_equity" value="1" />
								<input type="submit" class="button" value="Convert to Non-Equity" />
							</label>
						</form>
					{{/project.isEquity}}
					{{^project.isEquity}}
						<form method="post">
							<label class="submit">
								<input type="hidden" name="convert_to_equity" value="1" />
								<input type="submit" class="button" value="Convert to Equity" />
							</label>
						</form>
					{{/project.isEquity}}
				{{/user.isAdmin}}
				{{#levels.length}}
					<a href="{{fundUrl}}">Fund Project</a>
				{{/levels.length}}
			</div>
		</div>
	</div>
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft">
				<div class="reg top">
					<div class="subtitle relative grid-contain-with-border-bottom">
						<h2 class="name">By <a href="{{{ownerUrl}}}">{{owner.name}}</a></h2>
						<div class="options-container">
							<a class="psuedobutton small hili" href="{{updatesUrl}}">
								<span class="subbertitle">{{numUpdates}} updates</span>
							</a>
							{{#canEdit}}
								<a class="psuedobutton small hili second" href="{{#self.url}}delete,{{project.id}}{{/self.url}}">
									<span class="subbertitle">Delete Project</span>
								</a>
							{{/canEdit}}
						</div>
						<div class="clear"></div>
					</div>
					<div class="display relative">
						<div class="project-container image">
							<img class="project" src="{{#thumbnail.url}}400,400,1{{/thumbnail.url}}" alt="Project Image" />
						</div>
						{{#project.hasVideo}}
							<div class="project-container video">
								{{#project.isVideoYouTube}}
									<iframe width="488" height="275" src="http://www.youtube.com/embed/{{project.videoId}}" frameborder="0" allowfullscreen></iframe>
								{{/project.isVideoYouTube}}
								{{#project.isVideoVimeo}}
									<iframe src="http://player.vimeo.com/video/{{project.videoId}}" width="488" height="275" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								{{/project.isVideoVimeo}}
							</div>
						{{/project.hasVideo}}
						{{#project.hasPresentation}}
							<div class="project-container presentation">
								{{#project.isPresentationPrezi}}
									<div class="prezi-player">
										<style type="text/css" media="screen">
										.prezi-player { width: 488px; }
										.prezi-player-links { text-align: center; }
										</style>
										<object id="prezi_{{project.presentationId}}" name="prezi_{{project.presentationId}}" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="488" height="355">
											<param name="movie" value="http://prezi.com/bin/preziloader.swf"/>
											<param name="allowfullscreen" value="true"/>
											<param name="allowFullScreenInteractive" value="true"/>
											<param name="allowscriptaccess" value="always"/>
											<param name="wmode" value="direct"/>
											<param name="bgcolor" value="#ffffff"/>
											<param name="flashvars" value="prezi_id={{project.presentationId}}&amp;lock_to_path=0&amp;color=ffffff&amp;autoplay=no&amp;autohide_ctrls=0"/>
											<embed id="preziEmbed_{{project.presentationId}}" name="preziEmbed_{{project.presentationId}}" src="http://prezi.com/bin/preziloader.swf" type="application/x-shockwave-flash" allowfullscreen="true" allowFullScreenInteractive="true" allowscriptaccess="always" width="488" height="355" bgcolor="#ffffff" flashvars="prezi_id={{project.presentationId}}&amp;lock_to_path=0&amp;color=ffffff&amp;autoplay=no&amp;autohide_ctrls=0"></embed>
										</object>
									</div>
								{{/project.isPresentationPrezi}}
							</div>
						{{/project.hasPresentation}}
						<div class="share">
							<!--
							<a href="https://twitter.com/share" class="twitter-share-button" data-via="alumnifunder" data-size="large">Tweet</a>
							<div class="fb-like" data-href="{{#paraglide.longUrl}}{{/paraglide.longUrl}}{{projectUrl}}" data-send="true" data-layout="box_count" data-show-faces="true"></div>
							-->
							{{#project.websiteUrl}}
								<a target="_blank" href="{{project.websiteUrl}}" class="arrow-link arrow-link-small arrow-link-gray">
									<span>VISIT WEBSITE</span><i></i>
								</a>
							{{/project.websiteUrl}}
							<a target="_blank" href="https://twitter.com/intent/tweet?text={{project.title}}%20-%20{{project.subtitle}}%20-%20AlumniFunder&url={{{paraglide.longUrl}}}{{{projectUrl}}}&via=alumnifunder" class="foundicon normal social-icon">
								<span class="icon foundicon-twitter"></span>
								<span class="label">Tweet</span>
							</a>
							<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u={{{paraglide.longUrl}}}{{{projectUrl}}}" class="foundicon normal social-icon">
								<span class="icon foundicon-facebook"></span>
								<span class="label">Share</span>
							</a>
						</div>
					</div>
					<div class="project-description">
						<h3><b>Project Summary</b></h3>
						<p>{{#preserveLinebreaks}}{{#linkify}}{{{project.description}}}{{/linkify}}{{/preserveLinebreaks}}</p>
					</div>
					<div class="project-description">
						<h3><b>Background</b></h3>
						<p>{{#preserveLinebreaks}}{{#linkify}}{{{project.background}}}{{/linkify}}{{/preserveLinebreaks}}</p>
					</div>
					{{#project.purpose}}
						<div class="project-description">
							<h3><b>Purpose</b></h3>
							<p>{{#preserveLinebreaks}}{{#linkify}}{{{project.purpose}}}{{/linkify}}{{/preserveLinebreaks}}</p>
						</div>
					{{/project.purpose}}
					{{#project.moreInfo}}
						<div class="project-description">
							<h3><b>Additional Info</b></h3>
							<p>{{#preserveLinebreaks}}{{#linkify}}{{{project.moreInfo}}}{{/linkify}}{{/preserveLinebreaks}}</p>
						</div>
					{{/project.moreInfo}}
					{{#TODO}}
						<div class="project-description">
						<h3><b>The Team</b></h3>
							<div class="team">
								<ul>
									{{#teamMembers}}
										<li>
											<ul class="member">
												<li class="image">
													<a href="{{{teamMemberUrl}}}"><img src="{{{siteRoot}}}images/team/146x146/ryan.png" alt="ryan" /></a>
												</li>
												<li class="name">
													<a href="{{{teamMemberUrl}}}">{teamMember.name}}</a>
												</li>
												<li class="title">
													{{teamMember.title}}
												</li>
												<li class="about">
													<p>{{teamMember.about}}</p>
												</li>
											</ul>
										</li>
									{{/teamMembers}}
								</ul>
							</div>
						</div>
					{{/TODO}}
				</div>
			</div>
		<div class="details">
			<div class="bd">
				{{> projects/sidebar}}
			</div>
		</div>
	</div>
</div>