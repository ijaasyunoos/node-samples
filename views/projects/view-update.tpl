<div class="projectpage project">
	<div class="title swing border">
		<div class="bd">
			<div class="swingleft unBrowse">
				<h1><a href="{{{projectUrl}}}">{{project.title}}</a></h1>
			</div>
			<div class="options buttons">
				<ul>
					{{#canManage}}
						<li>
							<a href="{{#self.url}}edit-update,{{project.id}}/{{update.id}}{{/self.url}}" class="psuedobutton big">
								<span>Edit Update</span>
							</a>
						</li>
						<li>
							<a href="{{#self.url}}delete-update,{{project.id}}/{{update.id}}{{/self.url}}" class="psuedobutton big">
								<span>Delete Update</span>
							</a>
						</li>
					{{/canManage}}
					{{#levels.length}}
						<li>
							<a href="{{fundUrl}}" class="psuedobutton big">
								<span>Fund Project</span>
							</a>
						</li>
					{{/levels.length}}
				</ul>
			</div>
		</div>
	</div>
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft unBrowse">
				<div class="updates">
					<ul>
						<li class="update">
							<ul>
								<li class="title">
									<div class="inner-title">
										<a href="{{updateUrl}}" class="highlight title">{{update.title}}</a>
										<div class="inner-subtitle">
											<b class="highlight subtitle date">{{#dateFormat}}{{update.dateUpdated}}{{/dateFormat}}</b>
											<!-- #comments hidden#
											<a href="#"><b class="highlight subtitle">5 comments</b></a> -->
											{{#canManage}}
												{{^update.isPublic}}
													<form method="post">
														<input type="hidden" name="publish" value="1" />
														<input type="submit" class="highlight subtitle" value="Publish Now?" />
													</form>
												{{/update.isPublic}}
											{{/canManage}}
										</div>
									</div>
								</li>
								<li class="content">
									<p>{{#preserveLinebreaks}}{{update.description}}{{/preserveLinebreaks}}</p>
								</li>
								<!-- comments hidden
								<li class="comments summary">
									<a href="#"><span class="highlight subsubtitle update-button">5 comments</span></a>
								</li>
								-->
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>