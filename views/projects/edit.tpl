<div class="title swing border grid-contain-without-border-bottom">
	<div class="bd">
		<div class="swingleft">
			<h1>
				{{#project.id}}
					Edit {{project.title}}
				{{/project.id}}
				{{^project.id}}
					Create a project
				{{/project.id}}
			</h1>
		</div>
		<div>
			&nbsp;
		</div>
	</div>
</div>
{{#project.hasErrors}}
	{{#project.errorsArray}}
		{{> common/form/error}}
	{{/project.errorsArray}}
{{/project.hasErrors}}
<form method="post" enctype="multipart/form-data">
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft">
				<div class="inputs wide">
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Project Title</b></span>
						<span class="required_label">REQUIRED FIELDS <b>*</b></span>
						<input type="text" class="showAll toggle-label" name="title" value="{{project.title}}" autocomplete="off" maxlength="255" />
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Project Subtitle</b></span>
						<input type="text" class="showAll toggle-label" name="subtitle" value="{{project.subtitle}}" autocomplete="off" maxlength="255" />
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />
					<hr class="spacer short" />
					<hr class="spacer short" />
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Location</b></span>
						<input type="text" class="showAll toggle-label" name="location" value="{{project.location}}" autocomplete="off" maxlength="255" />
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Website</b></span>
						<input type="text" class="showAll toggle-label" name="website_url" value="{{project.websiteUrl}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Image</b></span>
						<input type="file" class="showAll toggle-label" name="image" autocomplete="off" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Video URL (YouTube or Vimeo)</b></span>
						<input type="text" class="showAll toggle-label" name="video_url" value="{{project.videoUrl}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Presentation URL (Prezi)</b></span>
						<input type="text" class="showAll toggle-label" name="presentation_url" value="{{project.presentationUrl}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Project Summary</b></span>
						<textarea type="textarea" class="showAll toggle-label" name="description" autocomplete="off" maxlength="2000">{{project.description}}</textarea>
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Background</b></span>
						<textarea type="textarea" class="showAll toggle-label" name="background" autocomplete="off" maxlength="2000">{{project.background}}</textarea>
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />                  
					<label class="big field">
						<span class="label primary"><b>Purpose</b></span>
						<textarea type="textarea" class="showAll toggle-label" name="purpose" autocomplete="off" maxlength="2000">{{project.purpose}}</textarea>
					</label>
					<hr class="spacer short" />                  
					<label class="big field">
						<span class="label primary"><b>Additional Information</b></span>
						<textarea type="textarea" class="showAll toggle-label" name="more_info" autocomplete="off" maxlength="2000">{{project.moreInfo}}</textarea>
					</label>
					<hr class="spacer short" />
					<!-- detailed title and inputs -->
					<div class="swing final">
						<div class="bd">
							<div class="swingleft">
								<label class="submit">
									<input type="submit" class="button" value="SAVE" />
								</label>
								<label class="submit">
									<a href="{{projectUrl}}" class="button">Cancel</a>
								</label>
							</div>
						</div>
					</div>
					<hr class="spacer short" />
					<hr class="spacer" />
				</div>
			</div>
			<div>
				<div class="inputs narrow">
					<hr class="spacer short" />
					<label class="field">
						<span class="label primary"><b>Set your funding target</b></span>
						<input type="text" class="showAll toggle-label" name="amount_goal" value="{{project.amountGoal}}" autocomplete="off" maxlength="255" />
						<span class="required">*</span>
					</label>
					{{#isDurationEditable}}
						<hr class="spacer short" />
						<label class="field">
							<span class="label primary"><b>Project Duration (days)</b></span>
							<input type="text" class="showAll toggle-label" name="duration" value="{{project.duration}}" autocomplete="off" maxlength="255" />
							<span class="required">*</span>
						</label>
					{{/isDurationEditable}}
					<hr class="spacer short" />
					<label class="field">
						<span class="label primary"><b>Start Date</b></span>
						<input type="text" class="showAll date toggle-label" name="date_funding_starts" value="{{#dateFormat}}{{project.dateFundingStarts}}{{/dateFormat}}" autocomplete="off" maxlength="255" />
						<span class="required">*</span>
					</label>
					<hr class="spacer short" />
					<label class="field">
						<span class="label primary"><b>Privacy</b></span>
						<div class="privacy-content">
							<input type="checkbox" class="showAll toggle-label" name="is_private" {{#project.isPrivate}}checked="checked"{{/project.isPrivate}} value="1" />
							<p>Hide project from public index (can still be accessed from URL)</p>
						</div>
					</label>
				</div>
			</div>
		</div>
	</div>
</form>