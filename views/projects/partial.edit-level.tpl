<div class="level">
	{{#hasErrors}}
		{{#errorsArray}}
			{{> common/form/error}}
		{{/errorsArray}}
		<br />
	{{/hasErrors}}
	<form class="level" method="post">
		<input type="hidden" name="level" value="{{id}}" />
		<ul>
			<li class="title">
				<input type="text" class="name" placeholder="Level title" name="title" value="{{title}}" />
				{{#canSellUnlimited}}
					<input type="text" class="quantity hide-quantity" disabled="disabled" placeholder="Qty" name="num_allowed" value="{{numAllowed}}" />
				{{/canSellUnlimited}}
				{{^canSellUnlimited}}
					<input type="text" class="quantity" placeholder="Qty" name="num_allowed" value="{{numAllowed}}" />
				{{/canSellUnlimited}}
			</li>
			<li class="check-quantity">
				<label class="quantity-field">
					<input type="checkbox" name="can_sell_unlimited" class="unlimitedQuantityCheckbox" value="1" {{#canSellUnlimited}}checked="checked"{{/canSellUnlimited}} />UNLIMITED QUANTITY
				</label>
			</li>
			<li class="amount">
				<span><input type="text" name="amount" placeholder="$" value="{{amount}}" /></span>
			</li>
			<li class="rule"></li>
			<li class="content">
				<textarea name="description" maxlength="2000" placeholder="Add your level description">{{description}}</textarea>
			</li>
			<li class="delete">
				<input type="submit" value="Update" />
			</li>
		</ul>
	</form>
	<form class="delete" method="post">
		<input type="hidden" name="level" value="{{id}}" />
		<input type="hidden" name="delete" value="1" />
		<input type="submit" value="Delete" />
	</form>
</div>