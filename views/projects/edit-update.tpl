<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>
				{{#update.id}}
					<h2>Edit update '{{update.title}}' from project '{{project.title}}'</h2>
				{{/update.id}}
				{{^update.id}}
					<h2>Add a new project update for '{{project.title}}'</h2>
				{{/update.id}}
			</h1>
			<hr class="spacer short" />
		</div>
	</div>
</div>
{{#update.hasErrors}}
	{{#update.errorsArray}}
		{{> common/form/error}}
	{{/update.errorsArray}}
	<br />
{{/update.hasErrors}}
<form class="edit-update" method="post">
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft">
				<div class="inputs wide">
					<hr class="spacer short" />
					<span class="required_label">REQUIRED FIELDS <b>*</b></span>
					<label class="big field">
						<span class="label primary"><b>Update Title</b></span><br />
						<input type="text" class="showAll big toggle-label" name="title" value="{{update.title}}" autocomplete="off" maxlength="255" />
						<span class="required title-asterik">*</span>
					</label>
					<!--
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<hr class="spacer short" />
					<label>
              <span class="label primary"><b>Update Image</b></span>
              <br />
              <span class="upload-image button"><b>Upload Image</b></span>
            </label> -->
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Update Description</b></span><br />
						<textarea type="textarea" class="showAll toggle-label" name="description" autocomplete="off" maxlength="2000">{{update.description}}</textarea>
						<span class="required description-asterik">*</span>
					</label>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<!-- detailed title and inputs -->
					<div class="swing final">
						<div class="bd">
							<div class="swingleft">
								<label class="submit">
									<input type="submit" class="button" value="SAVE" />
								</label>
								<label class="submit">
									<a class="cancel" href="{{{projectUrl}}}" class="button">Cancel</a>
								</label>	
							</div>
						</div>
					</div>
					<hr class="spacer short" />
					<hr class="spacer" />
				</div>
			</div>
		</div>
	</div>
</form>