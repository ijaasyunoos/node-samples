<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>{{#university}}{{name}}{{/university}} {{^university}}All{{/university}} Projects</h1>
		</div>
		{{^whitelabelUniversity}}
			<div class="projects links grid-contain-without-border-bottom">
				<a class="arrow-link arrow-link-gray arrow-link-small" href="{{#self.url}}{{/self.url}}">
					<span>All Projects</span><i></i>
				</a>
				{{#universities}}
					<a class="arrow-link arrow-link-gray arrow-link-small" href="{{#self.url}}universities,{{university.slug}}{{/self.url}}">
						<span>{{university.name}}</span><i></i>
					</a>
				{{/universities}}
				{{#user.isAdmin}}
					<a class="arrow-link arrow-link-gray arrow-link-small" href="{{#self.url}}unapproved{{/self.url}}">
						<span>Admin: Unapproved Queue</span><i></i>
					</a>
				{{/user.isAdmin}}
			</div>
		{{/whitelabelUniversity}}
	</div>
	{{#projects.length}}
		<h2 class="project-count">{{projects.length}} project{{^hasOneProject}}s{{/hasOneProject}}</h2>
	</div>
	<div class="clear"></div>
	<div class="content wide">
		<div class="projects list">
			<ul>
				{{#projects}}
					{{> projects/partial.project}}
				{{/projects}}
			</ul>
			<hr class="spacer" />
		</div>
	</div>
	{{/projects.length}}
	{{^projects.length}}
		{{#university}}
			<h2 class="project-count">There are no active projects for this university right now.</h2>
		</div>
		<div class="clear"></div>
		<hr class="spacer short" />
		<h2>Check back soon!</h2>
		{{/university}}
		{{^university}}
			<h2 class="project-count">There are no active projects right now.</h2>
		</div>
		<div class="clear"></div>
		<hr class="spacer short" />
		<h2>Check back soon!</h2>
		{{/university}}
		<hr class="spacer" />
		<hr class="spacer" />
		<hr class="spacer" />
		<hr class="spacer" />
	{{/projects.length}}
