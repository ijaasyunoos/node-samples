<h2>You must be an accredited investor to view and invest in equity projects.</h2>
{{#user}}
	<h3>Your account isn't set up as an accredited investor.<br />Please email <a href="mailto:info@alumnifunder.com">info@alumnifunder.com</a> to change your account.</h3>
{{/user}}
{{^user}}
	<h3>You can create an account <a href="{{#paraglide.url}}account,register-equity-investor{{/paraglide.url}}">here</a> or login <a href="{{#paraglide.url}}account,login{{/paraglide.url}}">here</a></h3>
{{/user}}
<hr class="spacer" />
<hr class="spacer" />
<hr class="spacer" />
