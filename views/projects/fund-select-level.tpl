<div class="fund select">
	<div class="title stretch grid-contain-with-border-bottom">
		<h1>
			So you'd like to fund
			<br />
			<a href="{{projectUrl}}">{{project.title}}</a>
		</h1>
	</div>
	<form method="post">
		<div class="content noborder">
			<hr class="spacer short" />
			<hr class="spacer short" />
			<h2>Confirm your funding level</h2>
			<hr class="spacer short" />
			<hr class="spacer short" />
			{{#project.hasErrors}}
				{{#project.errorsArray}}
					{{> common/form/error}}
				{{/project.errorsArray}}
				<br />
			{{/project.hasErrors}}
			{{#selectedLevel.hasErrors}}
				{{#selectedLevel.errorsArray}}
					{{> common/form/error}}
				{{/selectedLevel.errorsArray}}
				<br />
			{{/selectedLevel.hasErrors}}
			<div class="funding radio_fields">
				<ul>
					{{#levels}}
						{{#isSoldOut}}
							<li class="sold-out grid-contain-without-border-bottom">
								<label class="radio_label sold-out">
						{{/isSoldOut}}
						{{^isSoldOut}}
							<li class="grid-contain-without-border-bottom">
								<label class="radio_label">
						{{/isSoldOut}}
								<input type="radio" name="level" value="{{level.id}}" data-onchange='{"action": "val", "value": "{{level.amount}}", "element": "#Donate"}'{{#isSelected}}checked="checked"{{/isSelected}} />
								<ul class="level">
									<li class="title">
										<span>{{level.title}}{{#isSoldOut}} (sold out){{/isSoldOut}}</span>
									</li>
									<li class="amount">
										<span>${{level.amount}}</span>
									</li>
									<li>
										<hr class="spacer short border solid" />
									</li>
									<li class="description">
										<span>
											{{#preserveLinebreaks}}{{level.description}}{{/preserveLinebreaks}}
										</span>
									</li>
								</ul>
							</label>
						</li>
					{{/levels}}
				</ul>
			</div>
		</div>
		<hr class="spacer short" />
		<hr class="spacer short clear" />
		<hr class="spacer short border solid" />
		<hr class="spacer short" />
		<h2>Or, choose your own amount to donate</h2>
		<hr class="spacer short" />
		<hr class="spacer short" />

		<label class="Donate">
			<input type="text" class="showAll" name="amount" value="{{amount}}" autocomplete="off" maxlength="255" placeholder="Enter An Amount" />
		</label>
		<hr class="spacer short" />
		<hr class="spacer border short solid" />
		<hr class="spacer short" />
		<label class="confirm">
			<input type="submit" class="button" value="FUND PROJECT" />
		</label>
	</form>
</div>
