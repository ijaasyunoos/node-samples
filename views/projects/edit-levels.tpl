<div class="edit levels">
	<div class="title swing border">
		<div class="bd grid-contain-without-border-bottom">
			<div class="swingleft">
				{{#levels.length}}
					<h1>Change your<br />funding levels</h1>
				{{/levels.length}}
				{{^levels.length}}
					<h1>Create your<br />funding levels</h1>
				{{/levels.length}}
				<h2><a href="{{{projectUrl}}}">{{project.title}}</a></h2>
				{{#newLevel.hasErrors}}
					{{#newLevel.errorsArray}}
						{{> common/form/error}}
					{{/newLevel.errorsArray}}
					<br />
				{{/newLevel.hasErrors}}
			</div>
			<div>
				<hr class="spacer short" />
			</div>
		</div>
	</div>
	<hr class="spacer border short solid" />
	<div class="content noborder">
		<div class="new-funding">
			{{#levels}}
				{{> projects/partial.edit-level}}
			{{/levels}}
			<div class="level">
				<form class="level" method="post">
					<input type="hidden" name="level" value="new" />
					<ul>
						<li class="title">
							<input type="text" class="name" placeholder="Level title" name="title" value="{{newLevel.title}}" />
							{{#newLevel.canSellUnlimited}}
								<input type="text" class="quantity hide-quantity" disabled="disabled" placeholder="Qty" name="num_allowed" value="{{newLevel.numAllowed}}" />
							{{/newLevel.canSellUnlimited}}
							{{^newLevel.canSellUnlimited}}
								<input type="text" class="quantity" placeholder="Qty" name="num_allowed" value="{{newLevel.numAllowed}}" />
							{{/newLevel.canSellUnlimited}}
						</li>
						<li class="check-quantity">
							<label class="quantity-field">
								<input type="checkbox" name="can_sell_unlimited" value="1" class="unlimitedQuantityCheckbox" {{#newLevel.canSellUnlimited}}checked="checked"{{/newLevel.canSellUnlimited}} />UNLIMITED QUANTITY
							</label>
						</li>
						<li class="amount">
							<span><input type="text" name="amount" placeholder="$" value="{{newLevel.amount}}" /></span>
						</li>
						<li class="rule"></li>
						<li class="content">
							<textarea name="description" maxlength="2000" placeholder="Add your level description">{{newLevel.description}}</textarea>
						</li>
						<li>
							<label class="submit">
								<input type="submit" class="button" value="Add Level" />
							</label>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>