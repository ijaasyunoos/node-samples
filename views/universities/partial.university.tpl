<li class="project">
	<a href="{{universityUrl}}" class="link" title="{{university.name}}"></a>
	<h4>
		{{university.name}}
	</h4>
	{{^university.isActiveNow}}
		<b>Note: This university is not yet active.</b>
	{{/university.isActiveNow}}
</li>