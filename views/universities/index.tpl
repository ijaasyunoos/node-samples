<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>Universities</h1>
		</div>
	</div>
</div>
<div class="content-wide">
	<a href="{{#self.url}}create{{/self.url}}">Create University</a>
</div>
<hr class="spacer short border solid" />
{{#universities.length}}
	<h2>{{universities.length}} universities</h2>
	<hr class="spacer medium" />
	<div class="content wide">
		<hr class="spacer short border solid" />
		<div class="projects list">
			<ul>
				{{#universities}}
					{{> universities/partial.university}}
				{{/universities}}
			</ul>
			<hr class="spacer" />
		</div>
	</div>
{{/universities.length}}
{{^universities.length}}
	<h2>There are no active universities right now.</h2>
	<hr class="spacer short" />
	<h2>Check back soon!</h2>
	<hr class="spacer" />
{{/universities.length}}
