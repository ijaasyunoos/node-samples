<div class="projectpage">
	<div class="title swing border">
		<div class="bd">
			<div class="swingleft">
				<h1>{{university.name}}</h1><br />
			</div>
			<div class="register impulse">
				{{^university.isActiveNow}}
					<h2>Note: This university is not yet active.</h2>
				{{/university.isActiveNow}}
				{{#canEdit}}
					<a class="edit" href="{{#self.url}}edit,{{university.id}}{{/self.url}}">Edit</a>
				{{/canEdit}}
			</div>
		</div>
	</div>
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft">
				<div class="reg top">
					<div class="subtitle relative">
						<div class="options-container">
							{{#canEdit}}
								<a class="psuedobutton small hili second" href="{{#self.url}}delete,{{university.id}}{{/self.url}}">
									<span class="subbertitle">Delete University</span>
								</a>
							{{/canEdit}}
						</div>
						<div class="clear"></div>
					</div>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<h3><b>Description</b></h3>
					<p>{{#preserveLinebreaks}}{{#linkify}}{{{university.description}}}{{/linkify}}{{/preserveLinebreaks}}</p>
					<hr class="spacer short">
					<hr class="spacer border solid short" />
				</div>
			</div>
		</div>
	</div>
	<hr class="spacer" />
</div>