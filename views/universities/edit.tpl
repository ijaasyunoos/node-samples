<div class="title swing border">
	<div class="bd">
		<div class="swingleft">
			<h1>
				{{#university.id}}
					Edit {{university.name}}
				{{/university.id}}
				{{^university.id}}
					Create a <br />university
				{{/university.id}}
			</h1>
		</div>
		<div>
			&nbsp;
		</div>
	</div>
</div>
{{#university.hasErrors}}
	{{#university.errorsArray}}
		{{> common/form/error}}
	{{/university.errorsArray}}
{{/university.hasErrors}}
<form method="post" enctype="multipart/form-data">
	<div class="content swing border">
		<div class="bd">
			<div class="swingleft">
				<div class="inputs wide">
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>University Name</b></span>
						<input type="text" class="showAll toggle-label" name="name" value="{{university.name}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>University Slug</b></span>
						<input type="text" class="showAll toggle-label" name="slug" value="{{university.slug}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Is Active?</b></span>
						<input type="checkbox" class="showAll toggle-label" name="is_active" {{#university.isActive}}checked="checked"{{/university.isActive}} value="1" autocomplete="off" />
					</label>
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>Date Active</b></span>
						<input type="text" class="showAll date toggle-label" name="date_active" value="{{university.dateActive}}" autocomplete="off" maxlength="255" />
					</label>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<hr class="spacer short" />
					<label class="big field">
						<span class="label primary"><b>University Description</b></span>
						<textarea type="textarea" class="showAll toggle-label" name="description" autocomplete="off" maxlength="2000">{{university.description}}</textarea>
					</label>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<div class="swing final">
						<div class="bd">
							<div class="swingleft">
								<label class="submit">
									<input type="submit" class="button" value="SAVE" />
								</label>
								<label class="submit">
									<a href="{{universityUrl}}" class="button">Cancel</a>
								</label>
							</div>
						</div>
					</div>
					<hr class="spacer short" />
					<hr class="spacer border solid short" />
					<hr class="spacer" />
				</div>
			</div>
		</div>
	</div>
</form>