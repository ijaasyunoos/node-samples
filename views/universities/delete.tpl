<div class="projectpage project">
	<div class="title swing border">
		<div class="bd">
			<div class="swingleft unBrowse">
				<hr class="spacer short">
				<hr class="spacer short">
				<h2 class="delete-confirmation">Are you sure you want to delete this university,<br />'{{university.name}}'?</h2><br />
			</div>
			<div class="options buttons">
				<ul>
					<li>
						<form method="post" class="bg-fill">
							<input type="hidden" name="delete" value="1" />
							<input type="submit" class="psuedobutton-input" value="Yes, Delete It" />
						</form>
					<li>
						<a href="{{{universityUrl}}}" class="psuedobutton big">
							<span>&nbsp;No, Go Back</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>